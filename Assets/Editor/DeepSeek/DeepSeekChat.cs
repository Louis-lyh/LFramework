﻿using System;
using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using Editor.DeepSeek;
using Newtonsoft.Json; // 使用 Newtonsoft.Json

// 自定义聊天消息类
public class ChatMessage
{
    public string content;
    public string role;

    public ChatMessage(string content, string role)
    {
        this.content = content;
        this.role = role;
    }
}

public static class DeepSeekChat
{
    private static string apiKey = "sk-548f9ce483bd49c58de4ae8d17f3749e"; // 替换为你的API密钥
    private static string apiUrl = "https://api.deepseek.com/chat/completions"; // API URL
    public static async void PostRequest(string userMessage,Action<string> callback)
    {
        // 创建请求体对象
        var requestBody = new
        {
            //1. **messages 对话上下文配置**:
            // - 这个字段是一个数组，包含对话的消息历史。每个消息对象有`content`和`role`属性。
            // - `role`可以是`system`、`user`或`assistant`，分别代表系统消息、用户消息和助手消息。
            // - 示例中的系统消息设定了助手的行为，用户消息是用户的输入。
            messages = new[]
            {
                new { content = "You are a helpful assistant", role = "system" },
                new { content = userMessage, role = "user" }
            },
            // ** 模型控制参数 **
            // * deepseek-chat 模型已全面升级为 DeepSeek-V3，接口不变。 通过指定 model='deepseek-chat' 即可调用 DeepSeek-V3。
            // * deepseek-reasoner 是 DeepSeek 最新推出的推理模型 DeepSeek-R1。通过指定 model='deepseek-reasoner'，即可调用 DeepSeek-R1。
            model = "deepseek-reasoner",
            //  生成回复的最大token数，这里设为2048，控制回复长度
            max_tokens = 2048,
            // 控制生成随机性的参数，值越高越随机，1为默认值，平衡创造性和准确性。
            temperature = 1,
            // 核采样参数，控制生成时考虑的token集合，1表示考虑所有可能的token，通常与temperature配合使用。
            top_p = 1,
            
            // ** 内容控制参数 **
            // 频率惩罚，用于降低重复token的生成概率。值范围通常在-2.0到2.0之间，0表示无惩罚
            frequency_penalty = 0,
            // 存在惩罚，影响模型生成新主题的倾向。正值鼓励新内容，负值反之，0表示中性。
            presence_penalty = 0,
            // 设置停止序列，当生成遇到这些字符串时停止。这里设为`null`，表示不使用停止条件。
            stop = (string)null,
            
            // ** 响应格式控制 **
            // 指定响应格式，`type`设为`text`表示希望返回纯文本，而不是JSON等格式。
            response_format = new { type = "text" },
            // 是否启用流式传输，`false`表示一次性返回完整响应，而不是分块流式传输。
            stream = false,
            // 流式传输的额外选项，这里设为`null`，可能在不使用流式传输时不需要设置。
            stream_options = (object)null,
            
            // 这两个字段可能与外部工具集成有关，设为`null`和`"none"`表示不使用任何外部工具
            tools = (object)null,
            tool_choice = "none",
            logprobs = false,
            top_logprobs = (object)null
        };
        // 使用 Newtonsoft.Json 序列化对象
        string json = JsonConvert.SerializeObject(requestBody);
        // 创建 UnityWebRequest
        using (UnityWebRequest request = new UnityWebRequest(apiUrl, "POST"))
        {
            byte[] bodyRaw = System.Text.Encoding.UTF8.GetBytes(json);
            request.uploadHandler = new UploadHandlerRaw(bodyRaw);
            request.downloadHandler = new DownloadHandlerBuffer();
            request.SetRequestHeader("Content-Type", "application/json");
            request.SetRequestHeader("Accept", "application/json");
            request.SetRequestHeader("Authorization", "Bearer " + apiKey);
            // 打印调试信息
            Debug.Log("Request URL: " + apiUrl);
            Debug.Log("Request Body: " + json);
            // 发送请求
            var req = request.SendWebRequest();
            var isAwait = true;
            req.completed += delegate(AsyncOperation operation)
            {
                isAwait = false;
            };

            await UniTask.WaitWhile(() => isAwait);
            
            // 处理响应
            if (request.result == UnityWebRequest.Result.ConnectionError || request.result == UnityWebRequest.Result.ProtocolError)
            {
                Debug.LogError("Error: " + request.error);
                Debug.LogError("Response: " + request.downloadHandler.text); // 打印完整响应
                // 回调
                callback?.Invoke(request.downloadHandler.text);
            }
            else
            {
                string responseJson = request.downloadHandler.text;
                Debug.Log("Response: " + responseJson);
                // 解析 JSON 响应
                var response = JsonConvert.DeserializeObject<DeepSeekResponse>(responseJson);
                if (response.choices != null && response.choices.Length > 0)
                {
                    Debug.Log("DeepSeek says: " + response.choices[0].message.content);
                    // 回调
                    callback?.Invoke(response.choices[0].message.content);
                }
                else
                {
                    Debug.LogError("No response from DeepSeek.");
                    // 回调
                    callback?.Invoke("No response from DeepSeek.");
                }
            }
        }
    }
    
    private static string apiKey_db = "de1f6422-1f0a-442a-8927-42bbc2761c59"; // 替换为你的API密钥
    private static string apiUrl_db = "https://ark.cn-beijing.volces.com/api/v3/chat/completions"; // API URL
    public static async void PostRequest_DB(List<ChatMessage> message,Action<string> callback)
    {
        // 创建请求体对象
        var requestBody = new
        {
            model = "ep-20250218113735-s6z94",
            //1. **messages 对话上下文配置**:
            // - 这个字段是一个数组，包含对话的消息历史。每个消息对象有`content`和`role`属性。
            // - `role`可以是`system`、`user`或`assistant`，分别代表系统消息、用户消息和助手消息。
            // - 示例中的系统消息设定了助手的行为，用户消息是用户的输入。
            messages = message,
        };

        // 使用 Newtonsoft.Json 序列化对象
        string json = JsonConvert.SerializeObject(requestBody);
        // 创建 UnityWebRequest
        // 创建UnityWebRequest对象
        UnityWebRequest request = new UnityWebRequest(apiUrl_db, "POST");

        // 设置请求头
        request.SetRequestHeader("Content-Type", "application/json");
        request.SetRequestHeader("Authorization", "Bearer " + apiKey_db);

        // 设置请求体
        byte[] bodyRaw = System.Text.Encoding.UTF8.GetBytes(json);
        request.uploadHandler = new UploadHandlerRaw(bodyRaw);
        request.downloadHandler = new DownloadHandlerBuffer();
        
        // 打印调试信息
        Debug.Log("PostRequest_DB URL: " + apiUrl_db);
        Debug.Log("PostRequest_DB Body: " + json);
        // 发送请求
        var req = request.SendWebRequest();
        var isAwait = true;
        req.completed += delegate(AsyncOperation operation)
        {
            isAwait = false;
        };

        await UniTask.WaitWhile(() => isAwait);
        
        // 处理响应
        if (request.result == UnityWebRequest.Result.ConnectionError || request.result == UnityWebRequest.Result.ProtocolError)
        {
            Debug.LogError("Error: " + request.error);
            Debug.LogError("Response: " + request.downloadHandler.text); // 打印完整响应
            // 回调
            callback?.Invoke(request.downloadHandler.text);
        }
        else
        {
            string responseJson = request.downloadHandler.text;
            Debug.Log("Response: " + responseJson);
            // 解析 JSON 响应
            var response = JsonConvert.DeserializeObject<DeepSeekResponse>(responseJson);
            if (response.choices != null && response.choices.Length > 0)
            {
                Debug.Log("DeepSeek says: " + response.choices[0].message.content);
                // 回调
                callback?.Invoke(response.choices[0].message.content);
            }
            else
            {
                Debug.LogError("No response from DeepSeek.");
                // 回调
                callback?.Invoke("No response from DeepSeek.");
            }
        }
    }
}
// 定义响应数据结构
[System.Serializable]
public class DeepSeekResponse
{
    public Choice[] choices;
}
[System.Serializable]
public class Choice
{
    public Message message;
}
[System.Serializable]
public class Message
{
    public string content;
    public string role;
}
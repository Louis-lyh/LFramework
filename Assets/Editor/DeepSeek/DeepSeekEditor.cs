﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Editor.DeepSeek
{
    public class DeepSeekEditor : EditorWindow
    {
        /// <summary>
        /// 页签
        /// </summary>
        private readonly List<EditorTab> tabs = new List<EditorTab>();
        /// <summary>
        /// 选择的页签
        /// </summary>
        private int _selectedTabIndex = -1;
        /// <summary>
        /// 当前选择的页签
        /// </summary>
        private int _currentSelectedTabIntex = -1;
        
        
        [MenuItem("Tools/DeepSeek", false, 0)]
        private static void Init()
        {
            // 创建窗口
            var window = GetWindow(typeof(DeepSeekEditor));
            window.titleContent = new GUIContent("DeepSeek");
        }
        
        /// <summary>
        /// 当对象加载时调用此函数
        /// </summary>
        private void OnEnable()
        {
            tabs.Add(new DeepSeekTab(this));
            _selectedTabIndex = 0;
        }
        
        private void OnGUI()
        {
            // 顶部页签
            _selectedTabIndex = GUILayout.Toolbar(_selectedTabIndex, new[] {"DeepSeek"});

            if (_selectedTabIndex >= 0 && _selectedTabIndex < tabs.Count)
            {
                var selectedEditor = tabs[_selectedTabIndex];
                if (_selectedTabIndex != _currentSelectedTabIntex)
                {
                    selectedEditor.OnTabSelected();
                    GUI.FocusControl(null);
                }
                selectedEditor.Draw();
                _currentSelectedTabIntex = _selectedTabIndex;
            }
        }
    }
}
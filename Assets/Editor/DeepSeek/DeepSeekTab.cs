﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Editor.DeepSeek
{

    
    public class DeepSeekTab : EditorTab
    {
        private EditorWindow _parent;
        
        private Vector2 scrollPosition;
        private string inputMessage = "";
        private List<ChatMessage> chatHistory = new List<ChatMessage>();
        
        
        public DeepSeekTab(DeepSeekEditor parent)
        {
            _parent = parent;
        }
        
        /// <summary>
        /// 绘制
        /// </summary>
        public override void Draw()
        {
            // 使用 this.position 获取窗口尺寸 ✅
            EditorGUILayout.BeginVertical(GUILayout.Height(_parent.position.height - 60)); 
            // 绘制聊天记录
            DrawChatHistory();
            EditorGUILayout.EndVertical();
            
            // 绘制输入
            DrawInputArea();
        }
        /// <summary>
        /// 绘制聊天记录
        /// </summary>
        private void DrawChatHistory()
        {
            // 滚动视图区域
            scrollPosition = EditorGUILayout.BeginScrollView(scrollPosition, GUILayout.ExpandHeight(true));
            // 绘制信息
            foreach (var message in chatHistory)
            {
                // 使用 this.position.width ✅
                GUILayout.Label(message.content, GetMessageStyle(message), GUILayout.MaxWidth(_parent.position.width * 0.8f));
                GUILayout.Space(5);
            }
            EditorGUILayout.EndScrollView();
        }
        /// <summary>
        /// 绘制消息类型
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        private GUIStyle GetMessageStyle(ChatMessage message)
        {
            var style = new GUIStyle(EditorStyles.helpBox);
            style.normal.textColor = message.role.Equals("system") ? Color.white : Color.black;
            style.alignment = message.role.Equals("system") ? TextAnchor.MiddleLeft : TextAnchor.MiddleRight;
        
            // 创建背景纹理
            var bgColor = message.role.Equals("system") ? new Color(0.1f, 0.5f, 0.9f) : new Color(0.9f, 0.9f, 0.9f);
            style.normal.background = CreateBackgroundTexture(bgColor);
        
            return style;
        }

        private Texture2D CreateBackgroundTexture(Color color)
        {
            var tex = new Texture2D(1, 1);
            tex.SetPixel(0, 0, color);
            tex.Apply();
            return tex;
        }
        
        private void DrawInputArea()
        {
            EditorGUILayout.BeginVertical(GUI.skin.box, GUILayout.Height(60));

            EditorGUILayout.BeginHorizontal();
        
            // 添加输入框名称用于焦点控制
            GUI.SetNextControlName("ChatInput");
            inputMessage = EditorGUILayout.TextArea(inputMessage, GUILayout.ExpandWidth(true));

            if (GUILayout.Button("发送", GUILayout.Width(60)))
            {
                SendMessage();
            }

            EditorGUILayout.EndHorizontal();
            EditorGUILayout.EndVertical();

            // 回车发送逻辑
            HandleEnterKey();
        }
        
        private void HandleEnterKey()
        {
            if (Event.current.type == EventType.KeyDown && 
                Event.current.keyCode == KeyCode.Return &&
                GUI.GetNameOfFocusedControl() == "ChatInput")
            {
                SendMessage();
                Event.current.Use();
            }
        }

        private void SendMessage()
        {
            if (!string.IsNullOrEmpty(inputMessage))
            {
                
                // 添加用户消息
                chatHistory.Add(new ChatMessage(inputMessage, "user"));

                // 请求重绘
                _parent.Repaint();
                
                // 发送
                DeepSeekChat.PostRequest_DB(chatHistory, (content) =>
                {
                    // 回复
                    chatHistory.Add(new ChatMessage(content, "system"));
                    
                    // 自动滚动到底部
                    scrollPosition.y = Mathf.Infinity;
                    
                    // 请求重绘
                    _parent.Repaint();
                });
                
                // 清空输入框
                inputMessage = "";
            }
        }
    }
}
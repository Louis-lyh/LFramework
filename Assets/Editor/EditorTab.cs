﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;
using Newtonsoft.Json;
public class EditorTab
{
    /// <summary>
    /// 选中此选项卡时调用
    /// </summary>
    public virtual void OnTabSelected()
    {
    }
    /// <summary>
    /// 绘制选项卡时调用
    /// </summary>
    public virtual void Draw()
    {

    }

    protected T LoadJsonFile<T>(string path) where T:class
    {
        if (!File.Exists(path))
            return null;
        var file = new StreamReader(path);
        string jsonStr = file.ReadToEnd();
        file.Close();

        return JsonConvert.DeserializeObject<T>(jsonStr);
    }

    protected void SaveJsonFile<T>(string path,T data) where T : class
    {
        string info = JsonConvert.SerializeObject(data);

        FileStream fs = new FileStream(path,FileMode.Create,FileAccess.Write);
        fs.Close();
        StreamWriter sw = new StreamWriter(path);
        sw.WriteLine(info);
        sw.Close();
        sw.Dispose();
        Debug.Log("SaveJsonFile ");
    }
}

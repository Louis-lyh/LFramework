﻿using System.Collections;
using System.Collections.Generic;
using GameLogic.Hotfix;
using UnityEngine;

public class MapData 
{
    /// <summary>
    /// 地图名字
    /// </summary>
    public string mapName;

    public int x;

    public int y;

    /// <summary>
    /// 地图的障碍物和建筑集合
    /// </summary>
    public List<MapElement> mapElements = new List<MapElement>();

    /// <summary>
    /// 障碍物百分比
    /// </summary>
    [Range(0, 1)] public float obsPercent;

    /// <summary>
    /// 地图中心
    /// </summary>
    public Coord mapCenter;


    /// <summary>
    /// 生成地图
    /// </summary>
    public void GenerateMap()
    {
        mapCenter = new Coord(x/2,y/2);
        // 是否是障碍
        bool[,] mapObstacles = new bool[x,y];

        int obsCount;
        mapElements.Clear();
        
        // 创建路元素
        for (int i = 0; i < y; i++)
        {
            for (int j = 0; j < x; j++)
            {
                MapElement mapElement = new MapElement()
                {
                    elementType = MapElementType.Road,
                    coord = new Coord(j,i),
                };

                mapElements.Add(mapElement);
            }
        }

        MapElement[] randomAllRilesCoord = GameHelpUtil.ShuffleCoords<MapElement>(mapElements.ToArray());
        
        // 障碍数量
        obsCount = (int)( x * y * obsPercent);

        int currenrObsCount = 0;
        for (int i = 0; i < obsCount; i++)
        {
            MapElement randomCoord = randomAllRilesCoord[i];

            mapObstacles[randomCoord.coord.x, randomCoord.coord.y] = true;
            currenrObsCount++;

            if (randomCoord.coord != mapCenter && MapIsFulyAccessible(mapObstacles, currenrObsCount))
            {
                randomCoord.elementType = MapElementType.Mountain;
            }
            else
            {
                mapObstacles[randomCoord.coord.x, randomCoord.coord.y] = false;
                currenrObsCount--;
            }
        }
    }
    /// <summary>
    /// 洪水填充
    /// </summary>
    private bool MapIsFulyAccessible(bool[,] _mapObstacles, int _currentObsCount)
    {
        bool[,] mapFlags = new bool[_mapObstacles.GetLength(0), _mapObstacles.GetLength(1)];

        Queue<Coord> queue = new Queue<Coord>();
        queue.Enqueue(mapCenter);
        mapFlags[mapCenter.x, mapCenter.y] = true;

        int accessibleCount = 1;

        while (queue.Count > 0)
        {
            Coord currentTile = queue.Dequeue();

            for (int x = -1; x <= 1; x++)
            {
                for (int y = -1; y <= 1; y++)
                {
                    int neighborX = currentTile.x + x;
                    int neighborY = currentTile.y + y;
                    //将没有true的放入队列 然后一直广度搜索相邻的true
                    if (x == 0 || y == 0)
                    {
                        if (neighborX >= 0 && neighborX < _mapObstacles.GetLength(0)
                            && neighborY >= 0 && neighborY < _mapObstacles.GetLength(1))
                        {
                            if (!mapFlags[neighborX, neighborY] && !_mapObstacles[neighborX, neighborY])
                            {
                                mapFlags[neighborX, neighborY] = true;
                                accessibleCount++;

                                queue.Enqueue(new Coord(neighborX, neighborY));
                            }

                        }

                    }

                }
            }

        }

        //剩余总共true的数量
        int obsTargetCount = (int)(x * y - _currentObsCount);

        return accessibleCount == obsTargetCount;
    }

}

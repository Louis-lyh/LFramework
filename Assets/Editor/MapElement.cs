﻿using System.Collections;
using System.Collections.Generic;
using GameLogic.Hotfix;
using UnityEngine;

public class MapElement
{
    public MapElementType elementType = MapElementType.Road;
    public BuildingType buildingType = BuildingType.None;
    public CampType camp = CampType.None;
    public SerialNumber serialNumber = SerialNumber.None;

    [Tooltip("坐标")]
    public Coord coord;
}
public struct Coord
{
    public int x;
    public int y;

    public Coord(int _x, int _y)
    {
        x = _x;
        y = _y;
        
    }

    public static bool operator ==(Coord _c1, Coord _c2)
    {
        return _c1.x == _c2.x && _c1.y == _c2.y;
    }
    public static bool operator !=(Coord _c1, Coord _c2)
    {
        return !(_c1 == _c2);
    }
}


public enum MapElementType
{
    Road,
    Building,
    Mountain,
}

/// <summary>
/// 序号
/// </summary>
public enum SerialNumber
{
    None,
    A,
    B,
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;
using GameLogic.Hotfix;

public class MapGenerateEditor : EditorTab
{
    MapData currentMapData;

    private string filePath = "/Res/Json/Map";
    private Vector2 scrollPos;

    private string editorImagePath = "/Editor/UI/EditorImage";
    private readonly Dictionary<string, Texture> textures = new Dictionary<string, Texture>();

    #region 地图信息
    Transform mapParent;
    Transform mountainParent;
    Transform buildingParent;

    MapElementType mapElementTypeSelect = MapElementType.Road;
    BuildingType buildingTypeSelect = BuildingType.Arsenal;
    SerialNumber  serialNumberSelect = SerialNumber.None;
    CampType campSelect = CampType.None;

    #endregion
    public MapGenerateEditor()
    {
        //获取图片
        var imagesPath =  new DirectoryInfo(Application.dataPath + editorImagePath);
        var fileInfo = imagesPath.GetFiles("*.png", SearchOption.TopDirectoryOnly);
        foreach(var file in fileInfo)
        {
            var filename = Path.GetFileNameWithoutExtension(file.Name);
            textures[filename] = AssetDatabase.LoadAssetAtPath<Texture>($"Assets/Editor/UI/EditorImage/{filename}.png");
        }

        //地图父级
        GameObject mapGO = GameObject.Find("Map");
        if (mapGO == null)
        {
            mapGO = new GameObject("Map");
        }
        mapParent = mapGO.transform;

        mountainParent = mapParent.Find("Mountain");
        if (mountainParent == null)
            mountainParent = new GameObject("Mountain").transform;
        mountainParent.SetParent(mapParent);

        buildingParent = mapParent.Find("Building");
        if (buildingParent == null)
            buildingParent = new GameObject("Building").transform;
        buildingParent.SetParent(mapParent);

    }

    public override void Draw()
    {
        scrollPos = EditorGUILayout.BeginScrollView(scrollPos);

        var oldLableWidh = EditorGUIUtility.labelWidth;
        EditorGUIUtility.labelWidth = 90;

        GUILayout.Space(15);

        DrawMenu();

        if(currentMapData != null)
        {
            GUILayout.Space(15);

            GUILayout.BeginVertical();

            GUILayout.BeginHorizontal();
            EditorGUILayout.LabelField(new GUIContent("地图名称"), EditorStyles.boldLabel,
                GUILayout.Width(55));
            currentMapData.mapName = EditorGUILayout.TextField(currentMapData.mapName, GUILayout.Width(70));
            GUILayout.EndHorizontal();

            EditorGUILayout.LabelField("地图大小");
            GUILayout.BeginHorizontal();
            EditorGUILayout.LabelField(new GUIContent("X:"), EditorStyles.boldLabel,
                GUILayout.Width(20));
            currentMapData.x = EditorGUILayout.IntField(currentMapData.x, GUILayout.Width(50));
            EditorGUILayout.LabelField(new GUIContent("Y:"), EditorStyles.boldLabel,
               GUILayout.Width(20));
            currentMapData.y = EditorGUILayout.IntField(currentMapData.y, GUILayout.Width(50));
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            EditorGUILayout.LabelField(new GUIContent("障碍物百分比"), EditorStyles.boldLabel,
                GUILayout.Width(EditorGUIUtility.labelWidth));
            currentMapData.obsPercent = EditorGUI.Slider(new Rect(80, 145, 150, 20),currentMapData.obsPercent,0,1);
            GUILayout.EndHorizontal();

            GUILayout.Space(10);

            GUILayout.BeginHorizontal();
            if(GUILayout.Button("生成地图模板", GUILayout.Width(90), GUILayout.Height(30)))
            {
                currentMapData.GenerateMap();
            }
            if (GUILayout.Button("生成场景地图", GUILayout.Width(90), GUILayout.Height(30)))
            {
                GenerateMap();
            }
            GUILayout.EndHorizontal();

           

            GUILayout.EndVertical();

            EditorGUILayout.LabelField(new GUIContent("地图模板："), EditorStyles.boldLabel,
                GUILayout.Width(55));

            mapElementTypeSelect = (MapElementType)EditorGUILayout.EnumPopup("地图元素:", mapElementTypeSelect, GUILayout.Width(240));

            switch (mapElementTypeSelect)
            {
                case MapElementType.Road:
                    break;
                case MapElementType.Mountain:
                    break;
                case MapElementType.Building:
                    buildingTypeSelect = (BuildingType)EditorGUILayout.EnumPopup("建筑类型:", buildingTypeSelect, GUILayout.Width(240));
                    campSelect = (CampType)EditorGUILayout.EnumPopup("阵营:", campSelect, GUILayout.Width(240));
                    if (buildingTypeSelect == BuildingType.Arsenal)
                        serialNumberSelect = (SerialNumber)EditorGUILayout.EnumPopup("序号:", serialNumberSelect, GUILayout.Width(240));
                    else
                        serialNumberSelect = SerialNumber.None;
                    break;
                
            }

            GUILayout.Space(10);

            for (var i = currentMapData.y - 1; i >= 0 ; i--)
            {
                GUILayout.BeginHorizontal();
                for (var j = 0; j < currentMapData.x ; j++)
                {
                    var tileIndex = (currentMapData.x * i) + j;
                    CreateButton(tileIndex);
                }
                GUILayout.EndHorizontal();
            }

        }

        EditorGUILayout.EndScrollView();

    }

    public void DrawMenu()
    {
        GUILayout.BeginHorizontal();

        if (GUILayout.Button("新建", GUILayout.Width(100), GUILayout.Height(50)))
        {
            currentMapData = new MapData() {
                x = 10,
                y = 10,
                obsPercent = 0.2f,
                mapName = "新建",
            };
        }

        if (GUILayout.Button("打开", GUILayout.Width(100), GUILayout.Height(50)))
        {
            var path = EditorUtility.OpenFilePanel("Open Level", Application.dataPath + filePath, "json");

            if (!string.IsNullOrEmpty(path))
            {
                currentMapData = LoadJsonFile<MapData>(path);
            }
        }

        if (GUILayout.Button("保存", GUILayout.Width(100), GUILayout.Height(50)))
        {
            SaveLevel(Application.dataPath + filePath);
        }

        GUILayout.EndHorizontal();
    }

    private void DrawMapEditor()
    {


    }

    private void CreateButton(int index)
    {
        if (index < currentMapData.mapElements.Count)
        {
            string typeStr;
            MapElementType type = currentMapData.mapElements[index].elementType;
            typeStr = type.ToString();
            switch (type)
            {
                case MapElementType.Road:
                    break;
                case MapElementType.Mountain:
                    break;

                case MapElementType.Building:
                    string building = currentMapData.mapElements[index].buildingType.ToString();
                    typeStr = building.ToString();
                    if (currentMapData.mapElements[index].camp != CampType.None)
                        typeStr += "_" + currentMapData.mapElements[index].camp;

                    break;

            }

            float edge = 800/ currentMapData.y;
            edge = Mathf.Clamp(edge, 20, 60);
            if (textures.ContainsKey(typeStr))
            {
                if (GUILayout.Button(textures[typeStr], GUILayout.Width(edge), GUILayout.Height(edge)))
                {
                    DrawMap(index);
                }
            }
            else
            {
              
                if (GUILayout.Button("", GUILayout.Width(edge), GUILayout.Height(edge)))
                {
                    DrawMap(index);
                }
            }
        }
        
    }
    /// <summary>
    /// 手动绘制地图
    /// </summary>
    /// <param name="index"></param>
    private void DrawMap(int index)
    {
        int x = index % currentMapData.x;
        int y = index / currentMapData.x;

        MapElement mapElement = currentMapData.mapElements[index];

        switch (mapElementTypeSelect)
        {
            case MapElementType.Road:
            case MapElementType.Mountain:

                //DestoryElement(mapElement);

                mapElement.elementType = mapElementTypeSelect;
                mapElement.buildingType = BuildingType.None;
                mapElement.camp =CampType.None;
                mapElement.serialNumber = SerialNumber.None;

               /* if (mapElementTypeSelect == MapElementType.Mountain)
                    GenerateElement(mapElement);*/

                break;
            case MapElementType.Building:

               // DestoryElement(mapElement);

                mapElement.elementType = MapElementType.Building;
                mapElement.buildingType = buildingTypeSelect;
                mapElement.camp = campSelect;
                mapElement.serialNumber = serialNumberSelect;

                //GenerateElement(mapElement);

                break;
        }

        currentMapData.mapElements[index] = mapElement;

    }
    /// <summary>
    /// 自动生成地图
    /// </summary>
    private void GenerateMap()
    {
        GameObject wallPrefab = AssetDatabase.LoadAssetAtPath<GameObject>("Assets/Res/GamePlay/Mountain/Wall.prefab");
       
        GameObject.DestroyImmediate(mountainParent.gameObject);
        mountainParent = new GameObject("Mountain").transform;
        mountainParent.SetParent(mapParent);

        GameObject.DestroyImmediate(buildingParent.gameObject);
        buildingParent = new GameObject("Building").transform;
        buildingParent.SetParent(mapParent);

        for (var y = 0; y < currentMapData.y; y++)
        {
            for (var x = 0; x < currentMapData.x; x++)
            {
                int index = (currentMapData.x * y) + x;
                // 创建地图元素
                if (currentMapData.mapElements[index].elementType != MapElementType.Road )
                    GenerateElement(currentMapData.mapElements[index], index);

                // 创建墙 x 行
                if (y == 0 || y == currentMapData.y - 1)
                {
                    GameObject wall = PrefabUtility.InstantiatePrefab(wallPrefab) as GameObject;
                    wall.transform.position = new Vector3(1 * x + 0.5f, 0, 1 * y + ( y == 0 ?-0.5f : 1.5f));
                    wall.transform.SetParent(mountainParent);
                }
                // 创建墙 y 列
                if (x == 0 || x == currentMapData.x - 1)
                {
                    GameObject wall = PrefabUtility.InstantiatePrefab(wallPrefab) as GameObject;
                    wall.transform.position = new Vector3(1 * x + ( x == 0 ?-0.5f : 1.5f), 0, 1 * y + 0.5f);
                    wall.transform.SetParent(mountainParent);
                }
            }
        }
    }
    /// <summary>
    /// 生成地图元素
    /// </summary>
    /// <param name="element"></param>
    /// <param name="offsetY"></param>
    private void GenerateElement(MapElement element,int index,float offsetY = 0)
    {
        string prefabName = "";
        string path = "";
        Transform parent = null;
        // int index = currentMapData.x * element.coord.y + element.coord.x;
        int x = index % currentMapData.x;
        int y = index / currentMapData.x;
        switch (element.elementType)
        {
            case MapElementType.Building:
                path = $"Building/{element.buildingType}" + (element.serialNumber != SerialNumber.None ? "_" + element.serialNumber : "");
                prefabName = $"{element.buildingType}" + (element.serialNumber != SerialNumber.None ? "_" + element.serialNumber : "");
                parent = buildingParent;
                break;
            case MapElementType.Mountain:
                path = $"Mountain/{MapElementType.Mountain}One";
                prefabName = $"{MapElementType.Mountain}One";
                parent = mountainParent;
                break;
        }
        GameObject prefab = AssetDatabase.LoadAssetAtPath<GameObject>($"Assets/Res/GamePlay/{path}.prefab");

        string elementName = prefabName + $".{x}_{y}";
        GameObject elementGO = PrefabUtility.InstantiatePrefab(prefab) as GameObject;

        elementGO.transform.position = new Vector3(0.5f + (x * 1), offsetY, 0.5f + (y * 1));
        elementGO.transform.rotation = RotateElement(index);
        elementGO.transform.SetParent(parent);
        elementGO.name = elementName;
    }

    private Quaternion RotateElement(int index)
    {
        int x = currentMapData.x;
        int y = currentMapData.y;
        Quaternion quaternion = Quaternion.identity;
        int[] randomNum = new int[] { 1, 2, 3, 4 };
        //randomNum = Learn.Utilities.ShuffleCoords(randomNum);

        for(int i =0;i<randomNum.Length;i++)
        {
            bool sentry = false;
            switch(randomNum[i])
            {
                case 1:
                    if ( index < currentMapData.mapElements.Count - x && currentMapData.mapElements[index + x].elementType == MapElementType.Road)
                    {
                        quaternion.eulerAngles = Vector3.up * 0;
                        sentry = true;
                    }
                    break;
                case 2:
                    if (index < currentMapData.mapElements.Count-1 && currentMapData.mapElements[index + 1 ].elementType == MapElementType.Road && (index+1) % currentMapData.x != 0)
                    {
                        quaternion.eulerAngles = Vector3.up * 90;
                        sentry = true;

                    }

                    break;
                case 3:
                    if (index > x && currentMapData.mapElements[index - x].elementType == MapElementType.Road )
                    {
                        quaternion.eulerAngles = Vector3.up * 180;
                        sentry = true;

                    }

                    break;
                case 4:
                    if (index >= 1 && currentMapData.mapElements[index - 1].elementType == MapElementType.Road && (index - 1) % currentMapData.x != currentMapData.x - 1)
                    {
                        quaternion.eulerAngles = Vector3.up * 270;
                        sentry = true;

                    }
                    break;
            }
            if (sentry)
                break;
        }

        return quaternion;
    }

    /// <summary>
    /// 销毁地图元素
    /// </summary>
    /// <param name="element"></param>
    private void DestoryElement(MapElement element)
    {
        string name;
        string elementPath = "";
        switch (element.elementType)
        {
            case MapElementType.Mountain:
                name = $"{MapElementType.Mountain}One";
                elementPath = "Mountain/"+name + $".{element.coord.x}_{element.coord.y}";
                break;
            case MapElementType.Building:
                name = $"{element.buildingType}"+ (element.serialNumber != SerialNumber.None ? "_"+element.serialNumber:"");
                elementPath = "Building/" + name + $".{element.coord.x}_{element.coord.y}";
                break;
        }
        if(elementPath != "")
        {
            GameObject currElement = GameObject.Find($"Map/{elementPath}");
            if (currElement != null)
                GameObject.DestroyImmediate(currElement);
        }
       
    }


    /// <summary>
    /// 将当前信息保存到指定路径。
    /// </summary>
    /// <param name="path">The path to which to save the current currentLevel.</param>
    public void SaveLevel(string path)
    {
#if UNITY_EDITOR
        SaveJsonFile(path + "/" + currentMapData.mapName + ".json", currentMapData);
        AssetDatabase.Refresh();
#endif
    }
}

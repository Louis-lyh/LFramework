using UnityEngine;
using UnityEditor;

namespace AICommand {

[FilePath("UserSettings/AICommandSettings.asset",
          FilePathAttribute.Location.ProjectFolder)]
public sealed class AICommandSettings : ScriptableSingleton<AICommandSettings>
{
    public string apiUrl = "";
    public string apiKey = null;
    public string modelKey = "";
    public int timeout = 0;
    public void Save() => Save(true);
    void OnDisable() => Save();
}

sealed class AICommandSettingsProvider : SettingsProvider
{
    public AICommandSettingsProvider()
      : base("Project/AI Command", SettingsScope.Project) {}

    public override void OnGUI(string search)
    {
        var settings = AICommandSettings.instance;

        var url = settings.apiUrl;
        var key = settings.apiKey;
        var modelKey = settings.modelKey;
        var timeout = settings.timeout;

        EditorGUI.BeginChangeCheck();

        url = EditorGUILayout.TextField("API Url", url);
        key = EditorGUILayout.TextField("API Key", key);
        modelKey = EditorGUILayout.TextField("Model Key", modelKey);
        timeout = EditorGUILayout.IntField("Timeout", timeout);

        if (EditorGUI.EndChangeCheck())
        {
            settings.apiUrl = url;
            settings.apiKey = key;
            settings.modelKey = modelKey;
            settings.timeout = timeout;
            settings.Save();
        }
    }

    [SettingsProvider]
    public static SettingsProvider CreateCustomSettingsProvider()
      => new AICommandSettingsProvider();
}

} // namespace AICommand

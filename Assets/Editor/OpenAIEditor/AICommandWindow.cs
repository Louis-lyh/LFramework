using UnityEngine;
using UnityEditor;
using System.Reflection;

namespace AICommand {

public sealed class AICommandWindow : EditorWindow
{
    #region Temporary script file operations

    const string TempFilePath = "Assets/AICommandTemp.cs";

    bool TempFileExists => System.IO.File.Exists(TempFilePath);

    void CreateScriptAsset(string code)
    {
        // UnityEditor内部方法
        // UnityEditor internal method: ProjectWindowUtil.CreateScriptAssetWithContent
        var flags = BindingFlags.Static | BindingFlags.NonPublic;
        var method = typeof(ProjectWindowUtil).GetMethod("CreateScriptAssetWithContent", flags);
        method.Invoke(null, new object[]{TempFilePath, code});
    }

    #endregion

    #region 脚本生成器

    /// <summary>
    /// 前缀提示
    /// </summary>
    static string WrapPrompt(string input)
      => "Write a Unity Editor script.\n" +
         " - It provides its functionality as a menu item placed \"Edit\" > \"Do Task\".\n" +
         " - It doesn’t provide any editor window. It immediately does the task when the menu item is invoked.\n" +
         " - Don’t use GameObject.FindGameObjectsWithTag.\n" +
         " - There is no selected object. Find game objects manually.\n" +
         " - I only need the script body. Don’t add any explanation.\n" +
         "The task is described as follows:\n" + input;

    /// <summary>
    /// 中文前缀提示
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    static string WrapPromptChinese(string input)
        => "编写Unity编辑器脚本。\n" +
           "-它提供的功能是放置在“编辑”>“执行任务”的菜单项。\n" +
           "-它不提供任何编辑器窗口。当菜单项被调用时，它会立即执行任务。\n" +
           "-不使用游戏对象。查找带有标签的游戏对象。\n" +
           "-没有选定对象。手动查找游戏对象。\n" +
           "-我只需要脚本正文。不要添加任何解释。\n" +
           "任务描述如下：\n" + input;
    
    /// <summary>
    /// 运行生成
    /// </summary>
    void RunGenerator()
    {
        var code = OpenAIUtil.InvokeChat(WrapPrompt(_prompt));
        Debug.Log("AI command script:" + code);
        CreateScriptAsset(code);
    }

    #endregion

    #region 编辑器GUI

    string _prompt = "在随机点创建100个立方体";

    const string ApiKeyErrorText =
      "尚未设置API密钥。请检查项目设置 " +
      "(Edit > Project Settings > AI Command > API Key).";

    /// <summary>
    /// 是否设置 api key
    /// </summary>
    bool IsApiKeyOk
      => !string.IsNullOrEmpty(AICommandSettings.instance.apiKey);

    [MenuItem("Window/AI Command")]
    static void Init() => GetWindow<AICommandWindow>(true, "AI Command");

    void OnGUI()
    {
        if (IsApiKeyOk)
        {
            // 输入框
            _prompt = EditorGUILayout.TextArea(_prompt, GUILayout.ExpandHeight(true));
            // 运行
            if (GUILayout.Button("Run")) RunGenerator();
        }
        else
        {
            // 提示
            EditorGUILayout.HelpBox(ApiKeyErrorText, MessageType.Error);
        }
    }

    #endregion

    #region 脚本生命周期

    void OnEnable()
      => AssemblyReloadEvents.afterAssemblyReload += OnAfterAssemblyReload;

    void OnDisable()
      => AssemblyReloadEvents.afterAssemblyReload -= OnAfterAssemblyReload;

    void OnAfterAssemblyReload()
    {
        if (!TempFileExists) return;
        // log
        Debug.Log("执行菜单项 Edit/Do Task");
        // 执行菜单项
        EditorApplication.ExecuteMenuItem("Edit/Do Task");
        // 删除脚本
        AssetDatabase.DeleteAsset(TempFilePath);
    }

    #endregion
}

} // namespace AICommand

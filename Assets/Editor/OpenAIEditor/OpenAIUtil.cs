using UnityEngine;
using UnityEditor;
using UnityEngine.Networking;

namespace AICommand {

static class OpenAIUtil
{
    /// <summary>
    /// 创建聊天请求体
    /// </summary>
    /// <param name="prompt"></param>
    /// <returns></returns>
    static string CreateChatRequestBody(string prompt)
    {
        var settings = AICommandSettings.instance;
        
        var msg = new OpenAI.RequestMessage();
        msg.role = "user";
        msg.content = prompt;

        var req = new OpenAI.Request();
        req.model = settings.modelKey;
        req.messages = new [] { msg };

        return JsonUtility.ToJson(req);
    }
    
    /// <summary>
    /// 调用
    /// </summary>
    /// <param name="prompt"></param>
    /// <returns></returns>
    public static string InvokeChat(string prompt)
    {
        var settings = AICommandSettings.instance;

        // 创建UnityWebRequest对象
        UnityWebRequest request = new UnityWebRequest(settings.apiUrl, "POST");

        // 设置请求头
        request.SetRequestHeader("Content-Type", "application/json");

        // 设置请求体
        byte[] bodyRaw = System.Text.Encoding.UTF8.GetBytes(CreateChatRequestBody(prompt));
        request.uploadHandler = new UploadHandlerRaw(bodyRaw);
        request.downloadHandler = new DownloadHandlerBuffer();
        
        // 请求超时设置
        request.timeout = settings.timeout;

        // API密钥授权
        request.SetRequestHeader("Authorization", "Bearer " + settings.apiKey);

        // 开始请求
        var req = request.SendWebRequest();
        
        // d等待
        while (req.progress < 1)
        {
            EditorUtility.DisplayProgressBar
                ("AI Command", $"Generating... {req.progress}%", req.progress);
            System.Threading.Thread.Sleep(100);
        }
        
        // 关闭进度条
        EditorUtility.ClearProgressBar();

        // 响应提取
        var json = request.downloadHandler.text;
        // log
        Debug.Log("InvokeChat "+json);
        var data = JsonUtility.FromJson<OpenAI.Response>(json);
        // 代码
        var code = data.choices[0].message.content;
        code = code.Replace("```", "//");
        return code;
    }
}

} // namespace AICommand

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
public class TankEditor : EditorWindow
{
    private readonly List<EditorTab> tabs = new List<EditorTab>();

    private int selectedTabIndex = -1;
    private int currentSelectedTabIntex = -1;


    [MenuItem("Tools/编辑器", false, 0)]
    private static void Init()
    {
        var window = GetWindow(typeof(TankEditor));
        window.titleContent = new GUIContent("编辑器");
    }
    /// <summary>
    /// 当对象加载时调用此函数
    /// </summary>
    private void OnEnable()
    {
       // tabs.Add(new EnemyLogicEditor(this));
        tabs.Add(new MapGenerateEditor());
        selectedTabIndex = 0;
    }

    private void OnGUI()
    {
        // 顶部页签
        selectedTabIndex = GUILayout.Toolbar(selectedTabIndex, new[] {"地图编辑"});

        if (selectedTabIndex >= 0 && selectedTabIndex < tabs.Count)
        {
            var selectedEditor = tabs[selectedTabIndex];
            if (selectedTabIndex != currentSelectedTabIntex)
            {
                selectedEditor.OnTabSelected();
                GUI.FocusControl(null);
            }
            selectedEditor.Draw();
            currentSelectedTabIntex = selectedTabIndex;
        }

    }
}

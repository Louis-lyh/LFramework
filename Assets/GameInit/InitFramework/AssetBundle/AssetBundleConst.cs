using System.Collections.Generic;
using UnityEngine;

namespace GameInit.Framework
{
    /// <summary>
    /// ab包信息
    /// </summary>
    public class GameAbInfo
    {
        /// <summary>
        /// ab包名称
        /// </summary>
        public string AbName;
        /// <summary>
        /// md5
        /// </summary>
        public string AbMd5;
        /// <summary>
        /// 文件大小
        /// </summary>
        public long FileSize;
        /// <summary>
        /// 文件版本
        /// </summary>
        public long FileVer;
        
        /// <summary>
        /// 包含的文件路径
        /// </summary>
        public List<string> IncludeFiles { get; } = new List<string>();
        private string _includeFileToString;
        public string IncludeFileToString => _includeFileToString;
        
        /// <summary>
        /// 添加文件
        /// </summary>
        /// <param name="file"></param>
        public void AddFile(string file)
        {
            if (IncludeFiles.IndexOf(file) != -1)
                return;
            IncludeFiles.Add(file);
            
            if (string.IsNullOrEmpty(_includeFileToString))
                _includeFileToString = file;
            else
                _includeFileToString += ("," + file);
        }
        
        /// <summary>
        /// 解析
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public bool Parse(string value)
        {
            var data = value.Split('|');
            if (data.Length < 5)
                return false;
            
            AbName = data[0];
            AbMd5 = data[1];
            FileSize = long.Parse(data[2]);
            FileVer = long.Parse(data[3]);
            var includeFiles = data[4].Split(',');
            foreach (var file in includeFiles)
            {
                if(string.IsNullOrEmpty(file))
                    continue;
                IncludeFiles.Add(file);
            }
            return true;
        }
    }
    
    public static class AssetBundleConst
    {
        #region StreamingAssets相关路径
        /// 资源路径
        private static string StreamingAssetsPath => Application.streamingAssetsPath + "/" + Platform + "/";
        /// AssetBundle资源路径
        public static string StreamingABPath => StreamingAssetsPath + AssetsDirectory;
        /// Manifest文件路径
        public static string StreamingManifestFile => StreamingABPath + "/" + AssetsDirectory;
        /// 资源版本号文件
        public static string StreamingVersionFile => StreamingAssetsPath + VersionFile;
        /// AB资源信息文件名字
        public static string StreamingAbAssetFile => StreamingAssetsPath + AbAssetsInfoFile;

        #endregion

        #region PersistentAsset路径相关
        /// <summary>
        /// 数据持久化路径
        /// </summary>
        public static string PersistentDataPath => Application.persistentDataPath + "/" + Platform + "/";
        /// AssetBundle资源路径 （数据持久化路径）
        public static string PersistentABPath => PersistentDataPath + AssetsDirectory;
        /// Manifest文件路径 （数据持久化路径）
        public static string PersistentManifestFile => PersistentABPath + "/" + AssetsDirectory;
        /// 资源版本号文件 （数据持久化路径）
        public static string PersistentVersionFile => PersistentDataPath + VersionFile;
        /// 所有AB资源信息文件 （数据持久化路径）
        public static string PersistentAbAssetFile => PersistentDataPath + AbAssetsInfoFile;
        #endregion

        public static readonly string GatewayFile = "GatewayConfig.json";
        
        /// 资源版本号文件名
        public static readonly string VersionFile = "Ver.dis";

        /// AB资源信息文件名字
        public static readonly string AbAssetsInfoFile = "ABAssetsInfo.dis";

        /// AB资源文件夹
        public static readonly string AssetsDirectory = "GameRes";

#if UNITY_ANDROID
        public static string Platform => "Android";
#elif UNITY_IOS 
        public static string Platform => "IOS";
#elif UNITY_STANDALONE_OSX
        public static string Platform => "Mac";
#elif UNITY_WEBGL
        public static string Platform => "WEBGL";
#else
        public static string Platform => "Windows";
#endif
    }
}
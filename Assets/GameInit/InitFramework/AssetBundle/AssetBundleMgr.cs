using System;
using System.Collections.Generic;
using System.IO;
using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using Object = UnityEngine.Object;

namespace GameInit.Framework
{
    public class AssetBundleMgr : Singleton<AssetBundleMgr>
    {
        /// 所有AssetBundle的清单。
        private AssetBundleManifest _abManifest;

        /// 资源路径对应AssetBundle key : 资源路径, value : ab包名字
        private Dictionary<string, string> _assetPath2BundleName;
        
        /// 所有已经加载过的assetBundle， key: ab名字， value：ab包
        private Dictionary<string, AssetBundle> _allLoadedBundles;

        /// assetBundle引用资源 key: ab名字 ，value : 使用次数
        private Dictionary<string, int> _dictBundleCnt;
        
        #region 初始化配置数据

        /// <summary>
        /// 版本号
        /// </summary>
        public static long LastAssetVersion =>_streamingVersion > _persistentVersion ? _streamingVersion : _persistentVersion;
        /// <summary>
        /// 资源版本号 （自带资源文件）
        /// </summary>
        private static long _streamingVersion;

        /// <summary>
        /// 资源版本号 （热更的资源文件）
        /// </summary>
        private static long _persistentVersion;

        /// <summary>
        /// AssetBundle名字和文件结构（bundle名字，MD5，资源号）的对应 （自带资源文件）
        /// </summary>
        private static Dictionary<string, GameAbInfo> _streamingStructDic;

        /// <summary>
        /// AssetBundle名字和文件结构（bundle名字，MD5，资源号）的对应 （热更的资源文件）
        /// </summary>
        private static Dictionary<string, GameAbInfo> _persistentStructDic;
        #endregion

        #region 初始化
        
        /// <summary>
        /// 初始化AB包
        /// </summary>
        /// <returns></returns>
        public async UniTask Init()
        {
            _streamingStructDic = new Dictionary<string, GameAbInfo>();
            _persistentStructDic = new Dictionary<string, GameAbInfo>();
            
            //加载StreamingAssets下的版本号和文件清单
            await LoadStreamingAssetsConfig();

            //加载PersistentData下的版本号和文件清单
            LoadPersistentDataConfig();
            
            // ------ 选取版本最高的资源作为路径来加载配置文件 ------
            // manifest文件路径
            var manifestFile = "";
            // ab包配置信息
            Dictionary<string, GameAbInfo> gameAbInfosDic = null;
            // 防止PersistentData文件夹内的版本号较高但并未有资源文件
            var assetsDirectory = AssetBundleConst.AssetsDirectory;
            if (_streamingVersion < _persistentVersion && _persistentStructDic.ContainsKey(assetsDirectory))
            {
                manifestFile = AssetBundleConst.PersistentManifestFile;
                gameAbInfosDic = _streamingStructDic;
            }
            else
            {
                gameAbInfosDic = _streamingStructDic;
                manifestFile = AssetBundleConst.StreamingManifestFile;
            }
            
            // 初始化数据
            await InitData(gameAbInfosDic,manifestFile);
        }
        
        /// <summary>
        /// 初始化数据
        /// </summary>
        /// <returns></returns>
        private async UniTask InitData(Dictionary<string, GameAbInfo> gameAbInfosDic,string manifestFile)
        {
            // 所有已经加载过的assetBundle
            _allLoadedBundles = new Dictionary<string, AssetBundle>();
            // assetBundle引用资源
            _dictBundleCnt = new Dictionary<string, int>();
            // 资源路径对应AssetBundle
            _assetPath2BundleName = new Dictionary<string, string>();
            
            // 解析AB资源信息文件
            foreach (var rec in gameAbInfosDic.Values)
            {
                // 没有文件路径跳过
                if (rec.IncludeFiles.Count == 0)
                    continue;

                for (int i = 0; i < rec.IncludeFiles.Count; i++)
                {
                    if (string.IsNullOrEmpty(rec.IncludeFiles[i]))
                        continue;
                    
                    var filename = rec.IncludeFiles[i].ToLower();
                    if (_assetPath2BundleName.ContainsKey(filename))
                    {
                        Debug.Log($"[AssetBundleUtil.LoadBundle() => filename:{filename}, abName:{rec.AbName }, oldName:{_assetPath2BundleName[filename]}]");
                        continue;
                    }
                    // 记录文件对应的ab包名字
                    _assetPath2BundleName.Add(filename, rec.AbName);
                }
            }
            
            // 加载Manifest文件
            var bundle = await LoadFromFileAsync(manifestFile);
            // 这里bundle可能加载不到，需要进行修复处理，直接退出游戏
            if (bundle == null)
                CleanPersistent();
            else
            {
                // 获得所有AssetBundle的清单。
                _abManifest = bundle.LoadAsset<AssetBundleManifest>("AssetBundleManifest");
                bundle.Unload(false);
            }
        }

        /// <summary>
        /// 加载StreamingAssets文件夹先的配置
        /// </summary>
        private async UniTask LoadStreamingAssetsConfig()
        {
            // StreamingAssets下的版本号路径
            string verFilePath = AssetBundleConst.StreamingVersionFile;
            // StreamingAssets下的 ab资源信息文件路径
            string abAssetFile = AssetBundleConst.StreamingAbAssetFile;
            
            // if (Application.platform != RuntimePlatform.Android)
            // {
            //     verFilePath = "file://" + verFilePath;
            //     abAssetFile = "file://" + abAssetFile;
            // }
            // Log
            Logger.LogMagenta("AssetBundleMgr.Init() => verFilePath:" + verFilePath);
            // 加载StreamingAssets下的版本号
            var uri = new Uri(verFilePath);
            var req = await UnityWebRequest.Get(uri.AbsoluteUri).SendWebRequest();
            var text = req.downloadHandler?.text;
            // 版本号
            _streamingVersion = AssetBundleUtils.DecodeVersion(text);
            // 销毁请求
            req.Dispose();
            // Log
            Logger.LogMagenta("AssetBundleMgr.Init() => StreamingAssets目录下资源版版本是:" + _streamingVersion);
            
            // 加载StreamingAssets下的ab资源信息文件
            uri = new Uri(abAssetFile);
            req = await UnityWebRequest.Get(uri.AbsoluteUri).SendWebRequest();
            // 配置信息
            _streamingStructDic = AssetBundleUtils.DecodeStringPair(req.downloadHandler?.data);
            // 销毁请求
            req.Dispose();
        }
        /// <summary>
        /// PersistentData 文件夹下的配置
        /// </summary>
        private void LoadPersistentDataConfig()
        {
            // PersistentData下的版本号
            var verFilePath = AssetBundleConst.PersistentVersionFile;
            // PersistentData下的ab资源信息文件
            var abAssetFile = AssetBundleConst.PersistentAbAssetFile;

            // 加载PersistentData下的版本号
            if (File.Exists(verFilePath))
                _persistentVersion = AssetBundleUtils.DecodeVersion(File.ReadAllText(verFilePath));
            // Log
            Logger.LogMagenta("AssetBundleMgr.Init() => PersistentAssets目录下资源版版本是:" + _persistentVersion);
            
            // 加载persistentData下的ab资源信息文件
            if (File.Exists(abAssetFile))
                _persistentStructDic = AssetBundleUtils.DecodeStringPair(File.ReadAllBytes(abAssetFile));
        }


        #endregion
        
        #region 加载资源
        
        #region 同步加载资源
        /// <summary>
        /// 同步加载资源
        /// </summary>
        /// <param name="assetPath"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public T LoadAssetSync<T>(string assetPath) where T : Object
        {
            // 资源扩展名
            var ext = Path.GetExtension(assetPath);
            // 不带扩展名路径
            var path = assetPath.Replace(ext, "");
            
            // 取得ab包名
            if (_assetPath2BundleName.TryGetValue(path.ToLower(), out var bundleName))
            {
                // 加载ab包
                var ab = LoadAssetBundleSync(bundleName);
                if (ab != null)
                {
                    // 不包含扩展名路径
                    assetPath = Path.GetFileNameWithoutExtension(assetPath);
                    // 加载资源
                    return ab.LoadAsset<T>(assetPath);
                }
            }

            Debug.LogError("AssetBundleMgr.LoadAssetSync() => 加载资源失败，资源路径找不到对应的AssetBundle, assetPath:" + assetPath);
            return null;
        }

        /// <summary>
        /// 同步加载assetBundle
        /// </summary>
        /// <param name="bundleName">ad包名</param>
        /// <returns></returns>
        private AssetBundle LoadAssetBundleSync(string bundleName)
        {
            // 内存不中存在
            if (!_allLoadedBundles.TryGetValue(bundleName, out var assetBundle))
            {
                // 获取ab包路径
                var abPath = GetAssetBundlePath(bundleName);
                // 加载ab包
                assetBundle = AssetBundle.LoadFromFile(abPath);
                // 记录
                _allLoadedBundles[bundleName] = assetBundle;
            }
            
            // 记录ab包使用次数
            IncBundleDependCnt(bundleName);
            // 加载依赖
            var depends = _abManifest.GetDirectDependencies(bundleName);
            foreach (var depend in depends)
                LoadAssetBundleSync(depend);
            
            return assetBundle;
        }
        #endregion
        
        #region 异步加载资源
        
        /// <summary>
        /// 异步加载资源
        /// </summary>
        /// <param name="assetPath">资源路径</param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public async UniTask<T> LoadAssetAsync<T>(string assetPath) where T : Object
        {
            // 获取扩展名
            var ext = Path.GetExtension(assetPath);
            // 路径移除扩展名
            var path = assetPath.Replace(ext, "");
            // 获取 ab包名字
            if (!_assetPath2BundleName.TryGetValue(path.ToLower(), out var bundleName))
            {
                Debug.LogError("AssetBundleMgr.LoadAssetAsync() => 找不到资源路径为:" + assetPath + "对应的ab名字");
                return null;
            }
            // 异步加载ab包
            var assetBundle = await LoadAssetBundleAsync(bundleName);
            if (assetBundle == null)
                return null;
            var asset = await assetBundle.LoadAssetAsync(Path.GetFileNameWithoutExtension(assetPath));
            return (T) asset;
        }
        
        /// <summary>
        /// 异步加载场景
        /// </summary>
        /// <param name="sceneName"></param>
        /// <param name="loadMode"></param>
        /// <returns></returns>
        public async UniTask<AsyncOperation> LoadSceneAsync(string sceneName, LoadSceneMode loadMode = LoadSceneMode.Additive)
        {
            // 获取报名
            if (!_assetPath2BundleName.TryGetValue(sceneName.ToLower(), out var bundleName))
            {
                Logger.LogError("AssetBundleMgr.LoadSceneAsync() => 找不到资源路径为:" + sceneName + "对应的ab名字");
                return null;
            }
            // 异步加载ab包
            await LoadAssetBundleAsync(bundleName);
            // 加载场景
            return SceneManager.LoadSceneAsync(sceneName, loadMode);
        }
        /// <summary>
        /// 卸载场景
        /// </summary>
        /// <param name="sceneName"></param>
        /// <returns></returns>
        public AsyncOperation UnloadSceneAsync(string sceneName)
        {
            // 获得包名
            if (!_assetPath2BundleName.TryGetValue(sceneName.ToLower(), out var bundleName))
            {
                Logger.LogMagenta("AssetBundleMgr.UnloadSceneAsync() => 找不到sceneName:" + sceneName + "对应的ab资源");
                return SceneManager.UnloadSceneAsync(sceneName);
            }
            // 卸载ab包
            UnloadAssetBundle(bundleName);
            // 卸载场景
            return SceneManager.UnloadSceneAsync(sceneName);
        }

        // 移除加载ab包等待事件
        private static Dictionary<string, List<AutoResetUniTaskCompletionSource>> _tmpLoadTask = new Dictionary<string, List<AutoResetUniTaskCompletionSource>>();
        
        /// <summary>
        /// 异步加载ab包
        /// </summary>
        /// <param name="bundleName"></param>
        /// <returns></returns>
        private async UniTask<AssetBundle> LoadAssetBundleAsync(string bundleName)
        {
            // 内存中不存在
            if (!_allLoadedBundles.TryGetValue(bundleName, out var assetBundle))
            {
                // 获取ab包路径
                var abPath = GetAssetBundlePath(bundleName);
                
                // ab包已经加载中
                if (_tmpLoadTask.ContainsKey(abPath))
                {
                    // 创建等待事件
                    var waiteTask = AutoResetUniTaskCompletionSource.Create();
                    _tmpLoadTask[abPath].Add(waiteTask);
                    // 等待
                    await waiteTask.Task;
                    // 完成获得ab包
                    assetBundle = _allLoadedBundles[bundleName];
                }
                // 没有加载
                else
                {
                    // 创建等待事件列表
                    _tmpLoadTask[abPath] = new List<AutoResetUniTaskCompletionSource>();
                    // 异步加载ab包
                    assetBundle = await LoadFromFileAsync(abPath);
                    if (assetBundle != null)
                        _allLoadedBundles[bundleName] = assetBundle;
                    // 加载完成，完成等待列表中的等待事件
                    foreach (var value in _tmpLoadTask[abPath])
                        value.TrySetResult();
                    _tmpLoadTask.Remove(abPath);
                }
            }
            // 记录ab包使用次数
            IncBundleDependCnt(bundleName);
            
            // 加载依赖
            var dependencies = _abManifest.GetAllDependencies(bundleName);
            foreach (var dependency in dependencies)
            {
                // 内存存在跳过
                if (_allLoadedBundles.ContainsKey(dependency))
                    continue;
                // 异步加载ab包
                await LoadAssetBundleAsync(dependency);
            }

            return assetBundle;
        }
        
        /// <summary>
        /// 异步加载ab包
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        private async UniTask<AssetBundle> LoadFromFileAsync(string path)
        {
#if UNITY_WEBGL
            Logger.LogBlue("LoadFromFileAsync : "+path);
            UnityWebRequest abRequest3 = UnityWebRequestAssetBundle.GetAssetBundle(path);
            await abRequest3.SendWebRequest();
            return DownloadHandlerAssetBundle.GetContent(abRequest3);
#else
            return await AssetBundle.LoadFromFileAsync(path);
#endif
            return await AssetBundle.LoadFromFileAsync(path);
        }
        
        #endregion
        
        /// <summary>
        /// 卸载资源
        /// </summary>
        /// <param name="assetPath"></param>
        /// <param name="isForce"></param>
        public void UnloadRes(string assetPath, bool isForce = false)
        {
            if (_assetPath2BundleName.TryGetValue(assetPath, out var bundleName))
            {
                UnloadAssetBundle(bundleName, isForce);
            }
        }
        /// <summary>
        /// 卸载ab包
        /// </summary>
        /// <param name="bundleName"></param>
        /// <param name="needForceUnload"></param>
        private void UnloadAssetBundle(string bundleName, bool needForceUnload = false)
        {
            // 获得依赖
            string[] depBundles = _abManifest.GetDirectDependencies(bundleName);
            // 卸载依赖
            for (int i = 0, max = depBundles.Length; i < max; i++)
            {
                var depBundleName = depBundles[i];
                UnloadAssetBundle(depBundleName, needForceUnload);
            }
            // 卸载ab包
            DecBundleDependCnt(bundleName, needForceUnload);
        }
        /// <summary>
        /// 记录ab使用次数
        /// </summary>
        /// <param name="bundleName"></param>
        private void IncBundleDependCnt(string bundleName)
        {
            if (!_dictBundleCnt.ContainsKey(bundleName))
            {
                _dictBundleCnt[bundleName] = 0;
            }

            _dictBundleCnt[bundleName]++;
        }
        
        /// <summary>
        /// 卸载ab包
        /// </summary>
        /// <param name="bundleName">包名</param>
        /// <param name="needForceUnload">强制卸载</param>
        private void DecBundleDependCnt(string bundleName, bool needForceUnload = false)
        {
            if (_dictBundleCnt.ContainsKey(bundleName))
            {
                // 使用次数减1
                _dictBundleCnt[bundleName]--;
                // 剩余次数
                var lastCnt = needForceUnload ? 0 : _dictBundleCnt[bundleName];
                // 剩余次数为零卸载
                if (lastCnt == 0)
                {
                    _dictBundleCnt.Remove(bundleName);
                    // 从内存中拿到ab包
                    var bundle = _allLoadedBundles[bundleName];
                    try
                    {
                        // 卸载
                        bundle.Unload(true);
                    }
                    catch (Exception e)
                    {
                        Logger.LogMagenta("AssetBundleMgr.DecBundleDependCnt:释放ab出错，error:" + e.Message);
                    }
                    // 从内存中移除
                    _allLoadedBundles.Remove(bundleName);
                }
            }
        }
        /// <summary>
        /// 获得ab包路径
        /// </summary>
        /// <param name="assetBundle"></param>
        /// <returns></returns>
        private string GetAssetBundlePath(string assetBundle)
        {
            // 自带文件
            if (!_streamingStructDic.TryGetValue(assetBundle, out var streamingRec))
            {
                streamingRec = new GameAbInfo();
                streamingRec.AbMd5 = String.Empty;
                streamingRec.AbName = assetBundle;
                streamingRec.FileSize = 0;
                streamingRec.FileVer = 0;
                _streamingStructDic.Add(assetBundle, streamingRec);
            }    
            // 热更文件
            _persistentStructDic.TryGetValue(assetBundle, out var persistentRec);
            
            var pathEnd = "/" + assetBundle;
            
            // 如果因为AB被全部清空后，重新生成所有AB时，md5可能不会变化，但是ver会变化，此时，直接取streaming目录下的资源
            if (persistentRec == null || streamingRec.AbMd5.Equals(persistentRec.AbMd5))
                return AssetBundleConst.StreamingABPath + pathEnd;
            
            // 选择版本高的
            var path = streamingRec.FileVer < persistentRec.FileVer
                ? AssetBundleConst.PersistentABPath + pathEnd
                : AssetBundleConst.StreamingABPath + pathEnd;
            
            return path;
        }
        
        /// <summary>
        /// 判断资源是否存在
        /// </summary>
        /// <param name="assetPath">资源路径</param>
        /// <returns></returns>
        public bool AssetExists(string assetPath)
        {
            var ext = Path.GetExtension(assetPath);
            var path = assetPath.Replace(ext, "");
            return _assetPath2BundleName.ContainsKey(path.ToLower());
        }
        
        #endregion
        
        #region 下载远程资源必要辅助接口。比如获取文件的最新MD5和version

        /// <summary>
        /// 获取Bundle的最新版本号
        /// </summary>
        /// <param name="bundleName"></param>
        /// <returns></returns>
        public static long GetLastestAssetVer(string bundleName)
        {
            _streamingStructDic.TryGetValue(bundleName, out var streamingRec);
            _persistentStructDic.TryGetValue(bundleName, out var persistentRec);

            if (persistentRec == null && streamingRec == null)
                return 0;
            if (persistentRec != null && streamingRec == null)
                return persistentRec.FileVer;

            if (persistentRec == null && streamingRec != null)
                return streamingRec.FileVer;

            var sv = streamingRec.FileVer;
            var pv = persistentRec.FileVer;
            return sv > pv ? sv : pv;
        }
        
        /// <summary>
        /// 获取ab包的MD5信息
        /// </summary>
        /// <param name="bundleName"></param>
        /// <returns></returns>
        public static string GetLastestMD5(string bundleName)
        {
            _streamingStructDic.TryGetValue(bundleName, out var streamingRec);
            _persistentStructDic.TryGetValue(bundleName, out var persistentRec);

            if (persistentRec == null && streamingRec == null)
                return null;
            
            if (persistentRec != null && streamingRec == null)
                return persistentRec.AbMd5;
            
            if (persistentRec == null && streamingRec != null)
                return streamingRec.AbMd5;

            var sv = streamingRec.FileVer;
            var pv = persistentRec.FileVer;
            // 返回版本号高的MD5
            return sv > pv ? streamingRec.AbMd5 : persistentRec.AbMd5;
        }

        /// <summary>
        /// 更新MD5文件
        /// </summary>
        public static void UpdateMD5ToPersistentFile(GameAbInfo remoteAbInfo)
        {
            _persistentStructDic[remoteAbInfo.AbName] = remoteAbInfo;
        }

        /// <summary>
        /// 更新MD5文件并写入
        /// </summary>
        public static void WriteMD5ToPersistentFile(GameAbInfo remoteAbInfo, bool security = false)
        {
            if (!Directory.Exists(AssetBundleConst.PersistentDataPath))
                Directory.CreateDirectory(AssetBundleConst.PersistentDataPath);
            
            // 更新MD5文件
            UpdateMD5ToPersistentFile(remoteAbInfo);
            // 修改ab包信息文件
            File.WriteAllBytes(AssetBundleConst.PersistentAbAssetFile, AssetBundleUtils.EncodeStringPair(_persistentStructDic, security));
        }

        #endregion

        private void CleanPersistent()
        {
            if (Directory.Exists(AssetBundleConst.PersistentDataPath))
            {
                Directory.Delete(AssetBundleConst.PersistentDataPath, true);
            }
#if !UNITY_EDITOR
            Application.Quit();
#endif
        }
    }
}
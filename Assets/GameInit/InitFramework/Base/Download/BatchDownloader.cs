﻿using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;

namespace GameInit.Framework
{
    public enum DownloadStep
    {
        CdnUrl = 0,
        BackupCdnUrl,
        BackToSource,
    }
    
    public struct DownloadInfo
    {
        public string url;
        public string dir;
        public string fileName;
        public long fileSize;
        public string fileMD5;
        public string fileVer;
        // public string backupUrl;
    }

    /// <summary>
    /// 批量下载工具
    /// </summary>
    public class BatchDownloader
    {
        /// <summary>
        /// 下载总进度
        /// </summary>
        public float contentProgress
        {
            get
            {
                return (float)downloadedContentSize / contentSize;
            }
        }
        /// <summary>
        /// 更新文件的总大小
        /// </summary>
        public long contentSize { get; private set; }
        /// <summary>
        /// 已下载的文件总大小
        /// </summary>
        public long downloadedContentSize
        {
            get
            {
                if (_unityWebRequest  == null || !_loadYieldInstruction.keepWaiting)
                {
                    return _downloadedContentSize;
                }
                else
                {
                    return _downloadedContentSize + currentDownloadedContentSize;
                }
            }
        }

        /// <summary>
        /// 当前下载的文件的Url
        /// </summary>
        public string CurrentDownUrl { get; private set; }

        /// <summary>
        /// 当前下载设置的超时时间
        /// </summary>
        public int CurrentTimeOut { get; private set; }

        /// <summary>
        /// 当前文件的文件名
        /// </summary>
        public string CurrentContentName { get; private set; }

        /// <summary>
        /// 当前文件大小
        /// </summary>
        private long currentContentSize;// { get; private set; }
        /// <summary>
        /// 当前文件MD5
        /// </summary>
        public string currentContentMD5 { get; private set; }
        /// <summary>
        /// 当前文件的下载进度
        /// </summary>
        public float currentContentProgress
        {
            get
            {
                return _unityWebRequest.downloadProgress;
            }
        }
        /// <summary>
        /// 当前文件已下载的大小
        /// </summary>
        public long currentDownloadedContentSize
        {
            get
            {
                return (long)(currentContentProgress * currentContentSize);
            }
        }
        /// <summary>
        /// 当前文件是否下载完毕
        /// </summary>
        public bool isCurrentLoadOk
        {
            get
            {
                return !_loadYieldInstruction.keepWaiting;
            }
        }

        /// <summary>
        /// 总文件数量
        /// </summary>
        public int totalFileCnt { get; private set; }

        /// <summary>
        /// 当前文件序号
        /// </summary>
        public int currentFileIndex
        {
            get
            {
                return totalFileCnt - _downloadInfoQueue.Count + 1;
            }
        }
        /// <summary>
        /// 错误信息
        /// </summary>
        public string error { get; private set; }

        /// <summary>
        /// 下载信息队列
        /// </summary>
        private readonly Queue<DownloadInfo> _downloadInfoQueue;
        /// <summary>
        /// 下载完成哨兵
        /// </summary>
        private readonly InternalYieldInstruction _loadYieldInstruction;
        /// <summary>
        /// 当前下载信息
        /// </summary>
        private DownloadInfo _currentDownloadInfo;
        /// <summary>
        /// 下载请求
        /// </summary>
        private UnityWebRequest _unityWebRequest;
        /// <summary>
        /// 当前下载步骤
        /// </summary>
        private DownloadStep _curDownloadStep;
        /// <summary>
        /// 已经下载得问价大小
        /// </summary>
        private long _downloadedContentSize;

        public BatchDownloader()
        {
            _loadYieldInstruction = new InternalYieldInstruction();
            _downloadInfoQueue = new Queue<DownloadInfo>(100);
            contentSize = 0;
        }
        
        /// <summary>
        /// 添加下载信息
        /// </summary>
        /// <param name="dir"></param>
        /// <param name="url"></param>
        /// <param name="fileName"></param>
        /// <param name="size"></param>
        /// <param name="md5"></param>
        /// <param name="ver"></param>
        public void AddFile(string dir, string url, string fileName, long size, string md5, string ver = "")
        {
            var info = new DownloadInfo();
            info.dir = FileUtil.FormatPath(dir);
            info.url = FileUtil.FormatPath(url);
            info.fileName = fileName;
            info.fileSize = size;
            info.fileMD5 = md5;
            info.fileVer = ver;
            // 加入队列
            _downloadInfoQueue.Enqueue(info);
            // 文件信息数量
            contentSize += size;
            // 文件信息总数
            totalFileCnt = _downloadInfoQueue.Count;
        }
        /// <summary>
        /// 准备下一个文件下载
        /// </summary>
        /// <returns></returns>
        public bool MoveNext()
        {
            if (_downloadInfoQueue.Count > 0)
            {
                // 取出下载信息
                _currentDownloadInfo = _downloadInfoQueue.Dequeue();
                // 返回指定路径字符串中的文件名和扩展名。
                CurrentContentName = Path.GetFileName(_currentDownloadInfo.fileName);
                // 文件大小
                currentContentSize = _currentDownloadInfo.fileSize;
                // MD5
                currentContentMD5 = _currentDownloadInfo.fileMD5;
                return true;
            }
            return false;
        }

        /// <summary>
        /// 开始下载当前的文件。可以指定覆盖原文件还是断点续传
        /// </summary>
        public CustomYieldInstruction LoadCurrent(bool overrideFile, bool urlGet)
        {
            // 没有等待开始下载
            if (!_loadYieldInstruction.keepWaiting)
            {
                error = string.Empty;
                _curDownloadStep = DownloadStep.CdnUrl;
                // 请求下载
                RequestDownload();
            }

            return _loadYieldInstruction;
        }
        
        /// <summary>
        /// 下载成功
        /// </summary>
        private void OnDownloadFinish()
        {
            // 文件路径
            string fullPath = _currentDownloadInfo.dir + _currentDownloadInfo.fileName;
            // 创建路径
            if (!Directory.Exists(_currentDownloadInfo.dir))
                Directory.CreateDirectory(_currentDownloadInfo.dir);
            
            // 写入文件
            File.WriteAllBytes(fullPath, _unityWebRequest.downloadHandler.data);
            // 完成数量加一
            _downloadedContentSize += currentContentSize;
            // 取消等待
            _loadYieldInstruction.InternalSetWaiting(false);
            // 销毁请求
            _unityWebRequest.Dispose();
        }
        
        /// <summary>
        /// 下载失败
        /// </summary>
        /// <param name="errorMsg"></param>
        private void OnDownloadFailed(string errorMsg)
        {
            // 销毁请求
            _unityWebRequest.Dispose();
            
            if (_curDownloadStep == DownloadStep.BackToSource)
            {
                error = errorMsg;
                _loadYieldInstruction.InternalSetWaiting(false);
            }
            else
            {
                // 进去下一阶段
                _curDownloadStep = _curDownloadStep == DownloadStep.CdnUrl ? DownloadStep.BackupCdnUrl : DownloadStep.BackToSource;
                // 重新下载
                RequestDownload();
            }
        }
        
        /// <summary>
        /// 请求下载
        /// </summary>
        private void RequestDownload()
        {
            string url = "";
            int timeOut = 10;
            switch (_curDownloadStep)
            {
                case DownloadStep.CdnUrl:
                    timeOut = 120;
                    url = _currentDownloadInfo.url + _currentDownloadInfo.fileName;
                    break;
                case DownloadStep.BackupCdnUrl:
                    timeOut = 180;
                    url = _currentDownloadInfo.url + _currentDownloadInfo.fileName;
                    break;
                case DownloadStep.BackToSource:
                    timeOut = 240;
                    url = _currentDownloadInfo.url + _currentDownloadInfo.fileName;
                    break;
            }

            if (!string.IsNullOrEmpty(_currentDownloadInfo.fileVer))
            {
                url = url + "_" + _currentDownloadInfo.fileVer;
                if (_curDownloadStep == DownloadStep.BackupCdnUrl)
                    url = url.UrlFormat();
            }
            else
            {
                url = url.UrlFormat();
            }
            
            // 当前地址
            CurrentDownUrl = url;
            // 当前超时时间
            CurrentTimeOut = timeOut;
            // 创建 http 请求
            _unityWebRequest = UnityWebRequest.Get(url);
            // 发送请求
            _unityWebRequest.SendWebRequest(OnDownloadFinish, OnDownloadFailed, CurrentTimeOut);
            // 等待
            _loadYieldInstruction.InternalSetWaiting(true);
        }
    }
}
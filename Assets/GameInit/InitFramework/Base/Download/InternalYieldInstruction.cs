﻿using UnityEngine;

namespace GameInit.Framework
{
    /// <summary>
    /// 下载哨兵
    /// </summary>
    internal class InternalYieldInstruction : CustomYieldInstruction
    {
        private bool b_Waiting;

        internal void InternalSetWaiting(bool waiting)
        {
            b_Waiting = waiting;
        }

        public override bool keepWaiting
        {
            get
            {
                return b_Waiting;
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;

namespace GameInit.Framework
{
    /// <summary>
    /// 字符串扩展
    /// </summary>
    public static class StringExtensions
    {
        /// <summary>
        /// 在url的尾部加上时间戳
        /// URL 的末尾追加了时间。这就确保了请求不会在它第一次被发送后即缓存，而是会在此方法每次被调用后重新创建和重发；
        /// 此 URL 会由于时间戳的不同而稍微有些不同。
        /// 这种技巧常被用于确保到脚本的 POST 每次都会实际生成新请求且 Web 服务器不会尝试缓存来自服务器的响应。
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static string UrlFormat(this string url)
        {
            string t = DateTime.Now.Ticks.ToString();
            if (url.Contains("?"))
            {
                return url + "&t=" + t;
            }
            else
            {
                return url + "?t=" + t;
            }
        }
        
        /// <summary>
        /// 字符串 转整型列表
        /// </summary>
        /// <param name="self">字符串</param>
        /// <param name="splitChar">间隔字符</param>
        /// <returns></returns>
        public static List<int> ToListInt(this string self, char splitChar = '|')
        {
            if (!self.IsValid())
                return null;
            var result = new List<int>();
            var temp = self.Split(splitChar);
            foreach (var v in temp)
            {
                int.TryParse(v, out var value);
                result.Add(value);
            }
            return result;
        }
        
        /// <summary>
        /// 字符串转浮点列表
        /// </summary>
        /// <param name="self">字符串</param>
        /// <param name="splitChar">间隔字符</param>
        /// <returns></returns>
        public static List<float> ToListFloat(this string self, char splitChar = '|')
        {
            if (!self.IsValid())
                return null;
            var result = new List<float>();
            var temp = self.Split(splitChar);
            foreach (var v in temp)
            {
                float.TryParse(v, out var value);
                result.Add(value);
            }
            return result;
        }
        /// <summary>
        /// 字符串转列表
        /// </summary>
        /// <param name="self">字符串</param>
        /// <param name="splitChar">间隔字符</param>
        /// <returns></returns>
        public static List<string> ToList(this string self, char splitChar = '|')
        {
            var result = new List<string>();
            if (!self.IsValid())
            {
                result.Add(self);
            }
            else
            {
                var temp = self.Split(splitChar);
                foreach (var str in temp)
                    result.Add(str);
            }
            return result;
        }

        public static bool IsValid(this string self)
        {
            return !string.IsNullOrEmpty(self) && self != "0";
        }
    }
}
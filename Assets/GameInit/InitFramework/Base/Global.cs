﻿using System.Collections.Generic;

namespace GameInit.Framework
{
    public partial class Global
    {
        /// <summary>
        /// 需要补充的元数据名称
        /// </summary>
        public static List<string> AOTMetaAssemblyNames { get; } = new List<string>()
        {
            "DOTween.dll",
            "GameInit.dll",
            "System.Core.dll",
            "System.Runtime.CompilerServices.Unsafe.dll",
            "UniTask.dll",
            "UnityEngine.CoreModule.dll",
            "ZString.dll",
            "mscorlib.dll",
        };
    }
}
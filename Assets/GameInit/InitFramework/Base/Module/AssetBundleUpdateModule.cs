﻿using UnityEngine;

namespace GameInit.Framework
{
    /// <summary>
    /// 资源下载模块
    /// </summary>
    public class AssetBundleUpdateModule : ModuleBase
    {
        /// <summary>
        /// 进入
        /// </summary>
        protected override async  void OnEnter()
        { 
            // 不使用ab包退出
            if (!StartConfig.Instance.UserAssetBundle)
            {
#if UNITY_EDITOR
                ResourceManager.SetLoader(new DefaultResLoader());
#endif
                Exit();
                return;
            }
            
            // 使用ab包
            ResourceManager.SetLoader(new BundleResLoader());
            
            // 初始化ab包
            await AssetBundleMgr.Instance.Init();
            // 初始化加载页面
            await LoadingManager.Instance.InitLoadingView();
            // 初始化资源下载管理
            DownloadMgr.Instance.Init();
            
            // 开始下载热更
            if (Application.internetReachability != NetworkReachability.NotReachable && StartConfig.Instance.UserRemoteAssetBundle)
                await DownloadMgr.Instance.StartDownload(true);
            // 退出
            Exit();
        }
        
        protected override void OnExit()
        {
            // 隐藏加载页面
            LoadingManager.Instance.HideLoading();
            
            base.OnExit();
        }
    }
}
﻿namespace GameInit.Framework
{
    public class HotfixBaseEntry
    {
        public virtual void Awake()
        {
            
        }

        public virtual void Start()
        {
            
        }

        public virtual void Update()
        {
            
        }

        public virtual void FixedUpdate()
        {
            
        }
    
        public virtual void LateUpdate()
        {
            
        }

        public virtual void OnDestroy()
        {
            
        }

        public virtual void OnApplicationPause(bool pause)
        {
            
        }

        public virtual void OnApplicationFocus(bool focus)
        {
            
        }

        public virtual void OnApplicationQuit()
        {
            
        }
    }
}
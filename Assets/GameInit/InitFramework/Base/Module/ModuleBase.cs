﻿using System;

namespace GameInit.Framework
{
    /// <summary>
    /// 模块基类
    /// </summary>
    public abstract class ModuleBase : IDisposable
    {
        /// <summary>
        /// 进入
        /// </summary>
        public void Enter()
        {
            OnEnter();
        }
        protected abstract void OnEnter();
        
        /// <summary>
        /// 退出
        /// </summary>
        internal void Exit()
        {
            OnExit();
            ModuleManager.Instance.ModuleEnd(this);
        }
        protected virtual void OnExit()
        {
        }

        public virtual void Dispose()
        {
        }
    }
}
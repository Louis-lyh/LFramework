﻿using System.Collections.Generic;
using GameInit.Framework;

namespace GameInit.Framework
{
    /// <summary>
    /// 模块管理
    /// </summary>
    public class ModuleManager : Singleton<ModuleManager>
    {
        private bool _isInit;
        private List<ModuleBase> _allModule;
        private ModuleBase _curModule;

        /// <summary>
        /// 初始化
        /// </summary>
        public void Init()
        {
            _isInit = true;
            _allModule = new List<ModuleBase>();
        }

        /// <summary>
        /// 插入模块
        /// </summary>
        /// <param name="moduleBase"></param>
        /// <param name="index"></param>
        public void InsertModule(ModuleBase moduleBase, int index = 0)
        {
            if (_allModule.Contains(moduleBase))
            {
                Logger.LogError("重复添加 Module ---->"+moduleBase.GetType());
                return;
            }
            
            _allModule.Insert(index,moduleBase);
        }
        
        /// <summary>
        /// 模块停止
        /// </summary>
        /// <param name="moduleBase"></param>
        internal void ModuleEnd(ModuleBase moduleBase)
        {
            if (_curModule != moduleBase)
                return;
            
            _allModule.Remove(moduleBase);
            _curModule = null;
            
            // 运行下一个
            Run();
        }
        
        /// <summary>
        /// 运行模块
        /// </summary>
        public void Run()
        {
            if (!_isInit || _allModule.Count == 0)
            {
                return;
            }
            _curModule = _allModule[0];
            _curModule.Enter();
        }
    }
}
﻿using System;
using System.Linq;
using System.Reflection;
using Cysharp.Threading.Tasks;
using HybridCLR;
using UnityEngine;

namespace GameInit.Framework
{
    public class StartHotfixModule : ModuleBase
    {
        private static HotfixBaseEntry _hotFixMainEntry;
        protected override async void OnEnter()
        {
            Assembly assembly = null;
#if User_Hybrid 
            if(StartConfig.Instance.HotFixType == StartConfig.LoadDllType.Hotfix)
                // TODO:hybridCLR加载逻辑
                assembly = await LoadHotfixDllByHybridCLR();
            else 
                assembly = AppDomain.CurrentDomain.GetAssemblies().First(ass => ass.GetName().Name == "GameLogic");
#else
            assembly = AppDomain.CurrentDomain.GetAssemblies().First(ass => ass.GetName().Name == "GameLogic");
#endif
            
            if (assembly != null)
            {
                var type = assembly.GetType("GameLogic.Hotfix.HotfixEntry");
                _hotFixMainEntry = (HotfixBaseEntry)Activator.CreateInstance(type);
            }

            if (_hotFixMainEntry != null)
                new GameObject("HotfixEntry").AddComponent<HotFixBehaviour>();
            Exit();
        }

        #region hybridCLR启动逻辑
        
#if User_Hybrid 
        /// <summary>
        /// 加载热更程序集
        /// </summary>
        /// <returns></returns>
        private async UniTask<Assembly> LoadHotfixDllByHybridCLR()
        {
            // 补充元数据
            await LoadMetaDataForAOTAssemblies();
            
            // 加载热更程序集
            var dllAsset = await ResourceManager.LoadAssetAsync<TextAsset>("HotFixDll/GameLogic.dll");
            
            return Assembly.Load(dllAsset.bytes);
        }
        
        /// <summary>
        /// 加载元数据
        /// </summary>
        /// <returns></returns>
        private static async UniTask LoadMetaDataForAOTAssemblies()
        {
            var mode = HomologousImageMode.SuperSet;
            for (var i = 0; i < Global.AOTMetaAssemblyNames.Count; i++)
            {
                var path = "HotFixDll/" + Global.AOTMetaAssemblyNames[i];
                // 加载元数据文件
                var dllAsset = await ResourceManager.LoadAssetAsync<TextAsset>(path);
                if (dllAsset == null)
                {
                    Logger.LogError("[SetupHotfixModule.LoadMetadataForAOTAssemblies() => 加载AOT Dll失败, dllFile:" + Global.AOTMetaAssemblyNames[i] + "]");
                    return;
                }
                // 加载补充元数据assembly
                var err = RuntimeApi.LoadMetadataForAOTAssembly(dllAsset.bytes, mode);
                //失败log
                Logger.Log($"LoadMetadataForAOTAssembly:{Global.AOTMetaAssemblyNames[i]}. mode:{mode} ret:{err}");
            }
        }
#endif
        #endregion

        #region 热更代码生命周期
        /// <summary>
        /// 热更代码生命周期
        /// </summary>
        private class HotFixBehaviour : MonoBehaviour
        {
            private void Awake()
            { 
                DontDestroyOnLoad(gameObject);
                _hotFixMainEntry?.Awake();
            }

            private void Start()
            {
                _hotFixMainEntry?.Start();
            }

            private void Update()
            {
                _hotFixMainEntry?.Update();
            }

            private void FixedUpdate()
            {
                _hotFixMainEntry?.FixedUpdate();
            }

            private void LateUpdate()
            {
                _hotFixMainEntry?.LateUpdate();
            }

            private void OnDestroy()
            {
                _hotFixMainEntry?.OnDestroy();
            }

            private void OnApplicationPause(bool pause)
            {
                _hotFixMainEntry?.OnApplicationPause(pause);
            }

            private void OnApplicationFocus(bool focus)
            {
                _hotFixMainEntry?.OnApplicationFocus(focus);
            }

            private void OnApplicationQuit()
            {
                _hotFixMainEntry?.OnApplicationQuit();
            }
        }

        #endregion
    }
}
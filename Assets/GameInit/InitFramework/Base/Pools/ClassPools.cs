﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace GameInit.Framework
{
    /// <summary>
    /// 类对象池
    /// </summary>
    public class ClassPools<T> where T : IClassPool, new()
    {
        /// <summary>
        /// 类型
        /// </summary>
        public Type DefaultType { get; } = typeof(T);
        /// <summary>
        /// 池子
        /// </summary>
        private readonly List<T> _list = new List<T>();
        
        /// <summary>
        /// 出栈
        /// </summary>
        public T Pop()
        {
            if(_list.Count == 0)
                return new T();

            var result = _list[0];
            _list.Remove(result);
            return result;
        }

        public void Push(T t)
        {
            if (!_list.Contains(t))
            {
                t.Reset();
                _list.Add(t);
            }
            else
            {
                Debug.LogWarning("重复放入对象 " + t.GetType());
            }
        }
    }
}
﻿using System.Collections.Generic;

namespace GameInit.Framework
{
    /// <summary>
    /// 类对象池管理器
    /// </summary>
    public class ClassPoolsManager<T> where T : IClassPool,new()
    {
        /// <summary>
        /// 类对象池子
        /// </summary>
        private static readonly List<ClassPools<T>> list = new List<ClassPools<T>>();
        
        /// <summary>
        /// 出栈
        /// </summary>
        public static T Pop()
        {
            // 找到池子
            var pool = GetPool();
            // 池子不存在新建池子
            if (pool ==null)
            {
                pool = new ClassPools<T>();
                list.Add(pool);
            }
            // 返回对象
            return pool.Pop();
        }

        /// <summary>
        /// 入栈回收对象
        /// </summary>
        public static void Push(T msg)
        {
            var pool = GetPool();
            pool.Push(msg);
        }

        /// <summary>
        /// 获取对应池子
        /// </summary>
        private static ClassPools<T> GetPool()
        {
            for (int i = 0; i < list.Count; i++)
            {
                var item = list[i];
                if (typeof(T) == item.DefaultType)
                    return item;
            }

            return null;
        }

    }
}
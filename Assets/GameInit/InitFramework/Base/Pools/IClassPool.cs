﻿namespace GameInit.Framework
{
    /// <summary>
    /// 类池子接口
    /// </summary>
    public interface IClassPool
    {
        void Reset();
    }
}
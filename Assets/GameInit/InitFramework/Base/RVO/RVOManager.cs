using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace GameInit.Framework.RVO
{
    // Rvo寻路
    public class RVOManager : Singleton<RVOManager>
    {
        // 寻路模拟器
        private List<Simulator> _simulators;
        // 英雄代理
        private List<Dictionary<int, GameAgent>> _gameAgentList;

        public void Init()
        {
            // 初始化模拟器
            InitSimulator();
            // 初始化代理器字典
            InitGameAgentDic();
        }
        // 初始化模拟器
        private void InitSimulator()
        {
            _simulators = new List<Simulator>();
            foreach (int i in Enum.GetValues(typeof(SimulatorType)))
            {
                var simulator = new Simulator();
                _simulators.Add(simulator);
                // 设置刷新时间
                simulator.setTimeStep(0.25f);
                // 这个代理的默认属性
                simulator.setAgentDefaults(0.5f, 1, 1f,
                    1f, 0.4f, 0.1f,
                    new Vector2(0.0f, 0.0f));
                // add in awake
                simulator.processObstacles();
            }
        }
        
        // 初始化代理器字典
        private void InitGameAgentDic()
        {
            _gameAgentList = new List<Dictionary<int, GameAgent>>();
            foreach (int i in Enum.GetValues(typeof(SimulatorType)))
            {
                _gameAgentList.Add(new Dictionary<int, GameAgent>());
            }
        }
        // 刷新
        public void Update(float deltaTime)
        {
            // 刷新模拟器
            for (int i = 0; i < _simulators.Count; i++)
            {
                _simulators[i].doStep();
            }
            
            // 刷新代理逻辑
            for (int i = 0; i < _gameAgentList.Count; i++)
            {
                var gameAgentDic = _gameAgentList[i];
                var gameAgentKeys = gameAgentDic.Keys.ToList();
                for (int j = 0; j < gameAgentKeys.Count; j++)
                {
                    var key = gameAgentKeys[j];
                    gameAgentDic[key].Update();
                }
            }
        }

        // 删除代理
        public void DeleteAgent(int sid)
        {
            for (int i = 0; i < _gameAgentList.Count; i++)
            {
                var gameAgentDic = _gameAgentList[i];
                if (gameAgentDic.ContainsKey(sid))
                {
                    gameAgentDic.Remove(sid);// 移除字典
                    _simulators[i].delAgent(sid);// 移除模拟器
                }
            }
        }
        // 创建代理
        public GameAgent CreateAgent(Transform self,float radius,float speed,SimulatorType simulatorType)
        {
            int sid = -1;
            int type = (int) simulatorType;
            //
            var pos =new Vector2(self.position.x,self.position.z);
            // 添加代理
            sid = _simulators[type].addAgent(pos,
            7f,10,3f,3f, radius,speed,
            new Vector2(0f,0f));
            
            if (sid == -1)
            {
                Debug.LogError("创建代理失败");
                return null;
            }
            // 创建
            GameAgent gameAgent = new GameAgent(sid,self,_simulators[type]);
            // 加入字典
            _gameAgentList[type].Add(sid,gameAgent);

            return gameAgent;
        }
        // 创建障碍
        public List<int> CreateObstacle(Vector3 Position,float sideLength,SimulatorType simulatorType)
        {
            float minX = Position.x - sideLength;
            float minZ = Position.z - sideLength;
            float maxX = Position.x + sideLength;
            float maxZ = Position.z + sideLength;
            
            IList<Vector2> obstacle = new List<Vector2>();
            obstacle.Add(new Vector2(maxX, maxZ));
            obstacle.Add(new Vector2(minX, maxZ));
            obstacle.Add(new Vector2(minX, minZ));
            obstacle.Add(new Vector2(maxX, minZ));
            // 添加障碍
            var firstPointIndex = _simulators[(int)simulatorType].addObstacle(obstacle);
            // 重新计算障碍
            _simulators[(int)simulatorType].processObstacles();
            return firstPointIndex;
        } 
        // 删除障碍
        public void DeleteObstacle(List<int> obstacleIndexs,SimulatorType type)
        {
            _simulators[(int)type].DeleteObstacle(obstacleIndexs);
            // 重新计算障碍
            _simulators[(int)type].processObstacles();
        }

        public void Obstacle()
        {
            _simulators[(int)SimulatorType.Low].Obstacle();
        }

        public void Dispose()
        {
            // 清理模拟器
            for (int i = 0; i < _simulators.Count; i++)
            {
                _simulators[i].Clear();
            }
            _simulators.Clear();
            
            // 清理字典
            _gameAgentList.Clear();
        }
    }
    
    public enum SimulatorType
    {
        Low = 0,    // 低等级
        Height = 1,    // 高等级
    }

}



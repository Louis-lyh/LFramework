#if UNITY_EDITOR
using System.IO;
using Cysharp.Threading.Tasks;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace GameInit.Framework
{
    /// <summary>
    /// 默认资源加载器 - 在编辑器中使用
    /// </summary>
    public class DefaultResLoader : IResourceLoader
    {
        /// 资源根目录
        private const string PATH_PREFIX = "Assets/Res/";
        
        /// <summary>
        /// 加载场景
        /// </summary>
        /// <param name="sceneName">场景名称</param>
        /// <param name="loadMode">加载模式</param>
        /// <returns></returns>
        public async UniTask<AsyncOperation> LoadSceneAsync(string sceneName, LoadSceneMode loadMode = LoadSceneMode.Additive)
        {
            return SceneManager.LoadSceneAsync(sceneName, loadMode);
        }
        
        /// <summary>
        /// 卸载场景
        /// </summary>
        /// <param name="sceneName">场景名称</param>
        /// <returns></returns>
        public AsyncOperation UnloadSceneAsync(string sceneName)
        {
            return SceneManager.UnloadSceneAsync(sceneName);
        }
        
        /// <summary>
        /// 同步加载资源
        /// </summary>
        /// <param name="path">资源在Res下的路径</param>
        /// <returns></returns>
        public T LoadAssetSync<T>(string path) where T : Object
        {
            return AssetDatabase.LoadAssetAtPath<T>(PATH_PREFIX + path);
        }
        
        /// <summary>
        /// 异步加载资源
        /// </summary>
        /// <param name="path">资源在Res下的路径</param>
        /// <returns></returns>
        public async UniTask<T> LoadAssetAsync<T>(string path) where T : Object
        {
            var fullPath = PATH_PREFIX + path;
            // 异步加载
            var result = AssetDatabase.LoadAssetAtPath<T>(fullPath);
            // 等待加载完毕
            await UniTask.Yield();
            return result;
        }
        
        /// <summary>
        /// 卸载一个资源
        /// </summary>
        /// <param name="path">资源在Res下的路径</param>
        /// <param name="isForce"></param>
        public void UnloadRes(string path, bool isForce = false)
        {
            
        }
        
        /// <summary>
        /// 资源是否存在
        /// </summary>
        /// <param name="path">资源在Res下的路径</param>
        /// <returns></returns>
        public bool AssetExists(string path)
        {
            var fullPath = PATH_PREFIX + path;
            return File.Exists(fullPath);
        }
    }  
}

#endif



﻿

using Cysharp.Text;

namespace GameInit.Framework
{
    /// <summary>
    /// 资源路径工具
    /// </summary>
    public class ResPathUtils
    {
        /// <summary>
        /// ui预制体路径
        /// </summary>
        /// <param name="name">ui预制体名字</param>
        /// <returns></returns>
        public static string GetUIPrefab(string name)
        {
            return ZString.Concat("UI/", name, ".prefab");
        }
        /// <summary>
        /// 音频文件路径
        /// </summary>
        /// <param name="name">名字</param>
        /// <param name="suffix">后缀</param>
        /// <returns></returns>
        public static string GetSoundPath(string name,string suffix = ".ogg")
        {
            return ZString.Concat("Sound/", name);
        }

        /// <summary>
        /// 配置数据路径
        /// </summary>
        /// <param name="cfgName">名字</param>
        /// <returns></returns>
        public static string GetConfigPath(string cfgName)
        {
            return ZString.Concat("Config/", cfgName, ".bytes");
        }
        /// <summary>
        /// 多语言配置数据路径
        /// </summary>
        /// <param name="cfgName">名字</param>
        /// <returns></returns>
        public static string GetLanguageConfigPath(string cfgName)
        {
            return ZString.Concat("Config/", "LangSplit/", cfgName, ".bytes");
        }
        /// <summary>
        /// 图集预制体路径
        /// </summary>
        /// <param name="name">名字</param>
        /// <returns></returns>
        public static string GetAtlasPrefab(string name)
        {
            return ZString.Concat("Atlas/", name, ".prefab");
        }
        /// <summary>
        /// 图集路径
        /// </summary>
        /// <param name="name">名字</param>
        /// <returns></returns>
        public static string GetSpriteAtlasPath(string name)
        {
            return ZString.Concat("Atlas/", name, ".spriteAtlas");
        }
        /// <summary>
        /// 字体路径
        /// </summary>
        /// <param name="fontName">名字</param>
        /// <returns></returns>
        public static string GetFontPath(string fontName)
        {
            return ZString.Concat("Fonts/", fontName);
        }
        /// <summary>
        /// 材质球路径
        /// </summary>
        /// <param name="name">名字</param>
        /// <returns></returns>
        public static string GetMat(string name)
        {
            return ZString.Concat("Material/", name, ".mat");
        }
    }
}
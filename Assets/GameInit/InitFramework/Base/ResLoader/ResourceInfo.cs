﻿ /*
  资源对象池
  用于从文件夹中加载的资源的重复利用
 */
using System.Collections.Generic;
using UnityEngine;

namespace GameInit.Framework
{
    /// <summary>
    /// 资源信息
    /// </summary>
    public class ResourceInfo : IClassPool
    {
        /// <summary>
        /// 父节点
        /// </summary>
        public static Transform Parent;
        /// <summary>
        /// 引用次数
        /// </summary>
        public int UseCount;
        /// <summary>
        /// 资源
        /// </summary>
        private Object _res;
        /// <summary>
        /// 使用列表
        /// </summary>
        private List<GameObject> _useList;
        /// <summary>
        /// 空闲列表
        /// </summary>
        private List<GameObject> _freeList;

        public void Init(Object res)
        {
            // 资源
            _res = res;
            // 使用列表
            _useList = new List<GameObject>();
            // 空闲列表
            _freeList = new List<GameObject>();
            
            // 初始化对象池父节点
            if (Parent == null)
            {
                Parent = new GameObject().transform;
                GameObject.DontDestroyOnLoad(Parent);
                Parent.name = "UnitGameObjectPool";
            }
        }
        
        /// <summary>
        /// 弹出对象
        /// </summary>
        /// <returns></returns>
        public GameObject Pop()
        {
            GameObject go = null;
            if (_res != null)
            {
                // 检查列表是否存在空引用
                CheckNull();
                
                if (_freeList.Count > 0)
                {
                    go = _freeList[0];
                    _freeList.RemoveAt(0);
                }
                else
                {
                    go = GameObject.Instantiate(_res) as GameObject;
                    SetTransform(go);
                }
                // 加入使用列表
                _useList.Add(go);
            }

            return go;
        }
        
        /// <summary>
        /// 回收对象
        /// </summary>
        /// <param name="go">对象</param>
        /// <param name="isDestroyImmediate">是否直接消除</param>
        public void Push(GameObject go, bool isDestroyImmediate = false)
        {
            if (_res != null)
            {
                // 从使用列表中移除
                if (_useList.Contains(go))
                    _useList.Remove(go);
                // 直接销毁
                if (isDestroyImmediate)
                {
                    GameObject.Destroy(go);
                    return;
                }
                // 加入空闲列表
                _freeList.Add(go);
                SetTransform(go);
            }
            else
            {
                Debug.LogError("这个地方有点问题，正常不应该走这个Push:" + go.name);
            }
        }


        /// <summary>
        /// 检查对象是否为空
        /// </summary>
        private void CheckNull()
        {
            // 检查使用列表是否存在空引用
            if(_useList != null)
            {
                for (var i = _useList.Count - 1; i >= 0; i--)
                {
                    var temp = _useList[i];
                    if (temp == null)
                        _useList.RemoveAt(i);
                }
            }
            
            // 检查空闲列表是否存在空引用
            if (_freeList == null)
            {
                for (var i = _freeList.Count - 1; i >= 0; i--)
                {
                    var temp = _freeList[i];
                    if (temp == null)
                        _freeList.RemoveAt(i);
                } 
            }
        }
        
        private void SetTransform(GameObject go)
        {
            go.transform.SetParent(Parent, false);
            go.SetActive(false);
        }
        
        /// <summary>
        /// 重置对象池
        /// </summary>
        public void Reset()
        {
            // 清空使用列表
            if (_useList != null)
            {
                for (int i = 0, count = _useList.Count; i < count; i++)
                    GameObject.Destroy(_useList[i]);
            }
            
            // 清空空闲列表
            if (_freeList != null)
            {
                for (int i = 0, count = _freeList.Count; i < count; i++)
                    GameObject.Destroy(_freeList[i]);
            }
            
            // 置空
            _freeList = null;
            _useList = null;
            UseCount = 0;
            _res = null;
        }
    }
}
﻿
using System.Collections.Generic;
using System.Linq;
using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace GameInit.Framework
{
    /// <summary>
    /// 资源加载管理器 - 管理资源加载、资源重复利用
    /// 加载方式由加载器决定
    /// </summary>
    public class ResourceManager
    {
        // 加载器
        private static IResourceLoader _loader;
        
        /// <summary>
        /// 静态构造方法 类被实例化或者静态成员被调用的时候进行调用
        /// </summary>
        static ResourceManager()
        {
#if UNITY_EDITOR
            SetLoader(new DefaultResLoader());
#endif  
        }
        /// <summary>
        /// 设置加载器
        /// </summary>
        /// <param name="loader"></param>
        public static void SetLoader(IResourceLoader loader)
        {
            if (loader == null)
                return;
            Debug.Log("ResourceManager.SetLoader() => 设置加载器，loader:" + loader);
            _loader = loader;
        }
        /// <summary>
        /// 同步加载资源
        /// </summary>
        /// <param name="path"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static T LoadAssetSync<T>(string path) where T : Object
        {
            return _loader?.LoadAssetSync<T>(path);
        }
        
        /// <summary>
        /// 路径对应的异步加载列表
        /// </summary>
        private static readonly Dictionary<string,List<AutoResetUniTaskCompletionSource>> _tempLoadTask = new Dictionary<string, List<AutoResetUniTaskCompletionSource>>();

        /// <summary>
        /// 从对象池加载
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static async UniTask<GameObject> ConstructObjectAsync(string path)
        {
            // 从对象池中加载
            var targetObj = CreateObjFormPool(path);
            if (targetObj != null)
                return targetObj;

            //------ 资源还没有加载完成 等待资源加载 ------
            if (_tempLoadTask.ContainsKey(path))
            {
                // 创建一个异步
                var waiteTask = AutoResetUniTaskCompletionSource.Create();
                // 加入等待列表
                _tempLoadTask[path].Add(waiteTask);
                // 等待资源加载
                await waiteTask.Task;
                
                // 加载完成
                targetObj = CreateObjFormPool(path);
                return targetObj;
            }
            
            //------ 异步加载资源 ------
            // 创建异步加载等待列表
            _tempLoadTask[path] = new List<AutoResetUniTaskCompletionSource>();
            
            // 异步加载对象
            targetObj = await LoadAssetAsync<GameObject>(path);
            
            // 新建资源信息（资源池）
            var info = ClassPoolsManager<ResourceInfo>.Pop();
            info.Init(targetObj);
            // 距离路径对应的资源信息（资源池） 
            _pathResourceInfoDic.Add(path,info);
            // 从资源信息（资源池中）获得对象
            var obj = info.Pop();
            if (obj == null)
            {
                Debug.LogError("ResourceManager.ConstructObjAsync() => 加载资源出错，资源不存在，path:" + path);
                _pathResourceInfoDic.Remove(path);
            }
            else
            {
                // 记录对象所在路径
                _objPathDic.Add(obj, path);
            }
            
            // 资源加载完成完成等待列表
            foreach (var value in  _tempLoadTask[path])
                value.TrySetResult();
            // 移除等待列表
            _tempLoadTask.Remove(path);
            return obj;
        }
        
        /// <summary>
        /// 异步加载资源
        /// </summary>
        /// <param name="path">资源路径</param>
        /// <typeparam name="T">资源类型</typeparam>
        /// <returns></returns>
        public static UniTask<T> LoadAssetAsync<T>(string path) where T : Object
        {
            return _loader.LoadAssetAsync<T>(path);
        }
		
        /// <summary>
        /// 卸载资源
        /// </summary>
        /// <param name="path">资源路径</param>
        /// <param name="unload"></param>
        public static void UnloadRes(string path, bool unload = false)
        {
            _loader?.UnloadRes(path, unload);
        }
        /// <summary>
        /// 异步加载场景
        /// </summary>
        /// <param name="sceneName">场景名称</param>
        /// <param name="mode">加载美术</param>
        /// <returns></returns>
        public static UniTask<AsyncOperation> LoadSceneAsync(string sceneName, LoadSceneMode mode = LoadSceneMode.Additive)
        {
            return _loader.LoadSceneAsync(sceneName, mode);
        }
        
        /// <summary>
        /// 卸载场景
        /// </summary>
        /// <param name="sceneName">场景名称</param>
        public static void UnloadSceneAsync(string sceneName)
        {
            _loader.UnloadSceneAsync(sceneName);
        }
        /// <summary>
        /// 资源是否存在
        /// </summary>
        /// <param name="path">资源路径</param>
        /// <returns></returns>
        public static bool AssetExists(string path)
        {
            return _loader.AssetExists(path);
        }
        #region 对象池
        /// <summary>
        /// 路径对应的资源
        /// </summary>
        private static readonly Dictionary<string,ResourceInfo> _pathResourceInfoDic = new Dictionary<string, ResourceInfo>();
        
        /// <summary>
        /// 实例化对象和路径对应
        /// </summary>
        private static readonly Dictionary<GameObject,string> _objPathDic = new Dictionary<GameObject, string>();
        
        /// <summary>
        /// 从池子创建对象
        /// </summary>
        /// <param name="path">路径</param>
        /// <returns></returns>
        private static GameObject CreateObjFormPool(string path)
        {
            GameObject targetObj = null;
            // 从对象池中获取
            if (_pathResourceInfoDic.TryGetValue(path, out ResourceInfo info))
            {
                targetObj = info.Pop();
                // 距离对象的路径
                _objPathDic.Add(targetObj,path);
                // 使用次数
                info.UseCount++;
                // 重置对象状态
                targetObj.transform.position = Vector3.zero;
                targetObj.SetActive(false);
            }

            return targetObj;
        }
        /// <summary>
        /// 回收对象
        /// </summary>
        /// <param name="go">游戏对象</param>
        /// <param name="isDestroyImmediate">是否销毁</param>
        public static void CollectObj(GameObject go, bool isDestroyImmediate = false)
        {
            if(go == null)
                return;

            if (_objPathDic.ContainsKey(go))
            {
                var path = _objPathDic[go];
                _objPathDic.Remove(go);
                // 资源池子
                var info = _pathResourceInfoDic[path];
                if (info != null)
                {
                    if (info.UseCount > 0)
                        info.UseCount--;
                    // 回收对象
                    info.Push(go,isDestroyImmediate);
                    // 卸载资源
                    UnloadRes(path);
                }
                else
                {
                    Debug.LogWarning("回收资源时资源信息为空");
                    GameObject.Destroy(go);
                }
            }
            else
            {
                Debug.LogWarning("回收资源时没有对应资源信息");
                GameObject.Destroy(go);
            }
        }
        #endregion
        
    }
}
﻿using System.IO;
using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace GameInit.Framework
{
    /// <summary>
    /// unity 默认加载器
    /// </summary>
    public class UnityResourceLoader: IResourceLoader
    {
        /// <summary>
        /// 加载场景
        /// </summary>
        /// <param name="sceneName">场景名称</param>
        /// <param name="loadMode">加载模式</param>
        /// <returns></returns>
        public async UniTask<AsyncOperation> LoadSceneAsync(string sceneName, LoadSceneMode loadMode = LoadSceneMode.Additive)
        {
            return SceneManager.LoadSceneAsync(sceneName, loadMode);
        }
        
        /// <summary>
        /// 卸载场景
        /// </summary>
        /// <param name="sceneName">场景名称</param>
        /// <returns></returns>
        public AsyncOperation UnloadSceneAsync(string sceneName)
        {
            return SceneManager.UnloadSceneAsync(sceneName);
        }
        /// <summary>
        /// 同步加载资源
        /// </summary>
        /// <param name="path"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public T LoadAssetSync<T>(string path) where T : Object
        {
            return Resources.Load<T>(path);
        }
        /// <summary>
        /// 异步加载资源
        /// </summary>
        /// <param name="path">资源在Resources下的路径</param>
        /// <typeparam name="T">资源类型</typeparam>
        /// <returns></returns>
        public async UniTask<T> LoadAssetAsync<T>(string path) where T : Object
        {
            // 加载资源
            var res = Resources.LoadAsync(path);
            // 等待加载
            await UniTask.WaitUntil(()=>res.isDone);
            return res.asset as T;
        }
        /// <summary>
        /// 卸载资源
        /// </summary>
        /// <param name="path"></param>
        /// <param name="isForce"></param>
        public void UnloadRes(string path, bool isForce = false)
        {
            
        }
        /// <summary>
        /// 判断资源是否存在
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public bool AssetExists(string path)
        {
            return File.Exists("Assets/Resource/"+path);
        }
    }
}
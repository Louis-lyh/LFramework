using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameInit.Framework
{
    public class SpriteAtlasInfo : MonoBehaviour
    {
        public string Key;

        public string[] PngNames;
    }
}


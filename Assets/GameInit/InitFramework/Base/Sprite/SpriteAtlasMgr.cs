﻿using System;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using GameInit.Framework;
using UnityEngine;
using UnityEngine.U2D;
using Logger = GameInit.Framework.Logger;

namespace GameInit.Framework
{
    public class SpriteAtlasMgr : Singleton<SpriteAtlasMgr>
    {
        /// <summary>
        /// 图集信息
        /// </summary>
        private const string SPRITE_ATLAS_INFO_FILE = "AtlasInfoFile";
        /// <summary>
        /// 图集名包含的图片名
        /// </summary>
        private static Dictionary<string, string> _atlasDict;
        
        /// <summary>
        /// 加载图集信息
        /// </summary>
        /// <returns></returns>
        public async UniTask ReloadAtlasInfo()
        {
            // 初始化
            if (_atlasDict == null)
                _atlasDict = new Dictionary<string, string>();
            _atlasDict.Clear();

            // 加载图集信息prefab
            var obj = await ResourceManager.LoadAssetAsync<GameObject>(ResPathUtils.GetAtlasPrefab(SPRITE_ATLAS_INFO_FILE));
            if (obj == null)
                return;
            
            // 读取图集信息
            var infos = obj.GetComponents<SpriteAtlasInfo>();

            foreach (var info in infos)
            {
                foreach (var png in info.PngNames)
                {
                    if(!_atlasDict.ContainsKey(png))
                        _atlasDict.Add(png,info.Key);
                }
            }
        }
        
        /// <summary>
        /// 移除加载图片
        /// </summary>
        /// <param name="pngName">图片名</param>
        /// <returns></returns>
        public async UniTask<Sprite> LoadSpriteAsync(string pngName)
        {
            if (string.IsNullOrEmpty(pngName) || !_atlasDict.TryGetValue(pngName, out var atlasName))
            {
                return null;
            }

            return await LoadSpriteAsync(atlasName, pngName);
        }
        /// <summary>
        /// 异步加载图片
        /// </summary>
        /// <param name="atlasName">图集名</param>
        /// <param name="pngName">图片名</param>
        /// <returns></returns>
        public async UniTask<Sprite> LoadSpriteAsync(string atlasName, string pngName)
        {
            var atlas = await ResourceManager.LoadAssetAsync<SpriteAtlas>(ResPathUtils.GetSpriteAtlasPath(atlasName));
            return atlas.GetSprite(pngName);
        }
        
        /// <summary>
        /// 同步加载图片
        /// </summary>
        /// <param name="pngName">图片名</param>
        /// <returns></returns>
        public Sprite LoadSprite(String pngName)
        {
            if (string.IsNullOrEmpty(pngName) || !_atlasDict.TryGetValue(pngName, out var atlasName))
            {
                return null;
            }

            return LoadSprite(atlasName, pngName);
        }

        /// <summary>
        /// 同步加载图片
        /// </summary>
        /// <param name="atlasName">图集名</param>
        /// <param name="pngName">图片名</param>
        /// <returns></returns>
        public Sprite LoadSprite(string atlasName, string pngName)
        {
            var atlas = ResourceManager.LoadAssetSync<SpriteAtlas>(ResPathUtils.GetSpriteAtlasPath(atlasName));
            return atlas.GetSprite(pngName);
        }
    }
}
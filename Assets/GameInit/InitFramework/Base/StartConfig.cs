using System;
using GameInit.Framework;
using UnityEngine;
using Logger = GameInit.Framework.Logger;

/// <summary>
/// 游戏启动配置
/// </summary>
public class StartConfig : MonoBehaviour
{
    [Tooltip("是否开启log")]
    public bool OpenLog;
    
    [Tooltip("是否开启调试模式")]
    public bool OpenDebug;
    
    [Tooltip("热更方式，默认为System")]
    public LoadDllType HotFixType = LoadDllType.System;
    
    [Tooltip("资源加载是否为AB模式")]
    public bool UserAssetBundle;

    [Tooltip("开启热更下载")]
    public bool UserRemoteAssetBundle;
    
    [Tooltip("资源服务器地址")]
    public string GameAssetRemoteUrl = "http://172.31.128.128/ftp/hot/LFramework";

    //启动时间
    private DateTime _startTime;
    public DateTime StartTime => _startTime;
    
    public static StartConfig Instance { get; private set; }

    private void Awake()
    {
        //记录启动时间
        _startTime = DateTime.UtcNow;
        // 初始化
        Instance = this;
        
        // log
        Logger.EnableLog = OpenLog;
        // 游戏帧率
        Application.targetFrameRate = 90;
        // 节能设置，允许屏幕在无用户交互一段时间后变暗。 NeverSleep（一直连这）
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
        // 打开调试窗口
        if (OpenDebug)
            gameObject.AddComponent<GameLogTool>();
        // 长期保留改对象
        DontDestroyOnLoad(gameObject);
        
        ModuleManager.Instance.Init();
        // 插入启动热更模块
        ModuleManager.Instance.InsertModule(new StartHotfixModule());
        // 插入加载ab包模块
        ModuleManager.Instance.InsertModule(new AssetBundleUpdateModule());
    }

    private void Start()
    {
        // 运行模块
        ModuleManager.Instance.Run(); 
    }

    private void Update()
    {

    }

    public enum LoadDllType
    {
        System,
        Hotfix,
    }
}

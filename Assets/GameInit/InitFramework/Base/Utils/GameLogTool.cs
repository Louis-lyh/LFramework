﻿using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace GameInit.Framework
{
    /// <summary>
    /// 游戏log工具
    /// </summary>
    public class GameLogTool : MonoBehaviour
    {
        // log数据
        private Dictionary<string, LogInfoData> _dictLogDatas;
        // 是否初始化
        private bool _blIsInit = false;
        // 显示log
        private bool _isShowContent = false;
        // 显示log按钮样式
        private GUIStyle _fpsBtnGuiStyle;
        // log样式
        private GUIStyle _logGuiStyle;

        protected void Start()
        {
            if (_blIsInit)
                return;
            
            // 记录初始化
            _blIsInit = true;
            // 打开log
            Logger.EnableLog = true;
            // 打印数据
            _dictLogDatas = new Dictionary<string, LogInfoData>();
            // 监听打印事件
            Application.logMessageReceived += OnLogAdded;

            _timeCount = _updateInterval;
        }
        
        /// <summary>
        /// 打印事件
        /// </summary>
        /// <param name="logText">文本</param>
        /// <param name="stackTrace">堆栈跟踪</param>
        /// <param name="type">类型</param>
        private void OnLogAdded(string logText, string stackTrace, LogType type)
        {
            // 大于300条重置
            if (_dictLogDatas.Count > 300)
                _dictLogDatas.Clear();
            
            // 格式化
            string content = "{0}:{1}";
            if (type == LogType.Error)
                content = string.Format(content, type, logText + "\n" + stackTrace);
            else
                content = string.Format(content, type, logText);
            
            // 加入字典
            if (_dictLogDatas.ContainsKey(content))
                _dictLogDatas[content].m_logCount += 1;
            else
                _dictLogDatas.Add(content, new LogInfoData(content, type));
        }
        
        /// <summary>
        /// 刷新UI-显示提示信息
        /// </summary>
        void OnGUI()
        {
            // 没有初始化退出
            if(_blIsInit)
                return;

            if (_fpsBtnGuiStyle == null)
            {
                // 显示log按钮样式
                _fpsBtnGuiStyle = GUI.skin.button;
                _fpsBtnGuiStyle.fontSize = 20;
                // log样式
                _logGuiStyle = GUI.skin.label;
                _logGuiStyle.fontSize = 20;
            }

            // 显示log
            if (_isShowContent)
                ShowLogContent();
            
            // 显示log按钮 - 显示帧率
            if (GUI.Button(new Rect(5, 150, 150, 60), _fps, _fpsBtnGuiStyle))
                _isShowContent = !_isShowContent;
        }

        // 滑动视图滑动位置
        private Vector2 _scrollPos;
        
        /// <summary>
        /// 显示log
        /// </summary>
        void ShowLogContent()
        {
            GUI.skin.verticalScrollbar.fixedWidth = 30;
            GUI.skin.verticalScrollbarThumb.fixedWidth = 30;
            GUI.skin.horizontalScrollbar.fixedHeight = 30;
            GUI.skin.horizontalScrollbarThumb.fixedHeight = 30;
            
            // 区域
            GUILayout.BeginArea(new Rect(0, 210, Screen.width * 0.95f, Screen.height * 0.8f));
            // 滚动视图
            _scrollPos = GUILayout.BeginScrollView(_scrollPos, GUILayout.Width(Screen.width * 0.95f), GUILayout.Height(Screen.height * 0.8f));
            
            // log数据
            Dictionary<string, LogInfoData>.Enumerator rator = _dictLogDatas.GetEnumerator();
            var sb = new StringBuilder();
            while (rator.MoveNext())
            {
                switch (rator.Current.Value.m_logType)
                {
                    case LogType.Log:
                        GUI.contentColor = Color.white;
                        break;
                    case LogType.Warning:
                        GUI.contentColor = Color.yellow;
                        break;
                    case LogType.Error:
                    case LogType.Exception:
                        GUI.contentColor = Color.red;
                        var logStr = rator.Current.Key + "------" + rator.Current.Value.m_logText;
                        GUILayout.Label(logStr, _logGuiStyle);
                        sb.Append(logStr);
                        break;
                    default:
                        GUI.contentColor = Color.white;
                        break;
                }
                // 文本
                var str = rator.Current.Key + " --- Count:" + rator.Current.Value.m_logCount;
                GUILayout.Label(str, _logGuiStyle);
                sb.Append(str);
            }

            // 复制按钮
            if (GUILayout.Button("Copy", GUILayout.Height(50)))
                GUIUtility.systemCopyBuffer = sb.ToString();
            
            // 清除按钮
            if (GUILayout.Button("Clear", GUILayout.Height(50)))
                _dictLogDatas.Clear();
            
            // 区域结束
            GUILayout.EndScrollView();
            GUILayout.EndArea();
            
            GUI.skin.verticalScrollbar.fixedWidth = 0;
            GUI.skin.verticalScrollbarThumb.fixedWidth = 0;
            GUI.skin.horizontalScrollbar.fixedHeight = 0;
            GUI.skin.horizontalScrollbarThumb.fixedHeight = 0;
        }

        #region FPS
        // 刷新间隔时间
        private float _updateInterval = 0.5f;
        // 累计帧率
        private float _accum = 0.0f;
        // 累计次数
        private float _frames = 0f;
        // 间隔时间
        private float _timeCount;
        private string _fps = "LogTool";

        protected void Update()
        {
            // 时间减少
            _timeCount -= Time.deltaTime;
            // 累计帧率
            _accum += Time.timeScale / Time.deltaTime;
            // 统计次数
            ++_frames;
            if (_timeCount <= 0.0f)
            {
                // 帧率
                _fps = "FPS:" + (_accum / _frames).ToString("F2");
                // 重置时间
                _timeCount = _updateInterval;
                // 重置帧率
                _accum = 0.0f;
                _frames = 0f;
            }
        }

        #endregion
        
        /// <summary>
        /// 打印数据
        /// </summary>
        public class LogInfoData
        {
            public LogType m_logType { private set; get; }
            public string m_logText { private set; get; }
            public int m_logCount { set; get; }

            public LogInfoData(string value, LogType type)
            {
                m_logType = type;
                m_logText = value;
            }
        }
    }
}
using System;
using System.IO;
using System.Text;
using System.Security.Cryptography;
namespace GameInit.Framework
{
    public class MD5Util
    {
        /// <summary>
        /// 计算字符串的MD5值
        /// </summary>
        public static string MD5(string source)
        {
            MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
            byte[] data = Encoding.UTF8.GetBytes(source);
            byte[] retVal = md5.ComputeHash(data, 0, data.Length);
            md5.Clear();

            StringBuilder sb = new StringBuilder();
            for ( int i = 0; i < retVal.Length; i++ )
            {
                sb.Append(retVal[i].ToString("x2"));
            }
            return sb.ToString();
        }

        /// <summary>
        /// 计算文件的MD5值
        /// </summary>
        public static string MD5file(string file)
        {
            try
            {
                var source = File.ReadAllText(file);
                return MD5(source);
            }
            catch (Exception ex)
            {
                throw new Exception("md5file() fail, error:" + ex.Message);
            }
        }
        
        /// <summary>
        /// 加密-SHA256
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string SHA256(string str)
        {
            var SHA256Data = Encoding.UTF8.GetBytes(str);

            var Sha256 = new SHA256Managed();
            var by = Sha256.ComputeHash(SHA256Data);

            return BitConverter.ToString(by).Replace("-", "").ToLower();                                                                   
        }
    }
}
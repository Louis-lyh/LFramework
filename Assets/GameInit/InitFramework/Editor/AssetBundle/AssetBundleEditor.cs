using System;
using System.Collections.Generic;
using System.IO;
using Cysharp.Threading.Tasks;
using UnityEditor;
using UnityEngine;
using HybridCLR.Editor;
using HybridCLR.Editor.Commands;

namespace GameInit.Framework.Editor
{
    /**
     * 
     * 游戏AB生成工具,实现说明，AB生成由两个目录组成：ABOutPut_Path、ABFinally_Path、ABUpload_Path
     *     1、ABOutPut_Path: ab文件源目录，此目录下资源为源始资源生成的ab文件，不管目标ab是否需要加密，此目录下的资源都不会参与，目的是为了增量打包（加快导出速度）
     *     2、ABFinally_Path： ab最终输出目录，根据需求处理是否加密
     *     3、ABUpload_Path：ab上传到FTP目录，文件带了版本号
     * SearchAllResource() 根据需求设置需要生成ab的资源目录
     * CopyVideoToAssets() 游戏中mp4文件不作任何处理，直接复制到ab输出目录
     */
    public class AssetBundleTool
    {
        #region 调用生成AB入口

        [MenuItem("LFramework/AssetBundle/Build Android Bundle",false, 11)]
        public static UniTask BuildAndroidBundle()
        {
            return OneKeyBuildAssetBundle(BuildTarget.Android);
        }

        [MenuItem("LFramework/AssetBundle/Build IOS Bundle",false, 11)]
        public static UniTask BuildIOSBundle()
        {
            return OneKeyBuildAssetBundle(BuildTarget.iOS);
        }

        [MenuItem("LFramework/AssetBundle/Build Win Bundle",false, 11)]
        public static void BuildWinBundle()
        {
            OneKeyBuildAssetBundle(BuildTarget.StandaloneWindows);
        }
        
        [MenuItem("LFramework/AssetBundle/Build WebGL Bundle",false, 11)]
        public static void BuildWebGLBundle()
        {
            OneKeyBuildAssetBundle(BuildTarget.WebGL);
        }

        #endregion
        /// <summary>
        /// 根节点路径
        /// </summary>
        static string AssetsRoot = Application.dataPath + "/";
        /// <summary>
        /// ab最终输出目录，根据需求处理是否加密
        /// </summary>
        private static string ABFinally_Path = Application.streamingAssetsPath + "/" + AssetBundleConst.Platform + "/";
        /// <summary>
        /// ab上传到FTP目录，文件带了版本号
        /// </summary>
        private static string ABUpload_Path = Application.dataPath + "/../AssetBundles/Upload/" + AssetBundleConst.Platform + "/";
        
        /// <summary>
        /// 一键构建资产捆绑包
        /// </summary>
        /// <param name="target">目标平台</param>
        /// <param name="encryptAssetBundle">加密</param>
        /// <returns></returns>
        private static UniTask OneKeyBuildAssetBundle(BuildTarget target, bool encryptAssetBundle = false)
        {
            try
            {
                // 将所有资源统计分类
                SearchAllResource();
                // 打包
                BuildAssetBundle(target, encryptAssetBundle);
            }
            catch (Exception e)
            {
                Logger.LogError("AssetBundleEditor.OneKeyBuildAssetBundle() => 生成ab出错，Error:" + e.Message);
            }
            EditorUtility.ClearProgressBar();
            AssetDatabase.Refresh();
            return UniTask.CompletedTask;
        }

        #region 搜索资源
        /// bundle名称对应的资源列表，key：ab包名字，value：资源路径
        private static Dictionary<string, List<string>> _dicBundles = new Dictionary<string, List<string>>();
        /// 记录单个资源，key：资源路径，value：ab包名字
        private static Dictionary<string, string> _dictTmpRecord = new Dictionary<string, string>();
        /// 非场景资源依赖文件，先计数处理，如果被引用资源大于1，则生成公用ab
        private static Dictionary<string, int> _dictTmpCount = new Dictionary<string, int>();
        
         /// <summary>
        /// 将所有资源统计分类
        /// </summary>
        private static void SearchAllResource()
        {
            _dicBundles.Clear();
            _dictTmpRecord.Clear();
            _dictTmpCount.Clear();
            
            // 显示编辑器进度条
            EditorUtility.DisplayProgressBar("Set GameRes AssetBundleName", "Please wait...设置Resource目录", 1f);
            
            //加载AssetBundleScriptableObject（需要打包的文件目录）
            var sObjPath = "Assets/Game/GameInit/InitFramework/Editor/AssetBundle/AssetBundleScriptableObject.asset";
            var assetBundleSObj = AssetDatabase.LoadAssetAtPath<AssetBundleSobjConfig>(sObjPath);

            // ---------- 无需搜索依赖目录 ----------
            // 资源根目录
            string resourcePath = AssetsRoot + "Res/";
            
            // 配置数据 
            CollectBundleBuildWithoutDependencies(resourcePath + "Config", true);
            // 字体
            CollectBundleBuildWithoutDependencies(resourcePath + "Fonts", false, true, "font");
            // 图集
            CollectBundleBuildWithoutDependencies(resourcePath + "Atlas");
            // 音乐文件
            CollectBundleBuildWithoutDependencies(resourcePath + "Sound", true);
            // Dll 文件
            CollectBundleBuildWithoutDependencies(resourcePath + "HotFixDll");
            // shader
            CollectShaders();
            
            if (assetBundleSObj != null)
            {
                // 读取无需要搜索依赖目录
                for (int i = 0; i < assetBundleSObj.WithoutDList.Count; i++)
                {
                    var cwd = assetBundleSObj.WithoutDList[i];
                    CollectBundleBuildWithoutDependencies(resourcePath + cwd.Name, cwd.Recursive, cwd.BlInOneBundle, cwd.InOneBundleName);
                }
            }

            // UI资源目录
            var dirs = Directory.GetDirectories("Assets/Arts/UI");
            foreach (var dir in dirs)
            {
                var d = dir.Replace("Assets/", "").Replace('\\', '/');
                CollectBundleBuildWithoutDependencies(dir, true, true, d);
            }
            // ---------- END ----------
            
            
            // ---------- 非场景prefab资源搜索 （需要搜索依赖文件目录） ----------
            
            // UI预制体 
            CollectBundleBuildWithDependencies(resourcePath + "UI", true);
            // 特效预制体
            CollectBundleBuildWithDependencies(resourcePath + "Effect", true);
            // 游戏预制体
            CollectBundleBuildWithDependencies(resourcePath + "GamePlay", true);
           
            if (assetBundleSObj != null)
            {
                // 读取需要搜索依赖文件目录
                for (int i = 0; i < assetBundleSObj.WithDList.Count; i++)
                {
                    var cwd = assetBundleSObj.WithDList[i];
                    CollectBundleBuildWithDependencies(resourcePath + cwd.Name, cwd.Recursive);
                }
            }
            // ---------- END ----------

            // 关闭编辑器进度条
            EditorUtility.ClearProgressBar();
        }
         
         /// <summary>
         /// 收集shader
         /// </summary>
         private static void CollectShaders()
         {
             List<string> allShader = new List<string>();
             List<string> tmpList = new List<string>();
            
             //Spine shaders
             // FileUtil.RecursiveDirectory(AssetsRoot + "ThirdParty/Framework/Spine/Runtime/spine-unity/Shaders", tmpList, true, ".meta", ".cginc");
             // allShader.AddRange(tmpList);
             List<string> allAssetPathShaders = new List<string>();
             string assetPath;
             foreach (var file in allShader)
             {
                 assetPath = "Assets/" + file.Replace(AssetsRoot, string.Empty);
                 allAssetPathShaders.Add(assetPath);
             }
            
             // 记录
             AddBundleAssetFile(FormatPath2Name("Assets/Arts/GameShaders"), allAssetPathShaders);
         }
         
         /// <summary>
         /// 收集不带依赖项的捆绑包构建
         /// </summary>
         /// <param name="path">资源路径</param>
         /// <param name="recursive">是否递归文件夹</param>
         /// <param name="blInOneBundle">是否打包成单独捆绑包</param>
         /// <param name="InOneBundleName">捆绑包名称</param>
         private static void CollectBundleBuildWithoutDependencies(string path, bool recursive = false, bool blInOneBundle = false, string InOneBundleName = "")
         {
             // 路径下所有文件
             List<string> allAssetFiles = FilesToAssetPathFiles(path, recursive);
             
             foreach (var file in allAssetFiles)
             {
                 // 将文件路径转为AssetBundle名称
                 string bundleName = FormatPath2Name(!blInOneBundle ? file : InOneBundleName);
                 // 统计资源
                 AddBundleAssetFile(bundleName, file);
             }
         }
         /// <summary>
         /// 收集具有依赖项的捆绑包生成
         /// </summary>
         /// <param name="path">路径</param>
         /// <param name="recursive">递归</param>
         private static void CollectBundleBuildWithDependencies(string path, bool recursive = false)
         {
             // 获得路径下所有文件
             List<string> allAssetFiles = FilesToAssetPathFiles(path, recursive);
            
             for (int i = 0; i < allAssetFiles.Count; i++)
                 ParseAssetDependencies(allAssetFiles[i]);
         }
         /// <summary>
        /// 分类统计资源得依赖项
        /// </summary>
        /// <param name="assetPath">资源路径</param>
        /// <param name="blSceneDepends">是否是场景资源</param>
        private static void ParseAssetDependencies(string assetPath, bool blSceneDepends = false)
        {
            // 资源所有依赖项
            string[] depends = AssetDatabase.GetDependencies(assetPath);
            // 处理依赖资源
            if (depends.Length > 0)
            {
                for (int i = 0; i < depends.Length; i++)
                    DoParseAssetDependencies(depends[i],blSceneDepends);
            }
            else
            {
                DoParseAssetDependencies(assetPath,blSceneDepends);
            }
        }
         private static void DoParseAssetDependencies(string depAssetPath, bool blSceneDepends = false)
         {
             // 获取后缀名
            string fileExtension = Path.GetExtension(depAssetPath);
            // 文件路径转为 AB包名称
            string bundleName = FormatPath2Name(depAssetPath);
            
            // 记录依赖文件被使用次数
            if (_dictTmpRecord.ContainsKey(depAssetPath))
            {
                if (!blSceneDepends)
                {
                    if (_dictTmpCount.ContainsKey(depAssetPath))
                        _dictTmpCount[depAssetPath] += 1;
                    else
                        _dictTmpCount[depAssetPath] = 1;
                }
                return;
            }
            
            switch (fileExtension.ToLower())
            {
                case ".shader":
                    // 统计
                    AddBundleAssetFile(FormatPath2Name("Assets/Arts/GameShaders"), depAssetPath);
                    break;
                case ".prefab":
                case ".mat":
                    // 统计
                    AddBundleAssetFile(bundleName, depAssetPath);
                    // 分析预制体和材质球的依赖项
                    ParseAssetDependencies(depAssetPath, blSceneDepends);
                    break;
                case ".png":
                case ".jpg":
                case ".anim":
                case ".tga":
                case ".fbx":
                case ".controller":
                case ".mesh":
                case ".asset":
                    //如果是场景文件依赖，就按资源目录生成ab文件(具体原因可以参考:https://answer.uwa4d.com/question/5c35666c9efb1b001255de54)
                    if (blSceneDepends)
                    {
                        int idx = depAssetPath.LastIndexOf("/");
                        if (idx > 0)
                            bundleName = FormatPath2Name(depAssetPath.Substring(0, idx));
                        AddBundleAssetFile(bundleName, depAssetPath);
                        if (_dictTmpCount.ContainsKey(depAssetPath))
                            _dictTmpCount.Remove(depAssetPath);
                    }
                    // 计数
                    else
                    {
                        if (_dictTmpCount.ContainsKey(depAssetPath))
                            _dictTmpCount[depAssetPath] += 1;
                        else
                            _dictTmpCount[depAssetPath] = 1;
                    }
                    break;
            }
         }
         
         /// <summary>
        /// 将文件路径转为AB名字
        /// </summary>
        /// <param name="path">路径名称</param>
        /// <returns></returns>
        private static string FormatPath2Name(string path)
        {
            var abName = path.Replace('/', '$').Replace('.', '$').ToLower();
            return abName;
        }

         /// <summary>
        /// 搜索路径先所有资源
        /// </summary>
        /// <param name="path">资源路径</param>
        /// <param name="recursive">是否递归文件夹</param>
        /// <returns>返回资源路径</returns>
        static List<string> FilesToAssetPathFiles(string path, bool recursive = false)
        {
            // 所有文件路径
            List<string> allFiles = new List<string>();
            // 获取该路径下的所有文件
            FileUtil.RecursiveDirectory(path, allFiles, recursive, ".meta", ".DS_Store");
            
            List<string> values = new List<string>();
            // 格式化路口 以Assets 开头
            foreach (var file in allFiles)
            {
                string assetPath = "";
                if (file.StartsWith("Assets/"))
                    assetPath = file;
                else
                    assetPath = "Assets/" + file.Replace(AssetsRoot, string.Empty);
                
                values.Add(assetPath);
            }
            return values;
        }
        /// <summary>
        /// 统计分类统计资源
        /// </summary>
        /// <param name="bundleName">bundle 名称</param>
        /// <param name="assetFiles">资源路径</param>
        static void AddBundleAssetFile(string bundleName, List<string> assetFiles)
        {
            for (int i = 0; i < assetFiles.Count; i++)
                AddBundleAssetFile(bundleName, assetFiles[i]);
        }
        /// <summary>
        /// 分类统计资源
        /// </summary>
        /// <param name="bundleName">ab包名称</param>
        /// <param name="assetFile">资源路径</param>
        private static void AddBundleAssetFile(string bundleName, string assetFile)
        {
            List<string> listBundleAssets;
            // 资源重复返回
            if (_dictTmpRecord.ContainsKey(assetFile))
                return;
            
            // ab包名称 (图集和贴图公用一个ab包)
            bundleName = GetBundleName(bundleName);
            
            if (_dicBundles.ContainsKey(bundleName))
                listBundleAssets = _dicBundles[bundleName];
            else
            {
                listBundleAssets = new List<string>();
                _dicBundles.Add(bundleName, listBundleAssets);
            }
            
            // 加入bundle名称相同得列表
            listBundleAssets.Add(assetFile);
            // 记录资源对应的ab包
            _dictTmpRecord.Add(assetFile, bundleName);
        }
        
        /// <summary>
        /// 获取bundleName,图集和贴图打在一个AB里
        /// </summary>
        /// <param name="bundleName"></param>
        /// <returns></returns>
        static string GetBundleName(string bundleName)
        {
            string result = null;
            string prefix = "arts$ui$";
            if (bundleName.Contains(prefix))
            {
                result = bundleName.Replace(prefix, "res$atlas$") + "$spriteatlas";
                if (!result.StartsWith("assets$"))
                {
                    result = "assets$" + result;
                }
            }
            else
            {
                result = bundleName;
            }
            
            return result;
        }

        #endregion
        
        #region 生成AssetBundle配置文件
         /// <summary>
        /// 目标平台，是否加密，是否直接打到StreamingAssets下
        /// </summary>
        /// <param name="target"></param>
        /// <param name="encryptAssetBundle"></param>
        private static UniTask BuildAssetBundle(BuildTarget target, bool encryptAssetBundle = false)
        {
            // 显示编辑器进度条
            EditorUtility.DisplayProgressBar("Build All Resource to AssetBundle", "Please wait...", 1f);
            
           // 打包
           var abList = CreateAssetBundle(target);

           // 显示编辑器进度条
            EditorUtility.DisplayProgressBar("正在生成配置文件", "等等...", 1f);
            
            // ab包资源路径
            string finallyAssetPath = ABFinally_Path + AssetBundleConst.AssetsDirectory;
            
            // 搜索所有ab包资源
            List<string> allFiles = new List<string>();
            FileUtil.RecursiveDirectory(finallyAssetPath, allFiles, false, ".DS_Store", ".meta", ".manifest");
            
            // 生成ab包配置
            var abConfig = GenAssetBundleConfig(abList,allFiles);
            
            // 复制 ab包到更新文件夹
            CopyToUploadPath(abConfig,allFiles);
            
            // log
            Logger.Log("导出AB完成");
            
            // 关闭编辑器进度
            EditorUtility.ClearProgressBar();
            return UniTask.CompletedTask;
        }
        
         /// <summary>
         /// 创建ab包
         /// </summary>
         /// <param name="target"></param>
         /// <returns></returns>
         private static List<AssetBundleBuild> CreateAssetBundle(BuildTarget target)
         {
             // ab包文件夹
             string abPath = ABFinally_Path + AssetBundleConst.AssetsDirectory;
             // 创建存放AssetBundle文件的文件夹
             if (!Directory.Exists(abPath))
                 Directory.CreateDirectory(abPath);
            
             // 非场景资源依赖文件数量大于一 单独打一个ab包
             foreach (var kv in _dictTmpCount)
             {
                 if (kv.Value > 1)
                 {
                     int idx = kv.Key.LastIndexOf("/");
                     if (idx > 0)
                     {
                         // 以文件夹问包名
                         string bundleName = FormatPath2Name(kv.Key.Substring(0, idx));
                         // 统计ab包
                         AddBundleAssetFile(bundleName, kv.Key);
                     }
                 }
             }

             // 创建ab包
             List<AssetBundleBuild> list = new List<AssetBundleBuild>();
             foreach (var kv in _dicBundles)
             {
                 AssetBundleBuild assetBundleBuild = new AssetBundleBuild();
                 // 包名
                 assetBundleBuild.assetBundleName = kv.Key; 
                 // 资源路径
                 assetBundleBuild.assetNames = kv.Value.ToArray(); 
                 list.Add(assetBundleBuild);
             }
            
             // 删掉旧的ad包
             if (Directory.Exists(abPath))
                 Directory.Delete(abPath, true);
             Directory.CreateDirectory(abPath);
            
             // 进行ab包打包
             BuildPipeline.BuildAssetBundles(abPath, list.ToArray(),
                 BuildAssetBundleOptions.ChunkBasedCompression | BuildAssetBundleOptions.DeterministicAssetBundle, target);

             return list;
         }

         /// <summary>
         /// 生成ab包配置
         /// </summary>
         /// <param name="abList"></param>
         /// <param name="allFiles"></param>
         /// <returns></returns>
        private static Dictionary<string, GameAbInfo> GenAssetBundleConfig(List<AssetBundleBuild> abList,List<string> allFiles)
        {
            // AB资源信息文件名
            string streamingInfoFile = ABFinally_Path + AssetBundleConst.AbAssetsInfoFile;
            
            // 用于版本控制的时间戳
            long ver = DateTime.Now.Ticks;
            
            Dictionary<string, GameAbInfo> assetBundleInfos = new Dictionary<string, GameAbInfo>();
            // 记录ab包资源信息
            foreach (var assetBundle in abList)
            {
                // ab包信息
                var gameAbInfo = new GameAbInfo();
                // ab包名称
                string abName = assetBundle.assetBundleName;
                gameAbInfo.AbName = abName;
                // 文件版本
                gameAbInfo.FileVer = ver;
                // 遍历ab包中的资源
                foreach (var assetFile in assetBundle.assetNames)
                {
                    // 改为小写
                    var assets = assetFile.ToLower();
                    Logger.LogGreen("assets : "+assets);
                    // 资源扩展名
                    var ext = Path.GetExtension(assets);
                    if (ext.Equals(".unity"))
                    {
                        // 获取不带扩展名的文件名
                        assets = Path.GetFileNameWithoutExtension(assets);
                        // 记录路径
                        gameAbInfo.AddFile(assets);
                    }
                    else
                    {
                        if (assets.Contains("/res/"))
                        {
                            // 去除扩展名
                            assets = assets.Replace(ext, "");
                            int resourcesIndex = assets.IndexOf("/res/");
                            // 截取 "/res/" 后面的路径
                            assets = assets.Substring(resourcesIndex + "/res/".Length);
                            // 记录路径
                            gameAbInfo.AddFile(assets);
                        }
                    }
                }

                if (assetBundleInfos.ContainsKey(abName))
                {
                    Logger.LogError("[AssetBundleTool.GenAssetBundleConfig() => 重复AB资源, abName:" + abName + "]");
                    continue;
                }

                assetBundleInfos.Add(abName, gameAbInfo);
            }
            
            // 记录ab包 md5 文件大小
            for (int i = allFiles.Count - 1; i >= 0; i--)
            {
                GameAbInfo gameAbInfo;
                var file = allFiles[i];
                // 指定路径字符串的文件名和扩展名。
                var fileName = Path.GetFileName(file);
                // 文件的MD5值
                var md5Value = MD5Util.MD5file(file);
                // 文件大小
                var fileSize = FileUtil.GetFileSize(file);
                if (assetBundleInfos.ContainsKey(fileName))
                    gameAbInfo = assetBundleInfos[fileName];
                //没有的话，应该只有mp4文件，mp4文件不生成ab
                else
                {
                    gameAbInfo  = new GameAbInfo();
                    gameAbInfo.AbName = fileName;
                    gameAbInfo.AddFile(fileName);
                    gameAbInfo.FileVer = ver;
                    assetBundleInfos.Add(fileName, gameAbInfo);
                }
                // 记录 文件的MD5值 文件大小
                gameAbInfo.AbMd5 = md5Value;
                gameAbInfo.FileSize = fileSize;
            }

            
            // 文件数据没有更改 继续使用就的版本信息
            if (File.Exists(streamingInfoFile))
            {
                // 读取旧的AB资源信息
                var oldInfos = AssetBundleUtils.DecodeStringPair(File.ReadAllBytes(streamingInfoFile));
                foreach (var value in assetBundleInfos.Values)
                {
                    if (oldInfos.ContainsKey(value.AbName))
                    {
                        // 文件没有修改 版本控制信息不变
                        if (oldInfos[value.AbName].AbMd5.Equals(value.AbMd5))
                        {
                            value.FileVer = oldInfos[value.AbName].FileVer;
                            value.FileSize = oldInfos[value.AbName].FileSize;
                        }
                    }
                }
            }

            // 保存Ver文件
            FileWriteAllText(ABFinally_Path + AssetBundleConst.VersionFile, ver.ToString());
            // 保存AbAssetsInfoFile文件
            FileWriteAllBytes(ABFinally_Path + AssetBundleConst.AbAssetsInfoFile, AssetBundleUtils.EncodeStringPair(assetBundleInfos, true));

            return assetBundleInfos;
        }
        
        /// <summary>
        /// 复制 ab包到更新文件夹
        /// </summary>
        /// <param name="assetBundleInfos">ab包信息配置</param>
        /// <param name="allFiles">ab包路径</param>
        private static void CopyToUploadPath(Dictionary<string, GameAbInfo> assetBundleInfos, List<string> allFiles)
        {
            // ---------- 复制ab包到更新文件夹中 ----------
            // 删除旧文件夹
            if(Directory.Exists(ABUpload_Path))
                Directory.Delete(ABUpload_Path, true);
            // 创建行文件夹
            string uploadAssetDir = ABUpload_Path + AssetBundleConst.AssetsDirectory + "/";
            Directory.CreateDirectory(uploadAssetDir);
            
            // 移动ab包
            for (int i = allFiles.Count - 1; i >= 0; i--)
            {
                var file = allFiles[i];
                // 文件名
                var fileName = Path.GetFileName(file);
                if (assetBundleInfos.ContainsKey(fileName))
                {
                    var uploadFile = uploadAssetDir + fileName + "_" + assetBundleInfos[fileName].FileVer;
                    File.Copy(file, uploadFile);
                }
            }

            // Ver文件复制到 AssetBundles/Upload 下
            File.Copy(ABFinally_Path + AssetBundleConst.VersionFile, ABUpload_Path + AssetBundleConst.VersionFile);
            // AbAssetsInfoFile文件复制到 AssetBundles/Upload 下
            File.Copy(ABFinally_Path + AssetBundleConst.AbAssetsInfoFile, ABUpload_Path + AssetBundleConst.AbAssetsInfoFile);
        }


        /// <summary>
        /// 保存文件
        /// </summary>
        /// <param name="path"></param>
        /// <param name="bytes"></param>
        static void FileWriteAllBytes(string path, byte[] bytes)
        {
            CreateFileDir(path);
            File.WriteAllBytes(path,bytes);
        }
        /// <summary>
        /// 保存文件
        /// </summary>
        /// <param name="path"></param>
        /// <param name="contents"></param>
        static void FileWriteAllText(string path, string contents)
        {
            CreateFileDir(path);
            File.WriteAllText(path, contents);
        }
        static void FileWriteAllLines(string path, string[] contents)
        {
            CreateFileDir(path);
            File.WriteAllLines(path, contents);
        }
        /// <summary>
        /// 创建文件夹
        /// </summary>
        /// <param name="path"></param>
        static void CreateFileDir(string path)
        {
            if (!File.Exists(path))
            {
                var dir = Path.GetDirectoryName(path);
                if (!Directory.Exists(dir))
                {
                    Directory.CreateDirectory(dir);
                }
            }
        }
       
        #endregion
        
        #region 测试AssetBundle加密
        //[MenuItem("Tools/AssetBundle/Test Build AB")]
        static void TestBuildAB()
        {
            string path = Application.dataPath + "/../AssetBundles/Android/GameRes/";
            DirectoryInfo dir = new DirectoryInfo(path);
            FileInfo[] fileInfos = dir.GetFiles("*.*");
            string[] tmp;
            string newFile;
            foreach (var file in fileInfos)
            {
                tmp = file.FullName.Split('_');
                if(tmp.Length== 1)
                    continue;
                if (tmp.Length > 2)
                    newFile = file.FullName.Replace("_" + tmp[tmp.Length - 1], "");
                else
                    newFile = tmp[0];
                File.Copy(file.FullName, newFile);
                File.Delete(file.FullName);
            }
        }

        [MenuItem("LFramework/AssetBundle/Revert AB FileName",false, 11)]
        static void RevertAbFileName()
        {
            string finallyAssetPath = ABFinally_Path + AssetBundleConst.AssetsDirectory;
            List<string> allFiles = new List<string>();
            FileUtil.RecursiveDirectory(finallyAssetPath, allFiles, false, ".DS_Store");
            string fileFullName;
            string newFileName;
            
            for (int i = allFiles.Count - 1; i >=0 ; i--)//int i = 0; i < allFiles.Count; i++)
            {
                fileFullName = allFiles[i];
                if(!fileFullName.Contains("_"))
                    continue;
                int startIndex = fileFullName.LastIndexOf("_");
                string verstr = fileFullName.Substring(startIndex, fileFullName.Length - startIndex);
                newFileName = fileFullName.Replace(verstr, "");
                if(File.Exists(newFileName))
                    continue;
                File.Move(fileFullName, newFileName);
            }
        }
        #endregion
        
        #region HybridCLR代码相关
        /// <summary>
        /// 拷贝 HybridCLR dll 到资源目录 Res
        /// </summary>
        [MenuItem("LFramework/AssetBundle/BuildAssetsAndCopyToStreamingAssets")]
        public static void BuildAndCopyABAOTHotUpdateDlls()
        {
            // 热更资源地址
            var hotfixAssembliesDstDir = AssetsRoot + "Res/HotFixDll";
            if (!Directory.Exists(hotfixAssembliesDstDir))
                Directory.CreateDirectory(hotfixAssembliesDstDir);
            // 当前平台
            var target = EditorUserBuildSettings.activeBuildTarget;
            // 编译程序集
            CompileDllCommand.CompileDll(target);
            // 复制元数据
            CopyAOTAssembliesToStreamingAssets();
            // 复制热更程序集
            CopyHotUpdateAssembliesToStreamingAssets();
            // 刷新
            AssetDatabase.Refresh();
        }
        
        /// <summary>
        /// 复制元数据
        /// </summary>
        private static void CopyAOTAssembliesToStreamingAssets()
        {
            // 当前平台
            var target = EditorUserBuildSettings.activeBuildTarget;
            // 元数据路径
            var aotAssembliesSrcDir = SettingsUtil.GetAssembliesPostIl2CppStripDir(target);
            // 资源路径
            var aotAssembliesDstDir = AssetsRoot + "Res/HotFixDll";

            foreach (var dll in Global.AOTMetaAssemblyNames)
            {
                // 路径
                var srcDllPath = $"{aotAssembliesSrcDir}/{dll}";
                if (!File.Exists(srcDllPath))
                {
                    Debug.LogError($"ab中添加AOT补充元数据dll:{srcDllPath} 时发生错误,文件不存在。裁剪后的AOT dll在BuildPlayer时才能生成，因此需要你先构建一次游戏App后再打包。");
                    continue;
                }
                // 扩展名
                var ext = Path.GetExtension(dll);
                // 资源路径
                var dllBytesPath = $"{aotAssembliesDstDir}/{dll.Replace(ext, "")}.bytes";
                // 复制
                File.Copy(srcDllPath, dllBytesPath, true);
                // log
                Debug.Log($"[CopyAOTAssembliesToStreamingAssets] copy AOT dll {srcDllPath} -> {dllBytesPath}");
            }
        }
        /// <summary>
        /// 复制合格程序集到资源路径
        /// </summary>
        private static void CopyHotUpdateAssembliesToStreamingAssets()
        {
            // 当前平台
            var target = EditorUserBuildSettings.activeBuildTarget;
            // 热更程序集路径
            var hotfixDllSrcDir = SettingsUtil.GetHotUpdateDllsOutputDirByTarget(target);
            // 资源路径
            var hotfixAssembliesDstDir = AssetsRoot + "Res/HotFixDll";
            
            foreach (var dll in SettingsUtil.HotUpdateAssemblyFilesExcludePreserved)
            {
                // 扩展名
                var ext = Path.GetExtension(dll);
                // 程序集路径
                var dllPath = $"{hotfixDllSrcDir}/{dll}";
                // 资源路径
                var dllBytesPath = $"{hotfixAssembliesDstDir}/{dll.Replace(ext, "")}.bytes";
                // 复制
                File.Copy(dllPath, dllBytesPath, true);
                // log
                Debug.Log($"[CopyHotUpdateAssembliesToStreamingAssets] copy hotfix dll {dllPath} -> {dllBytesPath}");
            }
        }
        #endregion
    }
}
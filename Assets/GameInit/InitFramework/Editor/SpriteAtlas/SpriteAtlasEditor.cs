#if UNITY_EDITOR
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using Cysharp.Threading.Tasks;
using GameLogic.HotfixFramework;
using UnityEditor;
using UnityEditor.U2D;
using UnityEngine;
using UnityEngine.U2D;
using Object = UnityEngine.Object;

namespace GameInit.Framework.Editor
{
    public class SpriteAtlasEditor
    {
        /// <summary>
        /// 图集所在图片的文件夹路径
        /// </summary>
        private const string AtlasPath = "Assets/Arts/UI/";
        /// <summary>
        /// 图集输出路径
        /// </summary>
        private const string OutPutPath = "Assets/Res/Atlas/{0}.spriteAtlas";

        
        [MenuItem("Assets/LFramework/生成所有图集", false, 14)]
        [MenuItem("LFramework/UI Atlas/生成所有图集", false, 15)]
        public static UniTask GenerateAllAtlas()
        {
            // ui资源根节点
            var rootDir = new DirectoryInfo(Application.dataPath+"/Arts/UI/");
            // 遍历
            foreach (var dir in rootDir.GetDirectories())
            {
                // 返回当前目录（或包括子目录）下与搜索字符串匹配的所有文件
                var files = dir.GetFiles("*.png", SearchOption.AllDirectories);
                // 图片列表
                var spriteStructs = new List<SpriteStruct>();
                foreach(var file in files)
                {
                    // 跳过 .meta 文件
                    if(file.FullName.EndsWith(".meta"))
                        continue;
                    // 加载图片
                    var obj = AssetDatabase.LoadMainAssetAtPath(DataPathToAssetPath(file.FullName));
                    // 图片路径
                    var path = AssetDatabase.GetAssetPath(obj);
                    // 加入列表
                    spriteStructs.Add(new SpriteStruct() {obj = obj, path = path});
                }
                
                // 该文件夹没有图片跳过
                if(spriteStructs.Count == 0)
                    continue;
                // 排序
                spriteStructs.Sort((a, b) => String.Compare(a.path, b.path, StringComparison.Ordinal));
                // 生成图集
                GeneratePngAtlasByGuid(spriteStructs, true);
            }
            // 刷新
            AssetDatabase.Refresh();
            return UniTask.CompletedTask;
        }
        [MenuItem("Assets/LFramework/生成图集 #&U", false, 14)]
        [MenuItem("LFramework/UI Atlas/生成当前文件夹图集", false, 14)]
        private static void BuildPNGAtlasIncluded()
        {
            GeneratePngPNGAtlas(true);
        }
        /// <summary>
        /// 生产图集
        /// </summary>
        /// <param name="includeInBuild"></param>
        private static void GeneratePngPNGAtlas(bool includeInBuild)
        {
            // 选择的文件
            var guid = Selection.assetGUIDs[0];
            var gPath = AssetDatabase.GUIDToAssetPath(guid);
            
            if(!gPath.Contains(AtlasPath))
            {
                Logger.LogError("所选择的文件夹路径有误，正确的要包含：" + AtlasPath);
                return;
            }
            // 加载图片
            var selectionObj = Selection.GetFiltered(typeof(Texture2D), SelectionMode.DeepAssets);
            var spriteStructs = new List<SpriteStruct>();
            // 图信息
            foreach (var obj in selectionObj)
            {
                var path = AssetDatabase.GetAssetPath(obj);
                spriteStructs.Add(new SpriteStruct()
                {
                    obj = obj,
                    path = path,
                });
            }
            // 排序
            spriteStructs.Sort((a, b) => a.path.CompareTo(b.path));
            // 生成图集
            GeneratePngAtlasByGuid(spriteStructs, includeInBuild);
        }

        /// <summary>
        /// 生产png图集
        /// </summary>
        /// <param name="spriteStructs">图片信息</param>
        /// <param name="includeInBuild"></param>
        private static void GeneratePngAtlasByGuid(List<SpriteStruct> spriteStructs, bool includeInBuild)
        {
            // 图集信息
            var atlasDic = new Dictionary<string,AtlasInfo>();
            // 遍历
            foreach (var spriteStruct  in spriteStructs)
            {
                // 路径
                var path = spriteStruct.path;
                // 文件名做图集名
                var atlasName = Path.GetFileName(Path.GetDirectoryName(path));

                if (!atlasDic.TryGetValue(atlasName, out var info))
                {
                    info = new AtlasInfo();
                    info.objs = new List<Object>();
                    atlasDic.Add(atlasName,info);
                }

                // 检查图片是否有alpha通道
                if (info.isHaveAlpha == false)
                    info.isHaveAlpha = IsHaveAlpha(path);

                // 添加图片
                info.objs.Add(spriteStruct.obj);
            }
            
            // 创建图集文件夹
            var spriteAtlasPath = "Assets/Res/Atlas";
            if (Directory.Exists(spriteAtlasPath))
                Directory.CreateDirectory(spriteAtlasPath);
            
            // 获取所有的图集
            var atlasAll = Directory.GetFiles(spriteAtlasPath,"*.spriteAtlas", SearchOption.AllDirectories);
            // 删除需要重新删除的图集
            for(int i = atlasAll.Length - 1; i >= 0; i--)
            {
                var fileName = Path.GetFileNameWithoutExtension(atlasAll[i]);
                if(atlasDic.ContainsKey(fileName))
                    File.Delete(atlasAll[i]);
            }
            
            // 存储所有图集信息的预制体
            var infoFile = spriteAtlasPath + "/AtlasInfoFile.prefab";
            GameObject atlasInfoObj;
            // 加载预制体
            var atlasInfoPrefab = AssetDatabase.LoadAssetAtPath<GameObject>(infoFile);
            // 预制体为空创建存储所有图集信息的预制体
            if (atlasInfoPrefab == null)
            {
                Logger.Log("所有图集信息的预制体=>" + infoFile);
                atlasInfoObj = new GameObject();
                PrefabUtility.SaveAsPrefabAsset(atlasInfoObj, infoFile);
            }
            // 获得
            else
                atlasInfoObj = GameObject.Instantiate(atlasInfoPrefab);
            
            // 图集信息
            var atlasInfos = atlasInfoObj.GetComponents<SpriteAtlasInfo>();
            var spriteAtlasInfoDic = new Dictionary<string, SpriteAtlasInfo>();
            foreach (var info in atlasInfos)
            {
                spriteAtlasInfoDic.Add(info.Key, info);
            }
            
            // 遍历图片
            foreach ( var kvp in atlasDic )
            {
                // 图集名
                var name = kvp.Key;
                // 图片对象
                var objs = kvp.Value.objs.ToArray();
                // 是否有alpha通道
                var isHaveAlpha = kvp.Value.isHaveAlpha;
                
                // 创建新图集
                SpriteAtlas spriteAtlas = new SpriteAtlas();
                SpriteAtlasPackingSettings spriteAtlasPackingSettings = spriteAtlas.GetPackingSettings();
                // 确定在打包过程中是否可以旋转。
                spriteAtlasPackingSettings.enableRotation = false;
                // 精确包装技术。当启用此选项时，打包器会更精确地计算Sprite的边界
                spriteAtlasPackingSettings.enableTightPacking = false;
                // 指定精灵图集中各个精灵纹理之间的像素数。这个设置作为一个缓冲区，
                spriteAtlasPackingSettings.padding = 2;
                spriteAtlas.SetPackingSettings(spriteAtlasPackingSettings);

                // ---------- iPhone 平台配置 ----------
                TextureImporterPlatformSettings iosPlatformSettings = spriteAtlas.GetPlatformSettings("iPhone");
                // 图集压缩格式
                iosPlatformSettings.format = TextureImporterFormat.ASTC_8x8;
                iosPlatformSettings.overridden = true;
                // 图集最大尺寸
                iosPlatformSettings.maxTextureSize = 2048;
                iosPlatformSettings.textureCompression = TextureImporterCompression.Compressed;
                // 是否压缩
                iosPlatformSettings.allowsAlphaSplitting = true;
                // 压缩的质量范围
                iosPlatformSettings.compressionQuality = 50;
                // 设置参数
                spriteAtlas.SetPlatformSettings(iosPlatformSettings);

                // ---------- android 平台配置 ----------
                TextureImporterPlatformSettings androidPlatformSettings = spriteAtlas.GetPlatformSettings("Android");
                androidPlatformSettings.format = TextureImporterFormat.ASTC_8x8;
                androidPlatformSettings.overridden = true;
                spriteAtlas.SetPlatformSettings(androidPlatformSettings);

                // ---------- windows 平台配置 ----------
                TextureImporterPlatformSettings wind64PlatformSettings = spriteAtlas.GetPlatformSettings("Standalone");
                wind64PlatformSettings.format = TextureImporterFormat.RGBA32;
                wind64PlatformSettings.overridden = true;
                spriteAtlas.SetPlatformSettings(wind64PlatformSettings);
                
                // 添加图片
                spriteAtlas.Add(objs);
                // true 不用手动在代码中加载图集
                spriteAtlas.SetIncludeInBuild(includeInBuild);
                // 打包测试
                spriteAtlas.SetPackingSettings(new SpriteAtlasPackingSettings() {enableRotation = true, padding = 4});
                Logger.Log("Build Atlas " + name);
                // 保存图集到本地
                AssetDatabase.CreateAsset(spriteAtlas, string.Format(OutPutPath, name));//"Assets/Res/Atlas/" + name + ".spriteAtlas");
                
                // 图片名
                string[] pngNames = new string[objs.Length];
                int i = 0;
                foreach ( var obj in objs )
                {
                    pngNames[i] = Path.GetFileNameWithoutExtension(AssetDatabase.GetAssetPath(obj));
                    i++;
                }

                // 保存新图集信息
                if (!spriteAtlasInfoDic.TryGetValue(name, out var saver))
                {
                    saver = atlasInfoObj.AddComponent<SpriteAtlasInfo>();
                    saver.Key = name;
                    saver.PngNames = pngNames;
                }
                // 更新图集信息
                else
                {
                    spriteAtlasInfoDic[name].PngNames = pngNames;
                }
            }
            // 保存图集信息预制体
            PrefabUtility.SaveAsPrefabAsset(atlasInfoObj, infoFile);
            // 和删除内存的预制体对象
            GameObject.DestroyImmediate(atlasInfoObj);
            // 刷新
            AssetDatabase.Refresh();
            // 打包图集
            SpriteAtlasUtility.PackAllAtlases(EditorUserBuildSettings.activeBuildTarget,false);
            // 遍历图集，判断图集是否超过2048
            foreach (var kvp in atlasDic)
            {
                GetTextureCount(AssetDatabase.LoadAssetAtPath(string.Format(OutPutPath, kvp.Key), typeof(SpriteAtlas)) as SpriteAtlas);
            }
            // 刷新
            AssetDatabase.Refresh();
            // log
            Logger.Log("Build Finish");
        }

        /// <summary>
        /// 获取spriteatlas生成的贴图个数
        /// </summary>
        /// <param name="atlas"></param>
        /// <returns></returns>
        private static int GetTextureCount(SpriteAtlas atlas)
        {
            var getPreviewTextureMi = typeof(SpriteAtlasExtensions).GetMethod("GetPreviewTextures", BindingFlags.Static | BindingFlags.NonPublic);
            
            var atlasTextures = (Texture2D[])getPreviewTextureMi.Invoke(null, new System.Object[] { atlas });
            var result = atlasTextures == null ? 0 : atlasTextures.Length;
            
            if(result > 1)
                Logger.LogError("这个图集太大了，超过了2048，去分割一下吧！当前图集名字：" + atlas.name + "<<<个数：" + result);
            
            return result;
        }

        /// <summary>
        /// 绝对路径转为工程目录路径
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        private static string DataPathToAssetPath(string path)
        {
            if (Application.platform == RuntimePlatform.WindowsEditor)
                return path.Substring(path.IndexOf("Assets\\"));
            else
                return path.Substring(path.IndexOf("Assets/"));
        }
        
        /// <summary>
        /// 检查纹理源图像是否具有alpha通道。
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        private static bool IsHaveAlpha(string path)
        {
            var import = AssetImporter.GetAtPath(path) as TextureImporter;
            return import.DoesSourceTextureHaveAlpha();
        }
            
        /// <summary>
        /// 图集信息
        /// </summary>
        private class AtlasInfo
        {
            public List<Object> objs;
            public bool isHaveAlpha;
        }
        
        /// <summary>
        /// 图片信息
        /// </summary>
        private struct SpriteStruct
        {
            public Object obj;
            public string path;
        }
    }
}
#endif



﻿namespace GameInit.Framework.Editor
{
    /// <summary>
    /// uiPrefab 模板
    /// </summary>
    public class UIPrefabTemplate
    {
        /// <summary>
        /// 弹窗Prefab 导出模板
        /// </summary>
        public static string DialogTemplate = 
            @"using UnityEngine;
using UnityEngine.UI;
using GameLogic.HotfixFramework;

namespace GameLogic.Hotfix
{
    public partial class #ClassName# : UIDialogView
    {
        protected override string PrefabPath { get =>#PrefabPath#; }

        #Member#
        
        public #ClassName#()
            : base()
        {
        }
        
        protected override void ParseComponent()
        {
            #Find#
        }        

    }    
}
";
        /// <summary>
        /// 窗口Prefab 导出模板
        /// </summary>
        public static string WindowTemplate = 
            @"using UnityEngine;
using UnityEngine.UI;
using GameLogic.HotfixFramework;

namespace GameLogic.Hotfix
{
    public partial class #ClassName# : UIWindowBase<#ClassName#>
    {
        protected override string PrefabPath { get =>#PrefabPath#; }

        #Member#    
        
        public #ClassName#()
        {
        }

        protected override void ParseComponent()
        {
            #Find#
        }
    }    

}
";
        /// <summary>
        /// 子界面 导出模板
        /// </summary>
        public static string SubViewTemplate = 
            @"using UnityEngine;
using UnityEngine.UI;
using GameLogic.HotfixFramework;

namespace GameLogic.Hotfix
{
    public partial class #ClassName# : SubView
    {
        #Member#
        
        protected override void ParseComponent()
        {
            #Find#
        }        

    }
} 
";
        
        /// <summary>
        /// 循环列表 导出模板
        /// </summary>
        public static string ListViewTemplate = 
            @"using UnityEngine;
using UnityEngine.UI;
using GameLogic.HotfixFramework;

namespace GameLogic.Hotfix
{
    public partial class #ClassName# : ListView
    {
        public override ScrollRect ScrollRectNode { get=>_scrollViewList; }

        #Member#
        
        public #ClassName#()
            : base()
        {
        }
        
        protected override void ParseComponent()
        {
            #Find#
        }        

    }    
}
";
        /// <summary>
        /// 循环列表Item 导出模板
        /// </summary>
        public static string ListViewItemTemplate = 
            @"using UnityEngine;
using UnityEngine.UI;
using GameLogic.HotfixFramework;

namespace GameLogic.Hotfix
{
    public partial class #ClassName# : ListViewItem
    {
        #Member#
        
        public #ClassName#()
            : base()
        {
        }
        
        protected override void ParseComponent()
        {
            #Find#
        }
        
        public override ListViewItem Instantiate(UIBaseView parentView, GameObject parent)
        {
             _parentView = parentView;
            #ClassName# item = new #ClassName#();
            item.SetDisplayObject(UIItemPool.GetUIItem(RectTransform).gameObject);
            item.SetParent(parent.GetComponent<RectTransform>());
            return item;
        }        
    }    
}
";
        /// <summary>
        /// Item 导出模板
        /// </summary>
        public static string ItemTemplate = 
            @"using UnityEngine;
using UnityEngine.UI;
using GameLogic.HotfixFramework;

namespace GameLogic.Hotfix
{
    public partial class #ClassName# : UIItemView
    {
        protected override string PrefabPath { get =>#PrefabPath#; }

        #Member#    
        
        public #ClassName#()
        {
        }

        protected override void ParseComponent()
        {
            #Find#
        }
    }    
}
";
    }
}
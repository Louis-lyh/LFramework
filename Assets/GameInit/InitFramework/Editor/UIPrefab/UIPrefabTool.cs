﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Cysharp.Text;
using UnityEditor;
using UnityEngine;

namespace GameInit.Framework.Editor
{
    /// <summary>
    /// UIPrefab 导出工具
    /// </summary>
    public class UIPrefabTool
    {
        /// <summary>
        /// 节点类型
        /// </summary>
        private static readonly List<string> ExportType = new List<string>()
        {
            "Image",
            "Button",
            "Text",
            "Node",
            "ScrollView",
            "Toggle",
            "InputField",
            "SubView",
            "SubViewItem",
            "ListView",
            "ListViewItem",
            "Spine",
            "Slider",
        };
        
        /// <summary>
        /// 节点对应的组件
        /// </summary>
        private static readonly Dictionary<string,string> ExportTypeToComponentName = new Dictionary<string, string>()
        {
            {"Image","Image"},
            {"Button","Button"},
            {"Text","Text"},
            {"Node","RectTransform"},
            {"ScrollView","ScrollRect"},
            {"Toggle","Toggle"},
            {"InputField","InputField"},
            {"SubView","SubView"},
            {"SubViewItem","SubViewItem"},
            {"ListView","ListView"},
            {"ListViewItem","ListViewItem"},
            {"Spine","SkeletonGraphic"},
            {"Slider","Slider"}
        };

        /// <summary>
        /// 导出窗口脚本路径
        /// </summary>
        private static readonly string ExportWindowPathRoot =
            Application.dataPath + "/GameLogic/Hotfix/AutoGenUI/UiWindow/{0}/";
        
        /// <summary>
        /// 自动生成弹窗目录
        /// </summary>
        private static readonly string ExportDialogPathRoot =
            Application.dataPath + "/GameLogic/Hotfix/AutoGenUI/UiDialog/{0}/";

        
        [MenuItem("Assets/LFramework/导出UI脚本/导出UIWindow",false, 10)]
        [MenuItem("LFramework/导出UI脚本/导出UIWindow",false, 10)]
        private static void CreateScript()
        {
            ExportScript(Selection.activeGameObject, ExportWindowPathRoot, "Window");
        }
        
        [MenuItem("Assets/LFramework/导出UI脚本/导出UIDialog",false, 10)]
        [MenuItem("LFramework/导出UI脚本/导出UIDialog",false, 10)]
        private static void CreateDialogScriptScript()
        {
            ExportScript(Selection.activeGameObject, ExportDialogPathRoot, "Dialog");
        }
        
        [MenuItem("Assets/LFramework/导出UI脚本/导出UIItem",false, 10)]
        [MenuItem("LFramework/导出UI脚本/导出UIItem",false, 10)]
        private static void CreateItemScriptScript()
        {
            ExportScript(Selection.activeGameObject, ExportDialogPathRoot, "Item");
        }
        
        /// <summary>
        /// 导出 UI 脚本
        /// </summary>
        /// <param name="gameObject">UI Prefab</param>
        /// <param name="path">导出路径</param>
        /// <param name="isWindowUI">是否为窗口Prefab</param>
        private static void ExportScript(GameObject gameObject,string path, string type)
        {
            if (gameObject == null)
            {
                Debug.LogError("UIPrefab.ExportScript() => 导出脚本失败，ui Prefab 对象为空");
                return;
            }
            
            // 初始化根节点
            var root = new Node(gameObject,gameObject.name);
            
            // 遍历子节点
            TraverseChildNode(gameObject, root);
            
            // 子节点为空
            if(root.Children.Count == 0)
                return;
            
            // 导出名字
            var exportName = gameObject.name;
            // 导出路径
            var exportPath = ZString.Format(path,gameObject.name);
            // 创建路径
            if(Directory.Exists(exportPath))
                Directory.Delete(exportPath, true);
            Directory.CreateDirectory(exportPath);
            
            // 创建ui脚本模板
            GenerateTemplate(root,exportPath,out var members,out var findStr);
        
            var windowTemplate = string.Empty;
            
            // 窗口
            if (type.Equals("Window"))
            {
                windowTemplate = UIPrefabTemplate.WindowTemplate.Replace("#Member#", members);
            }
            // 弹窗
            else if (type.Equals("Dialog"))
            {
                windowTemplate = UIPrefabTemplate.DialogTemplate.Replace("#Member#", members);
            }
            else if (type.Equals("Item"))
            {
                windowTemplate = UIPrefabTemplate.ItemTemplate.Replace("#Member#", members);
            }
            windowTemplate = windowTemplate.Replace("#Find#", findStr);
            windowTemplate = windowTemplate.Replace("#ClassName#", exportName);
            windowTemplate = windowTemplate.Replace("#PrefabPath#", "\"" + gameObject.name + "\"");
            WriteFile(exportPath + exportName + "_UI.cs", windowTemplate);

            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
            Debug.Log("脚本导出完成");
        }
        
        /// <summary>
        /// 遍历子节点
        /// </summary>
        /// <param name="root">根节点</param>
        /// <param name="node">节点信息</param>
        /// <param name="parentPath">父节点路径</param>
        private static void TraverseChildNode(GameObject root,Node node,string parentPath = "")
        {
            for (int i = 0; i < root.transform.childCount; i++)
            {
                var child = root.transform.GetChild(i);
                // 节点路径
                var childPath = string.IsNullOrEmpty(parentPath) ? child.name : parentPath + "/" + child.name;
                
                // 子节点类型
                var childNameArray = child.name.Split('_');
                string childType = String.Empty;
                if (childNameArray.Length > 1)
                    childType = childNameArray[0];

                // 需要导出的节点
                if (ExportType.Contains(childType))
                {
                    // 新建节点
                    var childNode = new Node(child.gameObject,childNameArray[1]);
                    // 记录节点路径
                    childNode._path = childPath;
                    // 组件类型
                    childNode._comppnentName = ExportTypeToComponentName[childType];
                    
                    // 添加子节点
                    node.AddChildren(childNode);
                    
                    // 属于单独模块
                    var isView = true;
                    // 子节点属于单独的模块
                    switch (childType)
                    {
                        case "SubView":
                            childNode._nodeType = NodeType.SubView;
                            break;
                        case "ListView":
                            childNode._nodeType = NodeType.ListView;
                            break;
                        case "SubViewItem":
                            childNode._nodeType = NodeType.SubViewItem;
                            break;
                        case "ListViewItem":
                            childNode._nodeType = NodeType.ListViewItem;
                            break;
                        default:
                            isView = false;
                            break;
                    }
                    
                    // 遍历子节点
                    if (isView)
                    {
                        TraverseChildNode(child.gameObject,childNode,"");
                        continue;
                    }
                }
                
                // 遍历子节点
                TraverseChildNode(child.gameObject,node,childPath);
            }
        }

        /// <summary>
        /// 创建脚本模板
        /// </summary>
        /// <param name="root">根节点信息</param>
        /// <param name="members">成员字段</param>
        /// <param name="findStr">成员字段路径</param>
        private static void GenerateTemplate(Node root,string rootPath,out string members, out string findStr)
        {
            members = "";
            findStr = "";

            for (var i = 0; i < root.Children.Count; i++)
            {
                var node = root.Children[i];

                // 规范名字
                var name = node._gameObject.name.Replace("_", "");
                name = "_" + name.Substring(0,1).ToLower() + name.Substring(1);
                
                // 子界面
                if (node._nodeType != NodeType.Normal)
                {
                    // 创建子界面模板
                    GenerateTemplate(node,rootPath,out var subMembers,out var subFindStr);
                
                    var subViewStr = string.Empty;
                    // 子界面模板
                    switch (node._nodeType)
                    {
                        // 循环列表
                        case NodeType.ListView:
                            subViewStr = UIPrefabTemplate.ListViewTemplate;
                            break;
                        // 循环列表Item
                        case NodeType.ListViewItem:
                            subViewStr = UIPrefabTemplate.ListViewItemTemplate;
                            break;
                        // 子界面
                        default:
                            subViewStr = UIPrefabTemplate.SubViewTemplate;
                            break;
                    }
                    
                    // 类名
                    var className = node._nodeType + node._name;
                    
                    // 成员字段
                    subViewStr = subViewStr.Replace("#Member#", subMembers);
                    // 成员字段路径
                    subViewStr = subViewStr.Replace("#Find#", subFindStr);
                    // 类名
                    subViewStr = subViewStr.Replace("#ClassName#", className);
                    
                    // 写入磁盘
                    WriteFile(rootPath + className + "_UI.cs",subViewStr);
                    
                    // 编写路径
                    findStr += $"{name} = new {className}();\n\t\t\t";
                    findStr += $"{name}.SetDisplayObject(Find(\"{node._path}\"));\n\t\t\t";
                    
                    // 成员字段
                    members += $"private {className} {name};\n\t\t";
                }
                // 节点
                else
                {
                    // 编写路径
                    findStr += $"{name} = Find<{node._comppnentName}>(\"{node._path}\");\n\t\t\t";
                    if(node._comppnentName == "Button")
                        findStr += $"ListenButton({name});\n\t\t\t";
                    else if(node._comppnentName == "InputField")
                        findStr += $"ListenInput({name});\n\t\t\t";
                    // 成员字段
                    members += $"private {node._comppnentName} {name};\n\t\t";
                }
            }
        }
        
        /// <summary>
        /// 脚本写入磁盘
        /// </summary>
        /// <param name="file">脚本路径</param>
        /// <param name="fileContent">脚本内容</param>
        private static void WriteFile(string file, string fileContent)
        {
            // 文件写入
            FileStream fileStream;
            if (!File.Exists(file))
            {
                fileStream = File.Create(file);
            }
            else
            {
                fileStream = File.Open(file, FileMode.Truncate);
            }
            StreamWriter writer = new StreamWriter(fileStream, Encoding.UTF8);
            writer.Write(fileContent);
            writer.Close();
        }



        /// <summary>
        /// UI节点
        /// </summary>
        internal class Node
        {
            // 名字
            internal readonly string _name;
            /// <summary>
            /// 节点对象
            /// </summary>
            internal readonly GameObject _gameObject;
            /// <summary>
            /// 节点路径
            /// </summary>
            internal string _path;
            /// <summary>
            /// 节点类型
            /// </summary>
            internal NodeType _nodeType = NodeType.Normal;

            /// <summary>
            /// 组件类型
            /// </summary>
            /// <returns></returns>
            internal string _comppnentName;
            /// <summary>
            /// 子节点
            /// </summary>
            internal List<Node> Children { get; } = new List<Node>();

            internal Node(GameObject obj,string name)
            {
                _gameObject = obj;
                _name = name;
            }
            /// <summary>
            /// 添加子节点
            /// </summary>
            /// <param name="node"></param>
            /// <returns></returns>
            internal Node AddChildren(Node node)
            {
                if (!Children.Contains(node))
                {
                    Children.Add(node);
                }

                return this;
            }

        }
        
        internal enum NodeType
        {
            SubView,
            SubViewItem,
            ListView,
            ListViewItem,
            Normal,
        }
    }
}
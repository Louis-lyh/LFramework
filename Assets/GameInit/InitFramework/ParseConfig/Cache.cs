﻿namespace Framework.Editor
{
    public static class Cache
    {
        private static string _dbOutputFolder = "Assets/GameLogic/Hotfix/Config";
        private static string _configOutputFolder = "Assets/GameLogic/Hotfix/Config";

        private static string _assetOutputFolder = "Assets/Res/Config";
        public static string _assetOutPutJsonFolder = "Assets/Res/JsonConfig";

        public static string ConfigOutputFolder => _configOutputFolder;
        public static string AssetOutputFolder => _assetOutputFolder;
        // public static string AssetOutPutJsonFolder => _assetOutPutJsonFolder;
        public static string DefOutputFolder => _dbOutputFolder + "/Def"; 
    }
}
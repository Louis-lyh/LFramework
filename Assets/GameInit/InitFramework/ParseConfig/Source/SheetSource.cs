﻿
namespace Framework.Editor
{
    /// <summary>
    /// 表格（.txt .csv .xls .xlsx）
    /// </summary>
    public class SheetSource : Source
    {
        /// <summary>
        /// 解析出来的矩阵
        /// </summary>
        private string[,] _matrix;
    
        public void SetMatrix(string[,] matrix)
        {
            _matrix = matrix;
        }
    
        public string GetComment(int col)
        {
            return _matrix[ConfigConstants.ROW_COMMENT, col];
        }
    
        public string GetType(int col)
        {
            return _matrix[ConfigConstants.ROW_TYPE, col];
        }
    
        public string GetField(int col)
        {
            return _matrix[ConfigConstants.ROW_FIELD, col];
        }
    
        public string GetValue(int row, int col)
        {
            return _matrix[row, col];
        }
    
        public bool IsClient(int col)
        {
            var tmpStr = _matrix[ConfigConstants.ROW_ISCLIENT, col];
            if (string.IsNullOrEmpty(tmpStr))
                return true;
            return tmpStr != "#";
        }
    
        /// <summary>
        /// 行
        /// </summary>
        public int row { get { return _matrix.GetLength(0); } }
    
        /// <summary>
        /// 列
        /// </summary>
        public int column { get { return _matrix.GetLength(1); } }
    }
}



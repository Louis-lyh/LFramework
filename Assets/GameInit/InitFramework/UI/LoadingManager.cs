﻿using Cysharp.Threading.Tasks;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace GameInit.Framework
{
    /// <summary>
    /// 加载页面管理
    /// </summary>
    public class LoadingManager : Singleton<LoadingManager>
    {
        
        // loading 界面
        private LoadingView _loadingView;
        
        /// <summary>
        /// 初始化加载页面
        /// </summary>
        /// <returns></returns>
        public async UniTask InitLoadingView()
        {
            if (_loadingView != null)
                return;
            
            // 加载ui预制体
            var loadingObject = await ResourceManager.ConstructObjectAsync(ResPathUtils.GetUIPrefab("uiLoading"));
            if (loadingObject == null)                
            {
                Logger.LogError("LoadingManager.ShowLoading() => 找不到uiLoading显示对象");
                return;
            }
            loadingObject.name = "uiLoading";
            
            // 实例化 loading 页面对象
            _loadingView = new LoadingView();
            _loadingView.SetDisplayObject(loadingObject);
            // 初始化 loading 对象属性
            var parent = GameObject.Find("TopRoot").transform;
            _loadingView.RectTransform.SetParent(parent, false);
            _loadingView.RectTransform.localPosition = Vector3.zero;
            _loadingView.RectTransform.localScale = Vector3.one;
        }
        
        /// <summary>
        /// 显示loading页面
        /// </summary>
        /// <param name="content"></param>
        /// <param name="progress"></param>
        public void ShowLoading(string content, float progress = 1f)
        {
            if (_loadingView == null)
                return;
            _loadingView.ShowLoading(content, progress);
            _loadingView.RectTransform.SetAsLastSibling();
        }
        /// <summary>
        /// 隐藏loading页面
        /// </summary>
        /// <param name="immediateHide"></param>
        public void HideLoading(bool immediateHide = true)
        {
            _loadingView?.HideLoading(immediateHide);
        }
        /// <summary>
        /// 刷新进度
        /// </summary>
        /// <param name="progress"></param>
        public void UpdateProgress(float progress)
        {
            _loadingView?.UpdateProgress(progress);
        }
        /// <summary>
        /// 关闭loading页面
        /// </summary>
        public void CloseLoading()
        {
            _loadingView?.Close();
        }
        
        /// <summary>
        /// loading 页面
        /// </summary>
        private class LoadingView
        {
            /// <summary>
            /// 加载提示文字
            /// </summary>
            private Text _contentText;
            /// <summary>
            /// loading页面游戏对象
            /// </summary>
            private GameObject _loadingObject;
            public Transform RectTransform;
            /// <summary>
            /// loading 进度条
            /// </summary>
            private Image _barImage;
            
            /// <summary>
            /// 设置游戏对象
            /// </summary>
            /// <param name="gameObject"></param>
            internal void SetDisplayObject(GameObject gameObject)
            {
                _loadingObject = gameObject;
                RectTransform = gameObject.GetComponent<RectTransform>();
                _contentText = _loadingObject.transform.Find("ProgressBar/Text_Progress").GetComponent<Text>();
                _barImage = _loadingObject.transform.Find<Image>("ProgressBar/Image_Progress");
            }
            
            /// <summary>
            /// 显示加载页面
            /// </summary>
            /// <param name="content">加载提示内容</param>
            /// <param name="progress">进度</param>
            internal void ShowLoading(string content, float progress = 0f)
            {
                // 刷新进度
                UpdateProgress(progress, true);
                // 显示界面
                _loadingObject.SetActive(true);
                // 显示提示内容
                _contentText.text = string.IsNullOrEmpty(content) ? "Loading Server Resources..." : content;
            }    
            /// <summary>
            /// 刷新进度
            /// </summary>
            /// <param name="progress">进度</param>
            /// <param name="isForceProgress">是否跳过动画</param>
            internal void UpdateProgress(float progress, bool isForceProgress = false)
            {
                progress = Mathf.Min(progress, 1f);
                if (isForceProgress)
                {
                    _barImage.DOKill();
                    _barImage.fillAmount = progress;
                }
                else
                {
                    _barImage.DOKill();
                    _barImage.DOFillAmount(progress, 1.5f);
                }
            }
            
            /// <summary>
            /// 隐藏页面
            /// </summary>
            /// <param name="immediateHide"></param>
            internal void HideLoading(bool immediateHide = true)
            {
                if (immediateHide)
                    Hide();
                else
                    _barImage.DOFillAmount(1f, 0.3f).OnComplete(()=>Hide());
            }
            /// <summary>
            /// 隐藏页面
            /// </summary>
            internal void Hide()
            {
                _loadingObject.SetActive(false);
                _barImage.fillAmount = 0f;
                _barImage.DOKill();
                _contentText.text = "Server Resource Load Completed...";
            }
            /// <summary>
            /// 关闭界面
            /// </summary>
            internal void Close()
            {
                _loadingObject.SetActive(false);
            }
        }
    }
}
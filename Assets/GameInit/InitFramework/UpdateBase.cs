
namespace GameInit.Framework
{
    public abstract class UpdateBase
    {
        protected bool _isInitialed;

        protected virtual void OnInitialize()
        {
            _isInitialed = true;
        }
        
        public void Update(float deltaTime)
        {
            if (!_isInitialed)
                return;
            OnUpdate(deltaTime);
        }
        
        public void Dispose()
        {
            _isInitialed = false;
            OnDispose();
        }

        protected virtual void OnUpdate(float deltaTime)
        {
            
        }
        
        protected virtual void OnDispose()
        {
            
        }
    }
}
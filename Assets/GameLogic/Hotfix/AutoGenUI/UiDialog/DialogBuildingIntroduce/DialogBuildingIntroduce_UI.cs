﻿using UnityEngine;
using UnityEngine.UI;
using GameLogic.HotfixFramework;

namespace GameLogic.Hotfix
{
    public partial class DialogBuildingIntroduce : UIDialogView
    {
        protected override string PrefabPath { get =>"DialogBuildingIntroduce"; }

        private RectTransform _nodeLeft;
		private Button _buttonClose;
		private RectTransform _nodeHeadquarters;
		private RectTransform _nodeEnergy;
		private RectTransform _nodeArsenalA;
		private RectTransform _nodeArsenalB;
		
        
        public DialogBuildingIntroduce()
            : base()
        {
        }
        
        protected override void ParseComponent()
        {
            _nodeLeft = Find<RectTransform>("Node_Left");
			_buttonClose = Find<Button>("Node_Left/Button_Close");
			ListenButton(_buttonClose);
			_nodeHeadquarters = Find<RectTransform>("Node_Headquarters");
			_nodeEnergy = Find<RectTransform>("Node_Energy");
			_nodeArsenalA = Find<RectTransform>("Node_Arsenal_A");
			_nodeArsenalB = Find<RectTransform>("Node_Arsenal_B");
			
        }        

    }    
}

﻿using UnityEngine;
using UnityEngine.UI;
using GameLogic.HotfixFramework;

namespace GameLogic.Hotfix
{
    public partial class DialogGameSet : UIDialogView
    {
        protected override string PrefabPath { get =>"DialogGameSet"; }

        private Button _buttonQuit;
		private Toggle _toggleSoun;
		private RectTransform _nodeSounSphere;
		private Toggle _toggleMusic;
		private RectTransform _nodeMusicSphere;
		private Button _buttonClose;
		
        
        public DialogGameSet()
            : base()
        {
        }
        
        protected override void ParseComponent()
        {
            _buttonQuit = Find<Button>("Root/Button_Quit");
			ListenButton(_buttonQuit);
			_toggleSoun = Find<Toggle>("Root/uiSound/Toggle_Soun");
			_nodeSounSphere = Find<RectTransform>("Root/uiSound/Toggle_Soun/Node_SounSphere");
			_toggleMusic = Find<Toggle>("Root/uiMusic/Toggle_Music");
			_nodeMusicSphere = Find<RectTransform>("Root/uiMusic/Toggle_Music/Node_MusicSphere");
			_buttonClose = Find<Button>("Root/Button_Close");
			ListenButton(_buttonClose);
			
        }        

    }    
}

﻿using UnityEngine;
using UnityEngine.UI;
using GameLogic.HotfixFramework;

namespace GameLogic.Hotfix
{
    public partial class DialogLevelInfo : UIDialogView
    {
        protected override string PrefabPath { get =>"DialogLevelInfo"; }

        private RectTransform _nodeLeft;
		private Button _buttonClose;
		private Toggle _toggleEasy;
		private Toggle _toggleDifficulty;
		private Toggle _toggleInfernal;
		private RectTransform _nodeEasyLevelInfo;
		private RectTransform _nodeDifficultyLevelInfo;
		private RectTransform _nodeInfernalLevelInfo;
		
        
        public DialogLevelInfo()
            : base()
        {
        }
        
        protected override void ParseComponent()
        {
            _nodeLeft = Find<RectTransform>("Node_Left");
			_buttonClose = Find<Button>("Node_Left/Button_Close");
			ListenButton(_buttonClose);
			_toggleEasy = Find<Toggle>("Top/Toggle_Easy");
			_toggleDifficulty = Find<Toggle>("Top/Toggle_Difficulty");
			_toggleInfernal = Find<Toggle>("Top/Toggle_Infernal");
			_nodeEasyLevelInfo = Find<RectTransform>("LevelList/Viewport/Content/Node_EasyLevelInfo");
			_nodeDifficultyLevelInfo = Find<RectTransform>("LevelList/Viewport/Content/Node_DifficultyLevelInfo");
			_nodeInfernalLevelInfo = Find<RectTransform>("LevelList/Viewport/Content/Node_InfernalLevelInfo");
			
        }        

    }    
}

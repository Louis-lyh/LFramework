﻿using UnityEngine;
using UnityEngine.UI;
using GameLogic.HotfixFramework;

namespace GameLogic.Hotfix
{
    public partial class DialogLoading : UIDialogView
    {
        protected override string PrefabPath { get =>"DialogLoading"; }

        private Image _imageProgress;
		private Text _textProgress;
		
        
        public DialogLoading()
            : base()
        {
        }
        
        protected override void ParseComponent()
        {
            _imageProgress = Find<Image>("ProgressBar/Image_Progress");
			_textProgress = Find<Text>("ProgressBar/Text_Progress");
			
        }        

    }    
}

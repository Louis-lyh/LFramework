﻿using UnityEngine;
using UnityEngine.UI;
using GameLogic.HotfixFramework;

namespace GameLogic.Hotfix
{
    public partial class DialogPlayerInfo : UIDialogView
    {
        protected override string PrefabPath { get =>"DialogPlayerInfo"; }

        private InputField _inputFieldName;
		private Button _buttonClose;
		
        
        public DialogPlayerInfo()
            : base()
        {
        }
        
        protected override void ParseComponent()
        {
            _inputFieldName = Find<InputField>("Root/Name/InputField_Name");
			ListenInput(_inputFieldName);
			_buttonClose = Find<Button>("Root/Button_Close");
			ListenButton(_buttonClose);
			
        }        

    }    
}

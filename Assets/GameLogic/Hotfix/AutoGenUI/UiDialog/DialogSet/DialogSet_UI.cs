﻿using UnityEngine;
using UnityEngine.UI;
using GameLogic.HotfixFramework;

namespace GameLogic.Hotfix
{
    public partial class DialogSet : UIDialogView
    {
        protected override string PrefabPath { get =>"DialogSet"; }

        private Button _buttonClose;
		private Button _buttonOut;
		private Toggle _toggleSound;
		private RectTransform _nodeSoundSphere;
		private Toggle _toggleMusic;
		private RectTransform _nodeMusicSphere;
		
        
        public DialogSet()
            : base()
        {
        }
        
        protected override void ParseComponent()
        {
            _buttonClose = Find<Button>("Root/Button_Close");
			ListenButton(_buttonClose);
			_buttonOut = Find<Button>("Root/Button_Out");
			ListenButton(_buttonOut);
			_toggleSound = Find<Toggle>("Root/uiSound/Toggle_Sound");
			_nodeSoundSphere = Find<RectTransform>("Root/uiSound/Toggle_Sound/Node_SoundSphere");
			_toggleMusic = Find<Toggle>("Root/uiMusic/Toggle_Music");
			_nodeMusicSphere = Find<RectTransform>("Root/uiMusic/Toggle_Music/Node_MusicSphere");
			
        }        

    }    
}

﻿using UnityEngine;
using UnityEngine.UI;
using GameLogic.HotfixFramework;

namespace GameLogic.Hotfix
{
    public partial class DialogShowReward : UIDialogView
    {
        protected override string PrefabPath { get =>"DialogShowReward"; }

        private Text _textTitle;
		private RectTransform _nodeRewards;
		private Button _buttonClose;
		private Text _textClose;
		
        
        public DialogShowReward()
            : base()
        {
        }
        
        protected override void ParseComponent()
        {
            _textTitle = Find<Text>("Root/Text_Title");
			_nodeRewards = Find<RectTransform>("Root/Node_Rewards");
			_buttonClose = Find<Button>("Root/Button_Close");
			_textClose = Find<Text>("Root/Text_Close");
			
        }        

    }    
}

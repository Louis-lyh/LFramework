﻿using UnityEngine;
using UnityEngine.UI;
using GameLogic.HotfixFramework;

namespace GameLogic.Hotfix
{
    public partial class DialogTankIntroduce : UIDialogView
    {
        protected override string PrefabPath { get =>"DialogTankIntroduce"; }

        private RectTransform _nodeTop;
		private Image _imageTank;
		private Text _textTankName;
		private Slider _sliderHp;
		private Slider _sliderAttack;
		private Slider _sliderSpeed;
		private Slider _sliderView;
		private RectTransform _nodeBottom;
		private RectTransform _nodeLeft;
		private Button _buttonClose;
		private Button _buttonTitle;
		
        
        public DialogTankIntroduce()
            : base()
        {
        }
        
        protected override void ParseComponent()
        {
            _nodeTop = Find<RectTransform>("Node_Top");
			_imageTank = Find<Image>("Node_Top/Image_Tank");
			_textTankName = Find<Text>("Node_Top/Text_TankName");
			_sliderHp = Find<Slider>("Node_Top/AttributePanel/Attribute/uiHp/Slider_Hp");
			_sliderAttack = Find<Slider>("Node_Top/AttributePanel/Attribute/uiAttack/Slider_Attack");
			_sliderSpeed = Find<Slider>("Node_Top/AttributePanel/Attribute/uiSpeed/Slider_Speed");
			_sliderView = Find<Slider>("Node_Top/AttributePanel/Attribute/uiView/Slider_View");
			_nodeBottom = Find<RectTransform>("Node_Bottom");
			_nodeLeft = Find<RectTransform>("Node_Left");
			_buttonClose = Find<Button>("Node_Left/Button_Close");
			ListenButton(_buttonClose);
			_buttonTitle = Find<Button>("Node_Left/Button_Title");
			ListenButton(_buttonTitle);
			
        }        

    }    
}

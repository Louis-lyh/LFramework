﻿using UnityEngine;
using UnityEngine.UI;
using GameLogic.HotfixFramework;

namespace GameLogic.Hotfix
{
    public partial class DialogTips : UIDialogView
    {
        protected override string PrefabPath { get =>"DialogTips"; }

        private Text _textTitle;
		private Button _buttonClose;
		private ScrollRect _scrollViewDes;
		private Text _textContent;
		
        
        public DialogTips()
            : base()
        {
        }
        
        protected override void ParseComponent()
        {
            _textTitle = Find<Text>("Root/Text_Title");
			_buttonClose = Find<Button>("Root/Button_Close");
			ListenButton(_buttonClose);
			_scrollViewDes = Find<ScrollRect>("Root/ScrollView_Des");
			_textContent = Find<Text>("Root/ScrollView_Des/Viewport/Text_Content");
			
        }        

    }    
}

﻿using UnityEngine;
using UnityEngine.UI;
using GameLogic.HotfixFramework;

namespace GameLogic.Hotfix
{
    public partial class ItemLevelInfo : UIItemView
    {
        protected override string PrefabPath { get =>"ItemLevelInfo"; }

        private Image _imageHeadFrame;
		private Image _imageHeadIcon;
		private Text _textTankCount;
		private Text _textNamw;
		private Text _textMapScale;
		private Text _textCoin;
		private Text _textTime;
		private Button _buttonStart;
		    
        
        public ItemLevelInfo()
        {
        }

        protected override void ParseComponent()
        {
            _imageHeadFrame = Find<Image>("Image_HeadFrame");
			_imageHeadIcon = Find<Image>("Image_HeadFrame/Image_HeadIcon");
			_textTankCount = Find<Text>("IMage_TankIcon/Text_TankCount");
			_textNamw = Find<Text>("IMage_TankIcon/Text_Namw");
			_textMapScale = Find<Text>("MapScale/Text_MapScale");
			_textCoin = Find<Text>("Coins/Text_Coin");
			_textTime = Find<Text>("LimitTime/Text_Time");
			_buttonStart = Find<Button>("Button_Start");
			ListenButton(_buttonStart);
			
        }
    }    
}

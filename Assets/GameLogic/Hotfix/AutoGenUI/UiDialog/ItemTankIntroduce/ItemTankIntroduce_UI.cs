﻿using UnityEngine;
using UnityEngine.UI;
using GameLogic.HotfixFramework;

namespace GameLogic.Hotfix
{
    public partial class ItemTankIntroduce : UIItemView
    {
        protected override string PrefabPath { get =>"ItemTankIntroduce"; }

        private Button _buttonClick;
		private Image _imageIcon;
		private RectTransform _nodeSelect;
		    
        
        public ItemTankIntroduce()
        {
        }

        protected override void ParseComponent()
        {
            _buttonClick = Find<Button>("Button_Click");
			ListenButton(_buttonClick);
			_imageIcon = Find<Image>("Button_Click/Image_Icon");
			_nodeSelect = Find<RectTransform>("Button_Click/Node_Select");
			
        }
    }    
}

﻿using UnityEngine;
using UnityEngine.UI;
using GameLogic.HotfixFramework;

namespace GameLogic.Hotfix
{
    public partial class WindowGame : UIWindowBase<WindowGame>
    {
        protected override string PrefabPath { get =>"WindowGame"; }

        private RectTransform _nodeStartTips;
		private Image _imageHeadLeft;
		private Text _textLeftName;
		private Image _imageRght;
		private Text _textRightName;
		private Button _buttonStartBtn;
		private Text _textDes;
		private Button _buttonQuit;
		private Text _textCoins;
		private Text _textTime;
		private Button _buttonSet;
		private RectTransform _nodeBlock;
		private RectTransform _nodeTeam;
		    
        
        public WindowGame()
        {
        }

        protected override void ParseComponent()
        {
            _nodeStartTips = Find<RectTransform>("Root/Node_StartTips");
			_imageHeadLeft = Find<Image>("Root/Node_StartTips/Panel/HeadLeft/Image_HeadLeft");
			_textLeftName = Find<Text>("Root/Node_StartTips/Panel/HeadLeft/Text_LeftName");
			_imageRght = Find<Image>("Root/Node_StartTips/Panel/HeadRight/Image_Rght");
			_textRightName = Find<Text>("Root/Node_StartTips/Panel/HeadRight/Text_RightName");
			_buttonStartBtn = Find<Button>("Root/Node_StartTips/Panel/Button_StartBtn");
			ListenButton(_buttonStartBtn);
			_textDes = Find<Text>("Root/Node_StartTips/Panel/Content/Text_Des");
			_buttonQuit = Find<Button>("Root/Node_StartTips/Panel/Button_Quit");
			ListenButton(_buttonQuit);
			_textCoins = Find<Text>("Root/uiUpperLeft/Coins/Text_Coins");
			_textTime = Find<Text>("Root/uiUpperLeft/TimeTips/Text_Time");
			_buttonSet = Find<Button>("Root/Button_Set");
			ListenButton(_buttonSet);
			_nodeBlock = Find<RectTransform>("Root/Node_Block");
			_nodeTeam = Find<RectTransform>("Root/Bottom/Node_Team");
			
        }
    }    

}

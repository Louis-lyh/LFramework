﻿using UnityEngine;
using UnityEngine.UI;
using GameLogic.HotfixFramework;

namespace GameLogic.Hotfix
{
    public partial class ListViewItemList : ListViewItem
    {
        private Text _textName;
		private Button _buttonClick;
		
        
        public ListViewItemList()
            : base()
        {
        }
        
        protected override void ParseComponent()
        {
            _textName = Find<Text>("Text_Name");
			_buttonClick = Find<Button>("Button_Click");
			
        }
        
        public override ListViewItem Instantiate(UIBaseView parentView, GameObject parent)
        {
             _parentView = parentView;
            ListViewItemList item = new ListViewItemList();
            item.SetDisplayObject(UIItemPool.GetUIItem(RectTransform).gameObject);
            item.SetParent(parent.GetComponent<RectTransform>());
            return item;
        }        
    }    
}

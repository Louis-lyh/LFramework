﻿using UnityEngine;
using UnityEngine.UI;
using GameLogic.HotfixFramework;

namespace GameLogic.Hotfix
{
    public partial class ListViewLevel : ListView
    {
        public override ScrollRect ScrollRectNode { get=>_scrollViewList; }

        private Text _textName;
		private ScrollRect _scrollViewList;
		private Button _buttonClick;
		private ListViewItemList _listViewItemList;
		
        
        public ListViewLevel()
            : base()
        {
        }
        
        protected override void ParseComponent()
        {
            _textName = Find<Text>("Text_Name");
			_scrollViewList = Find<ScrollRect>("ScrollView_List");
			_buttonClick = Find<Button>("Button_Click");
			_listViewItemList = new ListViewItemList();
			_listViewItemList.SetDisplayObject(Find("ListViewItem_List"));
			
        }        

    }    
}

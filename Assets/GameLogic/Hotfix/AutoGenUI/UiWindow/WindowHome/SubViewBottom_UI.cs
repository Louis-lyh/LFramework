﻿using UnityEngine;
using UnityEngine.UI;
using GameLogic.HotfixFramework;

namespace GameLogic.Hotfix
{
    public partial class SubViewBottom : SubView
    {
        private Text _textName;
		private Button _buttonClick;
		
        
        protected override void ParseComponent()
        {
            _textName = Find<Text>("Text_Name");
			_buttonClick = Find<Button>("Button_Click");
			
        }        

    }
} 

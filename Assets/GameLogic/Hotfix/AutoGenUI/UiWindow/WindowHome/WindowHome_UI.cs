﻿using UnityEngine;
using UnityEngine.UI;
using GameLogic.HotfixFramework;

namespace GameLogic.Hotfix
{
    public partial class WindowHome : UIWindowBase<WindowHome>
    {
        protected override string PrefabPath { get =>"WindowHome"; }

        private Text _textTitle;
		private Button _buttonStart;
		private ListViewLevel _listViewLevel;
		private SubViewBottom _subViewBottom;
		private Button _buttonClose;
		    
        
        public WindowHome()
        {
        }

        protected override void ParseComponent()
        {
            _textTitle = Find<Text>("Root/Text_Title");
			_buttonStart = Find<Button>("Root/Button_Start");
			_listViewLevel = new ListViewLevel();
			_listViewLevel.SetDisplayObject(Find("Root/ListView_Level"));
			_subViewBottom = new SubViewBottom();
			_subViewBottom.SetDisplayObject(Find("Root/SubView_Bottom"));
			_buttonClose = Find<Button>("Root/Button_Close");
			
        }
    }    

}

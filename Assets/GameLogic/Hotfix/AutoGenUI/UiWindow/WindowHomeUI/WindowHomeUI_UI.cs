﻿using UnityEngine;
using UnityEngine.UI;
using GameLogic.HotfixFramework;

namespace GameLogic.Hotfix
{
    public partial class WindowHomeUI : UIWindowBase<WindowHomeUI>
    {
        protected override string PrefabPath { get =>"WindowHomeUI"; }

        private RectTransform _nodeRightPanel;
		private Button _buttonSetting;
		private Text _textTaskPoint;
		private Button _buttonTask;
		private Text _textTaskModel;
		private RectTransform _nodeLeftPanel;
		private Button _buttonHead;
		private Image _imageHead;
		private Text _textName;
		private Button _buttonIntroduce;
		private Button _buttonTank;
		private Button _buttonBuilding;
		private Button _buttonGameIntroduction;
		private Button _buttonMarket;
		private RectTransform _nodeBottom;
		private Button _buttonMatch;
		private Button _buttonMachine;
		    
        
        public WindowHomeUI()
        {
        }

        protected override void ParseComponent()
        {
            _nodeRightPanel = Find<RectTransform>("Root/Node_RightPanel");
			_buttonSetting = Find<Button>("Root/Node_RightPanel/Button_Setting");
			ListenButton(_buttonSetting);
			_textTaskPoint = Find<Text>("Root/Node_RightPanel/TaskPoint/Text_TaskPoint");
			_buttonTask = Find<Button>("Root/Node_RightPanel/Button_Task");
			ListenButton(_buttonTask);
			_textTaskModel = Find<Text>("Root/Node_RightPanel/Button_Task/Text_TaskModel");
			_nodeLeftPanel = Find<RectTransform>("Root/Node_LeftPanel");
			_buttonHead = Find<Button>("Root/Node_LeftPanel/Button_Head");
			ListenButton(_buttonHead);
			_imageHead = Find<Image>("Root/Node_LeftPanel/Button_Head/Image_Head");
			_textName = Find<Text>("Root/Node_LeftPanel/Button_Head/Text_Name");
			_buttonIntroduce = Find<Button>("Root/Node_LeftPanel/Button_Introduce");
			ListenButton(_buttonIntroduce);
			_buttonTank = Find<Button>("Root/Node_LeftPanel/Button_Tank");
			ListenButton(_buttonTank);
			_buttonBuilding = Find<Button>("Root/Node_LeftPanel/Button_Building");
			ListenButton(_buttonBuilding);
			_buttonGameIntroduction = Find<Button>("Root/Node_LeftPanel/Button_GameIntroduction");
			ListenButton(_buttonGameIntroduction);
			_buttonMarket = Find<Button>("Root/Node_LeftPanel/Button_Market");
			ListenButton(_buttonMarket);
			_nodeBottom = Find<RectTransform>("Root/Node_Bottom");
			_buttonMatch = Find<Button>("Root/Node_Bottom/Button_Match");
			ListenButton(_buttonMatch);
			_buttonMachine = Find<Button>("Root/Node_Bottom/Button_Machine");
			ListenButton(_buttonMachine);
			
        }
    }    

}

﻿using UnityEngine;
using GameInit.Framework;
using System.Collections.Generic;

namespace GameLogic.HotfixFramework
{
    public class CombatUnitsConfig : BaseConfig
    {
        public override void Deserialize(byte[] bytes)
        {
            _defDic.Clear();
            int byteIndex = 0;
            int defCount = ByteUtil.ReadSignedInt(bytes, ref byteIndex);
            CombatUnitsDef def;
            for (int i = 0; i < defCount; i++)
            {
                def = new CombatUnitsDef();
                def.Parse(bytes, ref byteIndex);
                _defDic.Add(def.ID, def);
            }
        }
        
        /// <summary>
        /// 通过ID获取CombatUnitsDef的实例
        /// </summary>
        /// <param name="ID">索引</param>
        public static CombatUnitsDef Get(int ID)
        {
            _defDic.TryGetValue(ID, out CombatUnitsDef def);
            return def;
        }
        
        /// <summary>
        /// 获取字典
        /// </summary>
        public static Dictionary<int, CombatUnitsDef> GetDefDic()
        {
            return _defDic;
        }

        private static Dictionary<int, CombatUnitsDef> _defDic = new Dictionary<int, CombatUnitsDef>();
    }
    
}

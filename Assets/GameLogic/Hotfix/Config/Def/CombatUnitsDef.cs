﻿using GameInit.Framework;

namespace GameLogic.HotfixFramework
{

    public class CombatUnitsDef
    {
        /// <summary>
        /// 编号
        /// </summary>
        public int ID;

        /// <summary>
        /// 名称
        /// </summary>
        public string Name;

        /// <summary>
        /// 类型
        /// </summary>
        public int UnitType;

        /// <summary>
        /// 子类型
        /// </summary>
        public int UnitTypeSub;

        /// <summary>
        /// 攻击力
        /// </summary>
        public int Attack;

        /// <summary>
        /// 生命值
        /// </summary>
        public int Hp;

        /// <summary>
        /// 移动速度
        /// </summary>
        public float Speed;

        /// <summary>
        /// 视野范围
        /// </summary>
        public float ViewRange;

        /// <summary>
        /// 价格
        /// </summary>
        public int Price;

        /// <summary>
        /// Icon
        /// </summary>
        public string Icon;

    

        public void Parse(byte[] bytes, ref int byteIndex)
		{
			ID = ByteUtil.ReadSignedInt(bytes,ref byteIndex);
			Name = ByteUtil.ReadUTFByte(bytes,ByteUtil.ReadUnsignedShort(bytes,ref byteIndex),ref byteIndex);
			UnitType = ByteUtil.ReadSignedInt(bytes,ref byteIndex);
			UnitTypeSub = ByteUtil.ReadSignedInt(bytes,ref byteIndex);
			Attack = ByteUtil.ReadSignedInt(bytes,ref byteIndex);
			Hp = ByteUtil.ReadSignedInt(bytes,ref byteIndex);
			Speed = ByteUtil.ReadFloat(bytes,ref byteIndex);
			ViewRange = ByteUtil.ReadFloat(bytes,ref byteIndex);
			Price = ByteUtil.ReadSignedInt(bytes,ref byteIndex);
			Icon = ByteUtil.ReadUTFByte(bytes,ByteUtil.ReadUnsignedShort(bytes,ref byteIndex),ref byteIndex);

		}
    }
    
}

﻿using UnityEngine;
using GameInit.Framework;
using System.Collections.Generic;
using GameLogic.HotfixFramework;

namespace GameHotfix.Game
{
    public class Excel01Config : BaseConfig
    {
        public override void Deserialize(byte[] bytes)
        {
            _defDic.Clear();
            int byteIndex = 0;
            int defCount = ByteUtil.ReadSignedInt(bytes, ref byteIndex);
            Excel01Def def;
            for (int i = 0; i < defCount; i++)
            {
                def = new Excel01Def();
                def.Parse(bytes, ref byteIndex);
                _defDic.Add(def.ID, def);
            }
        }
        
        /// <summary>
        /// 通过ID获取Excel01Def的实例
        /// </summary>
        /// <param name="ID">索引</param>
        public static Excel01Def Get(int ID)
        {
            _defDic.TryGetValue(ID, out Excel01Def def);
            return def;
        }
        
        /// <summary>
        /// 获取字典
        /// </summary>
        public static Dictionary<int, Excel01Def> GetDefDic()
        {
            return _defDic;
        }

        private static Dictionary<int, Excel01Def> _defDic = new Dictionary<int, Excel01Def>();
    }
    
}

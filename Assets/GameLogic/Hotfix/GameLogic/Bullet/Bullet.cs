﻿using Cysharp.Threading.Tasks;
using GameHotfix.Game;
using GameInit.Framework;
using UnityEngine;
using UnityEngine.Pool;

namespace GameLogic.Hotfix
{
    public abstract class Bullet : GameUnitBase
    {
        /// <summary>
        /// 类型
        /// </summary>
        public abstract BulletType Type{ get;}

        /// <summary>
        /// 碰撞事件
        /// </summary>
        private BulletMonoBehaviour _behaviour;
        
        /// <summary>
        /// 加载对象
        /// </summary>
        /// <returns></returns>
        protected async UniTask Init(string prefabPath)
        {
            //加载物体对象
            await LoadObj(prefabPath);
            // 碰撞事件
            _behaviour = Obj.GetComponent<BulletMonoBehaviour>();
            if(_behaviour == null)
                _behaviour = Obj.AddComponent<BulletMonoBehaviour>();
            _behaviour.OnTriggerEnterAction = OnTriggerEnter;
        }

        /// <summary>
        /// 初始化
        /// </summary>
        public abstract UniTask Init();

        /// <summary>
        /// 设置父节点
        /// </summary>
        public void SetParent(Transform parent,Vector3 localPos)
        {
            Obj.transform.SetParent(parent);
            Obj.transform.localPosition = localPos;
        }

        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="updateTime"></param>
        public abstract void FixedUpdate(float fixedTime);

        /// <summary>
        /// 碰撞
        /// </summary>
        /// <param name="other"></param>
        public abstract void OnTriggerEnter(Collider other);
    }
}
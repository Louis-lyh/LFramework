﻿using Cysharp.Text;
using Cysharp.Threading.Tasks;
using UnityEngine;
using Logger = GameInit.Framework.Logger;

namespace GameLogic.Hotfix
{
    public class BulletCommon : Bullet
    {
        /// <summary>
        /// 类型
        /// </summary>
        public override BulletType Type { get=>BulletType.Common;}

        /// <summary>
        /// 目标
        /// </summary>
        private Transform _target;

        /// <summary>
        /// 是否移动
        /// </summary>
        private bool _isMove;

        /// <summary>
        /// 速度
        /// </summary>
        private float _speed = 20;
        
        /// <summary>
        /// 初始化
        /// </summary>
        public override async UniTask Init()
        {
            // 移动
            _isMove = false;
            //加载物体对象
            var prefabPath = ZString.Concat("GamePlay/Bullet/", "Bullet_Common");
            await Init(prefabPath);
        }
        
        /// <summary>
        /// 设置
        /// </summary>
        public void Fire(Vector3 startPos,Transform target)
        {
            Obj.transform.position = startPos;
            _target = target;
            _isMove = true;
        }

        /// <summary>
        /// 固定更新
        /// </summary>
        /// <param name="fixedTime"></param>
        public override void FixedUpdate(float fixedTime)
        {
            // 向目标移动
            MoveToTarget(fixedTime);
        }
        
        /// <summary>
        /// 碰撞事件
        /// </summary>
        /// <param name="other"></param>
        public override void OnTriggerEnter(Collider other)
        {
            // 击中目标
            if (other.transform == _target)
            {
                // 移除子弹
                BulletManager.Instance.RemoveBullet(this);   
            }
        }

        /// <summary>
        /// 向目标移动
        /// </summary>
        private void MoveToTarget(float fixedTime)
        {
            if(!_isMove)
                return;
            
            // 移动
            Obj.transform.position = Vector3.MoveTowards(Obj.transform.position, _target.position, _speed * fixedTime);
        }
    }
}
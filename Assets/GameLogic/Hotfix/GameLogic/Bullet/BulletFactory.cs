﻿namespace GameLogic.Hotfix
{
    public static class BulletFactory
    {
        public static Bullet GetBullet(BulletType type)
        {
            switch (type)
            {
                case BulletType.Common:
                    return new BulletCommon();
                default:
                    return new BulletCommon();
            }
        }
    }
}
﻿using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using GameLogic.HotfixFramework;
using UnityEngine;

namespace GameLogic.Hotfix
{
    /// <summary>
    /// 子弹管理器
    /// </summary>
    public class BulletManager : Singleton<BulletManager>
    {
        /// <summary>
        /// 子弹列表
        /// </summary>
        private List<Bullet> _bulletList;

        /// <summary>
        /// 子弹节点
        /// </summary>
        private GameObject _bulletNode;
        
        /// <summary>
        /// 初始化
        /// </summary>
        public void Init()
        {
            _bulletList = new List<Bullet>();
            _bulletNode = GameObject.Find("BulletManager");
        }
        
        /// <summary>
        /// 生成子弹
        /// </summary>
        /// <returns></returns>
        public async UniTask<Bullet> GenerateBullet(BulletType type)
        {
            // 获得子弹
            var bullet = BulletFactory.GetBullet(BulletType.Common);
            // 初始化
            await bullet.Init();
            bullet.SetParent(_bulletNode.transform,Vector3.zero);
            // 加入列表
            _bulletList.Add(bullet);
            return bullet;
        }
        
        /// <summary>
        /// 固定更新
        /// </summary>
        public void FixedUpdate(float fixedTime)
        {
            for (int i = 0; i < _bulletList.Count; i++)
            {
                _bulletList[i].FixedUpdate(fixedTime);       
            }
        }
        
        /// <summary>
        /// 移除子弹
        /// </summary>
        /// <param name="bullet"></param>
        public void RemoveBullet(Bullet bullet)
        {
            if (_bulletList.Contains(bullet))
            {
                _bulletList.Remove(bullet);
                bullet.Destroy();
            }
        }

        /// <summary>
        /// 销毁
        /// </summary>
        public void Destroy()
        {
            for (int i = 0; i < _bulletList.Count; i++)
            {
                _bulletList[i].Destroy();
            }
        }
    }
    
    /// <summary>
    /// 子弹类型
    /// </summary>
    public enum BulletType
    {
        Common,   
    }
}
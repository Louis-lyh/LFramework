﻿using System;
using UnityEngine;

namespace GameHotfix.Game
{
    public class BulletMonoBehaviour : MonoBehaviour
    {
        /// <summary>
        /// 触碰回调
        /// </summary>
        public Action<Collider> OnTriggerEnterAction;
        private void OnTriggerEnter(Collider other)
        {
            OnTriggerEnterAction?.Invoke(other);
        }
    }
}
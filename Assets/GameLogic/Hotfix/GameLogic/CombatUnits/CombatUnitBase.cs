﻿using GameLogic.HotfixFramework;
using UnityEditor;

namespace GameLogic.Hotfix
{
    /// <summary>
    /// 战斗单位基类
    /// </summary>
    public abstract class CombatUnitBase : GameUnitBase
    {
        /// <summary>
        /// 行为树
        /// </summary>
        protected BTAction _behaviorTree;

        /// <summary>
        /// 行为数据
        /// </summary>
        protected CombatUnitBehaviourData _combatUnitBehaviourData;
        
        /// <summary>
        /// 预制体路径
        /// </summary>
        protected abstract string PrefabPath { get; }
        
        /// <summary>
        /// 初始化
        /// </summary>
        public async void Init()
        {
            // 加载预制体
            await LoadObj(PrefabPath); 
        }
        
        /// <summary>
        /// 销毁
        /// </summary>
        protected override void OnDestroy()
        {
            // 销毁行为树
            _behaviorTree.Dispose();
            // 销毁行为数据
            _combatUnitBehaviourData.Dispose();
        }
    }
}
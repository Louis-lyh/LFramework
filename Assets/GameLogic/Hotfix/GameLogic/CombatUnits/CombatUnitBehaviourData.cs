﻿using GameLogic.HotfixFramework;

namespace GameLogic.Hotfix
{
    /// <summary>
    /// 战斗单位行为数据
    /// </summary>
    public class CombatUnitBehaviourData : BTWorkingData
    {
        /// <summary>
        /// 时间
        /// </summary>
        public float DeltaTime;
        /// <summary>
        /// 战斗单位
        /// </summary>
        public CombatUnitBase Self { get; }
        
        public CombatUnitBehaviourData(CombatUnitBase self)
        {
            Self = self;
        }
    }
}
﻿using GameLogic.HotfixFramework;

namespace GameLogic.Hotfix
{
    public enum BuildingType
    {
        None,
        Energy,
        Arsenal,
        Headquarters,
    }
    public enum CampType
    {
        None,
        Red,
        Blue,
    }
    
    /// <summary>
    /// 战斗单位类型
    /// </summary>
    public enum CombatUnitType
    {
        TankMBT = 1,
        
    }

    public class CombatUnitsManager : Singleton<CombatUnitsManager>
    {
        
    }
}
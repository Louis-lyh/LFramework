﻿using System.Collections.Generic;
using System.Linq;
using GameLogic.HotfixFramework;

namespace GameLogic.Hotfix
{
    /// <summary>
    /// 战斗单位数据
    /// </summary>
    public class CombatUnitsData : GameInit.Framework.Singleton<CombatUnitsData>
    {
        /// <summary>
        /// 数据上限
        /// </summary>
        public CombatUnitsDef MaxData => CombatUnitsConfig.Get(1);
        /// <summary>
        /// 初始化
        /// </summary>
        public void Init()
        {

        }
        
        /// <summary>
        /// 获取所有战斗单位数据
        /// </summary>
        /// <returns></returns>
        public List<CombatUnitsDef> GetAllCombatUnitDef()
        {
            var allCombatUnitDefs = CombatUnitsConfig.GetDefDic().Values.ToList();
            
            return allCombatUnitDefs.GetRange(1,allCombatUnitDefs.Count - 1);
        }

    }
}
using System.Collections;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using GameHotfix.Game;
using GameInit.Framework;
using UnityEngine;
using Logger = GameInit.Framework.Logger;

namespace GameLogic.Hotfix
{
    /// <summary>
/// 游戏关卡管理器
/// </summary>
public class GameLevelManager : Singleton<GameLevelManager>
{
    /// <summary>
    /// 进入关卡
    /// </summary>
    public async UniTask EnterLevel()
    {
        // 初始化子弹管理器
        BulletManager.Instance.Init();

        // 激活游戏管理器
        GameManager.Instance.SetActive(true);
        // 激活输入
        InputManager.Instance.SetActive(true);
    }

    /// <summary>
    /// 更新
    /// </summary>
    /// <param name="updateTime"></param>
    public void Update(float updateTime)
    {
    }
        
    /// <summary>
    /// 延迟更新
    /// </summary>
    /// <param name="updateTime"></param>
    public void LateUpdate(float updateTime)
    {
    }
    /// <summary>
    /// 固定帧率更新
    /// </summary>
    public void FixedUpdate(float updateTime)
    {
    }


    /// <summary>
    /// 退出关卡
    /// </summary>
    public void ExitLevel()
    {
        // 关闭游戏管理
        GameManager.Instance.SetActive(false);
        // 关闭输入
        InputManager.Instance.SetActive(false);
    }


}
}



using GameHotfix.Game;
using GameLogic.HotfixFramework;
using UnityEngine;
using Logger = GameInit.Framework.Logger;

namespace GameLogic.Hotfix
{
    public class GameManager : Singleton<GameManager>
    {
        /// <summary>
        /// 游戏主相机
        /// </summary>
        /// <returns></returns>
        public Camera MainCamera { get; private set; }

        //初始化标记
        private bool _isInit;
        //销毁标记
        private bool _isDestroy;
        //激活标志
        private bool _isActive;
        //暂停标志
        private bool _isPause;

        /// <summary>
        /// 初始化
        /// </summary>
        public void Init()
        {
            if (_isInit) return;
            
            _isInit = true;
            _isDestroy = false;
            _isActive = false;
            _isPause = false;
            
            //得到游戏场景中的主相机
            MainCamera = GameObject.FindWithTag("MainCamera").GetComponent<Camera>();
            // log
            Logger.LogGreen("GameManager Init "+MainCamera);
        }
        /// <summary>
        /// 销毁
        /// </summary>
        public void Destroy()
        {
            if (_isDestroy) return;
            
            _isDestroy = true;
            _isInit = false;
        }
        
        /// <summary>
        /// 激活与否
        /// </summary>
        /// <param name="b"></param>
        public void SetActive(bool b)
        {
            _isActive = b;
        }
        
        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="updateTime"></param>
        public void Update(float updateTime)
        {
            //激活
            if (!_isActive) return;
            //暂定
            if (_isPause) return;
            //-------------------游戏逻辑------------------//
            // 游戏关卡
            GameLevelManager.Instance.Update(updateTime);
            // 输入
            InputManager.Instance.Update(updateTime);
        }
        
        /// <summary>
        /// 延迟更新
        /// </summary>
        /// <param name="updateTime"></param>
        public void LateUpdate(float updateTime)
        {
            //激活
            if (!_isActive) return;
            //暂定
            if (_isPause) return;

            //-------------------游戏逻辑------------------//
            // 游戏关卡
            GameLevelManager.Instance.LateUpdate(updateTime);
        }
        /// <summary>
        /// 固定帧率更新
        /// </summary>
        public void FixedUpdate(float updateTime)
        {
            //激活
            if (!_isActive) return;
            //暂定
            if (_isPause) return;

            //-------------------游戏逻辑------------------//
            // 游戏关卡
            GameLevelManager.Instance.FixedUpdate(updateTime);
            // 子弹管理
            BulletManager.Instance.FixedUpdate(updateTime);
        }
        
        //暂停与否
        public void SetPause(bool isPause)
        {
            if (isPause) //暂停
            {
                if (!_isPause)
                {
                    _isPause = true;
                }
            }
            else //恢复
            {
                if (_isPause)
                {
                    _isPause = false;
                }
            }
            // 暂停
            Logger.LogMagenta("SetPause " + isPause);
        }
    }
}



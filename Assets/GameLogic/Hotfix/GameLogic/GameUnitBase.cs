﻿using Cysharp.Threading.Tasks;
using GameInit.Framework;
using UnityEngine;

namespace GameLogic.Hotfix
{
    public abstract class GameUnitBase
    {
        /// <summary>
        /// 物体对象
        /// </summary>
        private GameObject _obj;
        public GameObject Obj=>_obj;
        /// <summary>
        /// id
        /// </summary>
        public int Id { get; protected set;}
        
        /// <summary>
        /// 加载对象
        /// </summary>
        /// <returns></returns>
        protected async UniTask LoadObj(string prefabPath)
        {
            //加载物体对象
            _obj =  await ResourceManager.ConstructObjectAsync(prefabPath);
        }
        
        /// <summary>
        /// 设置父节点
        /// </summary>
        public void SetParent(Transform parent,Vector3 localPos)
        {
            Obj.transform.SetParent(parent);
            Obj.transform.localPosition = localPos;
        }
        
        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="updateTime"></param>
        public virtual void Update(float updateTime) { }
        
        /// <summary>
        /// 固定更新
        /// </summary>
        /// <param name="fixedUpdateTime"></param>
        public virtual void FixedUpdate(float fixedUpdateTime) { }

        /// <summary>
        /// 销毁
        /// </summary>
        public void Destroy()
        {
            // 回收对象
            if (_obj != null)
            {
                ResourceManager.CollectObj(_obj);
                _obj = null;
                OnDestroy(); 
            }
        }
        protected virtual void OnDestroy() {}
    }
}
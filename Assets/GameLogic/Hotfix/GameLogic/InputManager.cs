﻿using GameLogic.HotfixFramework;
using UnityEngine;

namespace GameLogic.Hotfix
{ 
    public class InputManager : Singleton<InputManager>
    {
        //激活标志
        private bool _isActive;
        
        /// <summary>
        /// 是否机会
        /// </summary>
        /// <param name="isActive"></param>
        public void SetActive(bool isActive)
        {
            _isActive = isActive;
        }
        
        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="UpdateTime"></param>
        public void Update(float UpdateTime)
        {
            if(!_isActive)
                return;
            // 点击场景中的游戏对象
            ClickGameObjectInScene();
        }
        
        /// <summary>
        /// 点击场景中的游戏对象
        /// </summary>
        private void ClickGameObjectInScene()
        {
            // 点击
            if (TouchDown())
            {
            }
        }

        #region 手指操作
        /// <summary>
        /// 手指按下
        /// </summary>
        /// <returns></returns>
        private bool TouchDown()
        {
            if (Input.GetMouseButtonDown(0))
                return true;

            if (Input.touchCount == 1 && Input.touches[0].phase == TouchPhase.Began)
                return true;

            return false;
        }
        #endregion
    }
}
﻿using Cysharp.Threading.Tasks;
using GameInit.Framework;
using GameLogic.HotfixFramework;

namespace GameLogic.Hotfix
{
    public class GameFightState : GameState
    {
        public GameStateType StateType { get; }
        public void Init()
        {
        }

        public void Destroy()
        {
        }

        public async UniTask Enter()
        {
            //消息注册
            Global.AddEventListener("Msg_Win", ProcessMsgWin);                     //胜利
            Global.AddEventListener("Msg_Fail", ProcessMsgFail);                    //失败
            Global.AddEventListener("Msg_InterruptGame", ProcessMsgInterruptGame);           //中途退出
            Global.AddEventListener("Msg_ReturnHome", ProcessMsgReturnHome);              //返回主页
            // 关闭主页面
            WindowHomeUI.Instance.Close();
            // 打开战斗界面
            await WindowGame.Instance.Open(UILayer.Bottom);
        }

        public void Exit()
        {
             //消息注销
            Global.RemoveEventListener("Msg_Win", ProcessMsgWin);                       //胜利
            Global.RemoveEventListener("Msg_Fail", ProcessMsgFail);                      //失败
            Global.RemoveEventListener("Msg_InterruptGame", ProcessMsgInterruptGame);             //中途退出
            Global.RemoveEventListener("Msg_ReturnHome", ProcessMsgReturnHome);                //返回主页

            // 打开战斗界面
            WindowGame.Instance.Close();
        }

        public void Update(float fTick)
        {
        }

        public void FixedUpdate(float fTick)
        {
        }

        public void LateUpdate(float fTick)
        {
        }

        #region 处理消息
        //胜利
        private void ProcessMsgWin(BaseEvent msg)
        {
           
        }
        
        //失败
        private void ProcessMsgFail(BaseEvent msg)
        {
        }


        //中途退出
        private void ProcessMsgInterruptGame(BaseEvent msg)
        {
           
        }
        
        //返回主页
        private void ProcessMsgReturnHome(BaseEvent msg)
        {
            //回到主页
            GameStateManager.Instance.ChangeState(GameStateType.Home);
        }
        #endregion
    }
}
﻿using Cysharp.Threading.Tasks;
using GameLogic.HotfixFramework;

namespace GameLogic.Hotfix
{
    public class GameHomeState : GameState
    {
        //类型
        public GameStateType StateType
        {
            get { return GameStateType.Home; }
        }
        public void Init()
        {
        }

        public void Destroy()
        {
        }

        public async UniTask Enter()
        {
            //打开 Home 界面
            await WindowHomeUI.Instance.Open(UILayer.Bottom);
        }

        public void Exit()
        {
        }

        public void Update(float fTick)
        {
        }

        public void FixedUpdate(float fTick)
        {
        }

        public void LateUpdate(float fTick)
        {
        }
    }
}
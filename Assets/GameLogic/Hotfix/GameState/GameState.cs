﻿using Cysharp.Threading.Tasks;

namespace GameLogic.Hotfix
{
    public interface GameState
    {
        GameStateType StateType { get; }
        void Init();
        void Destroy();
        UniTask Enter();
        void Exit();
        void Update(float fTick);
        void FixedUpdate(float fTick);
        void LateUpdate(float fTick);
    }
}
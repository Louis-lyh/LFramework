using System;
using System.Collections;
using System.Collections.Generic;
using GameLogic.HotfixFramework;
using UnityEngine;

namespace GameLogic.Hotfix
{
    /// <summary>
    /// 游戏状态
    /// </summary>
    public enum GameStateType
    {
        Node = 0,
        Home,                         //主页
        Fight,                        //战斗
    }
    
    /// <summary>
    /// 游戏状态管理
    /// </summary>
    public class GameStateManager : Singleton<GameStateManager>
    {
        // 下一个状态
        private GameState _toState;
        // 当前状态
        private GameState _curState;
        // 游戏状态
        private Dictionary<GameStateType, GameState> _gameStateList = new Dictionary<GameStateType, GameState>();
        // 切换完成回调
        private Action _onChangeStateComplete;

        /// <summary>
        /// 初始化
        /// </summary>
        public void Init()
        {
            // 添加游戏状态
            _gameStateList.Add(GameStateType.Home, new GameHomeState());
            _gameStateList.Add(GameStateType.Fight, new GameFightState());
            
            // 初始化
            using (Dictionary<GameStateType, GameState>.Enumerator itr = _gameStateList.GetEnumerator())
            {
                while (itr.MoveNext())
                {
                    itr.Current.Value.Init();
                }
            }
        }
        
        /// <summary>
        /// 销毁
        /// </summary>
        public void Destroy()
        {
            // 销毁
            using (Dictionary<GameStateType, GameState>.Enumerator itr = _gameStateList.GetEnumerator())
            {
                while (itr.MoveNext())
                {
                    itr.Current.Value.Destroy();
                }
            }
            _gameStateList.Clear();
        }
        
        /// <summary>
        /// 修改游戏状态
        /// </summary>
        /// <param name="to"></param>
        /// <param name="onChangeStateComplete">切换完成回调</param>
        public void ChangeState(GameStateType to,Action onChangeStateComplete = null)
        {
            _toState = _gameStateList[to];
            _onChangeStateComplete = onChangeStateComplete;
        }
        
        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="fTick"></param>
        public async void Update(float fTick)
        {
            // 更新游戏状态
            _curState?.Update(fTick);

            if (_toState != _curState)
            {
                // 退出当前游戏状态
                _curState?.Exit();
                
                // 进入下一个游戏状态
                if (_toState != null)
                {
                    await _toState.Enter();
                    _curState = _toState;
                    // 切换完成回调
                    _onChangeStateComplete?.Invoke();
                }
            }
        }
            
        /// <summary>
        /// 固定帧率更新
        /// </summary>
        /// <param name="fTick"></param>
        public void FixedUpdate(float fTick)
        {
            _curState?.FixedUpdate(fTick);
        }

        public void LateUpdate(float fTick)
        {
            _curState?.LateUpdate(fTick);
        }
    }
}



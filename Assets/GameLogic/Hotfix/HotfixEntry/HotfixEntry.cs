using GameInit.Framework;
using GameLogic.HotfixFramework;
using UnityEngine;

namespace GameLogic.Hotfix
{
    /// <summary>
    /// 热更运行脚本
    /// </summary>
    public class HotfixEntry : HotfixBaseEntry
    {
        public override void Awake()
        {
            // ui节点对象池
            UIItemPool.Init();
            // ui管理
            UIManager.Instance.Init();
            // 游戏状态管理器
            GameStateManager.Instance.Init();
            // 加载本地资源初始化
            LocalResLoader.Instance.Init();
            // 音效
            SoundManager.Instance.Init();
        }
        
        public override async void Start()
        {
            // 加载本地资源启动
            LocalResLoader.Instance.Load();
        }
        
      
        public override void Update()
        {
            // 每帧的时间
            var deltaTime = Time.deltaTime;
            // 游戏状态管理器
            GameStateManager.Instance.Update(deltaTime);
            // 定时器
            TimerManager.Instance.Update(deltaTime);
            // 加载本地资源
            LocalResLoader.Instance.Update();
            // 音效
            SoundManager.Instance.Update(deltaTime);
        }
        
        public override void FixedUpdate()
        {
            // 每帧的时间
            var deltaTime = Time.fixedDeltaTime;
            // 游戏状态管理器
            GameStateManager.Instance.FixedUpdate(deltaTime);
        }
    
        public override void LateUpdate()
        {
            // 每帧的时间
            var deltaTime = Time.deltaTime;
            // 游戏状态管理器
            GameStateManager.Instance.LateUpdate(deltaTime);
        }
        
        public override void OnDestroy()
        {
            // 游戏状态管理器
            GameStateManager.Instance.Destroy();
            // 定时器
            TimerManager.Instance.Destroy();
            // ui节点对象池
            UIItemPool.Destory();
            // 音效
            SoundManager.Instance.Destroy();
        }
    }
}
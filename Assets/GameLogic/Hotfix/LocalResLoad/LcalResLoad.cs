﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cysharp.Threading.Tasks;
using GameHotfix.Game;
using GameInit.Framework;
using GameLogic.HotfixFramework;
using Logger = GameInit.Framework.Logger;

namespace GameLogic.Hotfix
{
    public class LocalResLoader : HotfixFramework.Singleton<LocalResLoader>
    {
        //加载状态
        private enum GameLoadingState
        {
            e_gls_null = 1,     //没有启动
            e_gls_loading,      //加载中
            e_gls_complete,     //完成
            e_gls_out,          //退出
        }
        
        /// <summary>
        /// 需要加载的类型
        /// </summary>
        private enum GameLoadingType
        {
            LoadingData, //本地数据
            LoadingGameConfig, //游戏数据
        }
        
        //状态
        private GameLoadingState _state = GameLoadingState.e_gls_null;
        //需要加载的类型
        private Dictionary<GameLoadingType, bool> _gameLoadingTypeDic;

        /// <summary>
        /// 初始化
        /// </summary>
        public void Init()
        {
            //标志=false
            _gameLoadingTypeDic = new Dictionary<GameLoadingType, bool>();
            foreach (GameLoadingType item in Enum.GetValues(typeof(GameLoadingType)))
                _gameLoadingTypeDic.Add(item,false);
        }
        
        /// <summary>
        /// 加载
        /// </summary>
        public async void Load()
        {
            //Log
            Logger.LogGreen("Game Loading... Enter");
            
            //激活加载
            _state = GameLoadingState.e_gls_loading;

            //打开加载界面
            await LoadingView.Instance.Open();
            
            // 加载游戏配置
            await LoadingGameConfig();

            //加载本地数据
            await LoadingData();

            //初始化常量
            //ConstantHelper.Init();

            //加载游戏管理器
            //LoadingGameManager();
        }
        
        /// <summary>
        /// 刷新
        /// </summary>
        public void Update()
        {
            if (_state == GameLoadingState.e_gls_out || _state == GameLoadingState.e_gls_null)
                return;
            
            if (_state == GameLoadingState.e_gls_loading)
            {
                // 加载进度
                var progress = LoadProgress();
                // 设置进度
                LoadingView.Instance.SetLoadingProgress(progress);

                if (progress >= 1 && LoadingView.Instance.IsComplete())
                {
                    //Log
                    Logger.LogGreen("Game Loading... Completed");
                    //标记完成
                    _state = GameLoadingState.e_gls_complete;
                    //切换到游戏界面
                    UnityEngine.SceneManagement.SceneManager.LoadScene("Game");
                }
            }
            else if (_state == GameLoadingState.e_gls_complete)
            {
                //Log
                Logger.LogGreen("Game Loading... Out");
                //标记退出
                _state = GameLoadingState.e_gls_out;

                //切换游戏状态 - Home
                GameStateManager.Instance.ChangeState(GameStateType.Home, () =>
                {
                    //关闭加載界面
                    LoadingView.Instance.Close();
                });
               
            }
        }
        
        /// <summary>
        /// 加载进度
        /// </summary>
        /// <returns></returns>
        public float LoadProgress()
        {
            // 完成个数
            var completeCount = 0;
            var keys = _gameLoadingTypeDic.Keys.ToList();
            // 判断
            for (int i = 0; i < keys.Count; i++)
            {
                if (_gameLoadingTypeDic[keys[i]])
                    completeCount++;
            }

            if (completeCount < keys.Count - 1)
                completeCount++;

            return (float)completeCount / keys.Count;
        }

        #region 游戏配置
        /// <summary>
        /// 添加配置
        /// </summary>
        private void AddConfig()
        {
            //热更配置注册
            ConfigManager.Instance.AddConfig("Excel01", new Excel01Config());
            ConfigManager.Instance.AddConfig("SoundVol", new SoundVolConfig());
            ConfigManager.Instance.AddConfig("CombatUnits", new CombatUnitsConfig());
        }
        /// <summary>
        /// 加载游戏配置
        /// </summary>
        /// <returns></returns>
        private async UniTask LoadingGameConfig()
        {
            // 提示文本
            LoadingView.Instance.SetText("加载游戏配置");
            
            //配置注册
            AddConfig();
            //热更配置读入
            await ConfigManager.Instance.LoadConfig();
            
            //图集读入
            await SpriteAtlasMgr.Instance.ReloadAtlasInfo();
            
            //初始化多语言
            // await LocationManager.Init();

            // 等待一秒
            await UniTask.Delay(1000);
            //标记完成
            _gameLoadingTypeDic[GameLoadingType.LoadingGameConfig] = true;

            //开始音效
            // SoundManager.Instance.PlaySound(16);
        }
        #endregion
        
        #region 本地数据
        private async UniTask LoadingData()
        {
            
            // 提示文本
            LoadingView.Instance.SetText("本地数据初始化");
            
            //本地数据初始化
            //LocalDataManager.Init();
            
            // 等待一秒
            await UniTask.Delay(1000);
            //标记完成
            //标记完成
            _gameLoadingTypeDic[GameLoadingType.LoadingData] = true;
        }
        #endregion
        
        #region 游戏管理器
        private void LoadingGameManager()
        {
            // //创建游戏核心玩法管理器
            // GameObject go = new GameObject();
            // go.name = "GameCoreGameplayMng";
            // go.AddComponent<GameplayManager>();
            // GameObject.DontDestroyOnLoad(go);
            //
            // //标记完成
            // _gameMgrComplete = true;
        }
        #endregion
    }
}

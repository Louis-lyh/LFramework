using Cysharp.Text;
using Cysharp.Threading.Tasks;
using DG.Tweening;
using GameLogic.HotfixFramework;
using UnityEngine;
using UnityEngine.UI;

namespace GameLogic.Hotfix
{
    /// <summary>
    /// 加载界面
    /// </summary>
    public class LoadingView : Singleton<LoadingView>
    {
        // 预制体
        private GameObject _loadingObject;
        // 版本号文本
        private Text _textVersion;
        // 信息内容
        private Text _contentText;
        // 进度条
        private Image _barImage;
        
        // 目标进度
        private float _targetProgress;
        
        //打开加载界面
        public async UniTask Open()
        {
            // 没有创建
            if (_loadingObject == null)
            {
                // 加载预制体
                var pfb = await GameInit.Framework.ResourceManager.LoadAssetAsync<GameObject>(GameInit.Framework.ResPathUtils.GetUIPrefab("uiLoading"));
                var parent = GameObject.Find("TopRoot").transform;
                // 实例化
                _loadingObject = Object.Instantiate(pfb,parent);
                _loadingObject.name = "uiLoading";
                // 初始化缩放位置
                _loadingObject.transform.localPosition = Vector3.zero;
                _loadingObject.transform.localScale = Vector3.one;
                // 初始化
                Init();
            }
            else
            {
                _loadingObject.SetActive(true);
            }
            
            // 进度为零
            _targetProgress = 0;
        }
        
        /// <summary>
        /// 初始化
        /// </summary>
        private void Init()
        {
            // 加载信息
            _contentText = _loadingObject.transform.Find("ProgressBar/Text_Progress").GetComponent<Text>();
            // 进度条
            _barImage = _loadingObject.transform.Find("ProgressBar/Image_Progress").GetComponent<Image>();
            // 版本信息
            _textVersion = _loadingObject.transform.Find("Text_Version").GetComponent<Text>();
            // 显示加载文本
            _contentText.text = "加载中";
            // 进度
            _barImage.fillAmount = 0f;
            // 显示版本信息
            _textVersion.text = ZString.Concat("版本:", Application.version);
        }
        /// <summary>
        /// 关闭
        /// </summary>
        public void Close()
        {
            // 关闭动画
            _barImage.DOKill();
            // 隐藏界面
            _loadingObject.SetActive(false);
        }
        
        /// <summary>
        /// 是否完成加载
        /// </summary>
        /// <returns></returns>
        public bool IsComplete()
        {
            if (_barImage.fillAmount >= 1f)
            {
                _barImage.DOKill();
                return true;
            }
            return false;
        }

        /// <summary>
        /// 显示进度
        /// </summary>
        /// <param name="pro"></param>
        public void SetLoadingProgress(float pro)
        {
            if(_loadingObject == null)
                return;
            
            if (pro != _targetProgress)
            {
                _targetProgress = pro;
                // 动画时间
                var duration = (pro - _barImage.fillAmount) * 2f;
                // 动画
                _barImage.DOKill();
                _barImage.DOFillAmount(_targetProgress,duration);
            }
        }
        
        /// <summary>
        /// 显示信息
        /// </summary>
        /// <param name="text"></param>
        public void SetText(string text)
        {
            _contentText.text = text;
        }
    }
}

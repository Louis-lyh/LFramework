﻿using System;
using System.Collections;
using GameLogic.HotfixFramework;
using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.Networking;

namespace GameLogic.Hotfix
{
    public class Test : MonoBehaviour
    {
        private void Start()
        {
            StartCoroutine(TestRequest()); 
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.A))
            {
                WindowHome.Instance.Open(UILayer.Bottom);
            }

          
        }
        
        // 最小化测试代码示例
        IEnumerator TestBasicRequest()
        {
            string url = "https://ark.cn-beijing.volces.com/api/v3";
            UnityWebRequest request = UnityWebRequest.Get(url);
    
            // 添加基础请求头
            request.SetRequestHeader("Content-Type", "application/json");
    
            yield return request.SendWebRequest();

            if (request.result != UnityWebRequest.Result.Success)
            {
                Debug.Log($"Error: {request.error} Code: {request.responseCode}");
            }
            else
            {
                Debug.Log($"Response: {request.downloadHandler.text}");
            }
        }
        
        private static string apiKey_db = "de1f6422-1f0a-442a-8927-42bbc2761c59"; // 替换为你的API密钥
        private static string apiUrl_db = "https://ark.cn-beijing.volces.com/api/v3/chat/completions"; // API URL
        
        // 最小化测试代码示例
        IEnumerator TestRequest()
        {
            // 创建请求体对象
            var requestBody = new
            {
                model = "ep-20250218113735-s6z94",
                //1. **messages 对话上下文配置**:
                // - 这个字段是一个数组，包含对话的消息历史。每个消息对象有`content`和`role`属性。
                // - `role`可以是`system`、`user`或`assistant`，分别代表系统消息、用户消息和助手消息。
                // - 示例中的系统消息设定了助手的行为，用户消息是用户的输入。
                messages = new[]
                {
                    new { content = "You are a helpful assistant", role = "system" },
                    new { content = "Hi", role = "user" }
                },
            };
            // 使用 Newtonsoft.Json 序列化对象
            string json = JsonConvert.SerializeObject(requestBody);
            // 创建 UnityWebRequest
            // 创建UnityWebRequest对象
            UnityWebRequest request = new UnityWebRequest(apiUrl_db, "POST");

            // 设置请求头
            request.SetRequestHeader("Content-Type", "application/json");
            request.SetRequestHeader("Authorization", "Bearer " + apiKey_db);

            // 设置请求体
            byte[] bodyRaw = System.Text.Encoding.UTF8.GetBytes(json);
            request.uploadHandler = new UploadHandlerRaw(bodyRaw);
            request.downloadHandler = new DownloadHandlerBuffer();
            
            yield return request.SendWebRequest();

            if (request.result != UnityWebRequest.Result.Success)
            {
                Debug.Log($"Error: {request.error} Code: {request.responseCode}");
            }
            else
            {
                Debug.Log($"Response: {request.downloadHandler.text}");
            }
        }
    }
}
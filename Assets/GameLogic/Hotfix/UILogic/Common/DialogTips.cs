﻿namespace GameLogic.Hotfix
{
    public partial class DialogTips
    {
        protected override void OnOpen(params object[] args)
        {
            // 标题
            _textTitle.text = (string) args[0];
            // 内容
            _textContent.text = (string) args[1];
        }
        protected override void OnClick(string name)
        {
            switch (name)
            {
                case "Button_Close":
                    Close();
                    break;
            }
        }
    }
}
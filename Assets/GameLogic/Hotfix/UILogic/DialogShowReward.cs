﻿using GameInit.Framework;

namespace GameLogic.Hotfix
{
    public partial class DialogShowReward
    {
        protected override void ParseComponentExit()
        {
            Logger.LogMagenta("DialogShowReward ParseComponentExit");
            // 关闭
            ListenButton(_buttonClose);
        }

        protected override void RegisterEvent()
        {
            Logger.LogMagenta("DialogShowReward RegisterEvent");
        }

        protected override void UnRegisterEvent()
        {
            Logger.LogMagenta("DialogShowReward UnRegisterEvent");
        }

        protected override void OnOpen(params object[] args)
        {
            Logger.LogMagenta("DialogShowReward OnOpen");
            // title
            _textTitle.text = "Dialog Show Reward";
        }

        protected override void OnShow(params object[] arg)
        {
            Logger.LogMagenta("DialogShowReward OnShow");
            // title
            _textTitle.text = "Dialog Show Reward";
        }

        protected override void OnHide()
        {
            Logger.LogMagenta("DialogShowReward OnHide");
        }

        public override void Dispose()
        {
            Logger.LogMagenta("DialogShowReward Dispose");
            base.Dispose();
        }
        
    }
}
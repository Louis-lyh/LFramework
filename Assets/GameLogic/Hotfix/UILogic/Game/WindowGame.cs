﻿using GameInit.Framework;

namespace GameLogic.Hotfix
{
    public partial class WindowGame
    {
        protected override void OnClick(string name)
        {
            switch (name)
            {
                case "Button_Quit":
                    // 返回主页
                    Global.DispatchEvent("Msg_ReturnHome");
                    break;
            }
        }
    }
}
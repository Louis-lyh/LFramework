﻿using System;
using DG.Tweening;
using GameLogic.HotfixFramework;
using UnityEngine;

namespace GameLogic.Hotfix
{
    public partial class DialogBuildingIntroduce
    {
        protected override void OnClick(string name)
        {
            switch (name)
            {
                case "Button_Close":
                    Close();
                    break;
            }
        }
        protected override void OnDialogIn()
        {
            _nodeLeft.anchoredPosition = new Vector2(-150,0);
            _nodeLeft.DOAnchorPosX(150,0.5f);

            var nodeHeadquartersPos = _nodeHeadquarters.localPosition;
            nodeHeadquartersPos.y = UIManager.Instance.Top + 255;
            _nodeHeadquarters.localPosition = nodeHeadquartersPos;
            _nodeHeadquarters.DOLocalMoveY(270,0.5f);
            
            var nodeEnergyPos = _nodeEnergy.localPosition;
            nodeEnergyPos.x = UIManager.Instance.Right + 255;
            _nodeEnergy.localPosition = nodeEnergyPos;
            _nodeEnergy.DOLocalMoveX(450, 0.5f);
            
            var nodeArsenalAPos = _nodeArsenalA.localPosition;
            nodeArsenalAPos.y = UIManager.Instance.Bottom - 255;
            _nodeArsenalA.localPosition = nodeArsenalAPos;
            _nodeArsenalA.DOLocalMoveY(-270,0.5f);
            
            var nodeArsenalBPos = _nodeArsenalB.localPosition;
            nodeArsenalBPos.x = UIManager.Instance.Right + 255;
            _nodeArsenalB.localPosition = nodeArsenalBPos;
            _nodeArsenalB.DOLocalMoveX(450, 0.5f).OnComplete(() =>DialogInComplete());;
        }

        protected override void OnDialogOut(Action callBack)
        {
            _nodeLeft.DOAnchorPosX(-150,0.5f);
            
            _nodeHeadquarters.DOLocalMoveY(UIManager.Instance.Top+255, 0.5f);
            _nodeEnergy.DOLocalMoveX(UIManager.Instance.Right+255, 0.5f);
            _nodeArsenalA.DOLocalMoveY(UIManager.Instance.Bottom - 255, 0.5f);
            _nodeArsenalB.DOLocalMoveX(UIManager.Instance.Right + 255, 0.5f).OnComplete(()=> callBack?.Invoke());
        }
    }
}
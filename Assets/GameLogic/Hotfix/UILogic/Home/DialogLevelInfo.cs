﻿using System.Collections.Generic;

namespace GameLogic.Hotfix
{
    public partial class DialogLevelInfo
    {
        // 关卡信息
        private List<ItemLevelInfo> _itemLevelInfos;
        protected override async void OnOpen(params object[] args)
        {
            _itemLevelInfos = new List<ItemLevelInfo>();
            // 创建三个关卡信息
            for (int i = 0; i < 3; i++)
            {
                var item = new ItemLevelInfo();
                await item.Create(this, _nodeEasyLevelInfo, "ItemLevelInfo");
                item.Show();
                _itemLevelInfos.Add(item);
            }
        }

        protected override void OnClose()
        {
        }

        protected override void OnClick(string name)
        {
            switch (name)
            {
                case "Button_Close":
                    Close();
                    break;
            }
        }

        public override void Dispose()
        {
            for (int i = 0; i < _itemLevelInfos.Count; i++)
            {
                _itemLevelInfos[i].Dispose();   
            }
            base.Dispose();
        }
    }
}
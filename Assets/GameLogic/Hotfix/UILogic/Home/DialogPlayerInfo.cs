﻿using GameInit.Framework;

namespace GameLogic.Hotfix
{
    public partial class DialogPlayerInfo
    {
        protected override void OnInputEndEdit(string name, string value)
        {
            Logger.LogGreen($"OnInputEndEdit: name {name} , value {value}");
        }

        protected override void OnInputValueChanged(string name, string value)
        {
            Logger.LogGreen($"OnInputValueChanged: name {name} , value {value}");
        }

        protected override void OnClick(string name)
        {
            switch (name)
            {
                case "Button_Close":
                    Close();
                    break;
            }
        }
    }
}
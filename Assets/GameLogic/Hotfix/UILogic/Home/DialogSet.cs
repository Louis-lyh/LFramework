﻿using GameLogic.HotfixFramework;

namespace GameLogic.Hotfix
{
    public partial class DialogSet
    {
        // 初始化
        private void Init()
        {
            // 注册音乐按钮点击
            _toggleMusic.onValueChanged.AddListener((isOn) =>
            {
                // 音乐按钮
                SoundManager.Instance.IsOpenMusic = isOn;
                var musicSpherePos = _nodeMusicSphere.localPosition;
                musicSpherePos.x = SoundManager.Instance.IsOpenMusic ?  46 : -46;
                _nodeMusicSphere.localPosition = musicSpherePos;
            });
            
            // 注册音效按钮点击
            _toggleSound.onValueChanged.AddListener((isOn) =>
            {
                // 音效按钮
                SoundManager.Instance.IsOpenSound = isOn;
                var soundSpherePos = _nodeSoundSphere.localPosition;
                soundSpherePos.x = SoundManager.Instance.IsOpenSound ?  46 : -46;
                _nodeSoundSphere.localPosition = soundSpherePos;
            });
            
        }

        protected override void OnOpen(params object[] args)
        {
            // 初始化
            Init();
            // 更新界面
            UpdateView();
        }

        protected override void OnClose()
        {
            
        }
        
        /// <summary>
        /// 更新UI
        /// </summary>
        private void UpdateView()
        {
            // 音乐按钮
            _toggleMusic.isOn = SoundManager.Instance.IsOpenMusic;
            var musicSpherePos = _nodeMusicSphere.localPosition;
            musicSpherePos.x = SoundManager.Instance.IsOpenMusic ?  46 : -46;
            _nodeMusicSphere.localPosition = musicSpherePos;
            // 音效按钮
            _toggleSound.isOn = SoundManager.Instance.IsOpenSound;
            var soundSpherePos = _nodeSoundSphere.localPosition;
            soundSpherePos.x = SoundManager.Instance.IsOpenSound ?  46 : -46;
            _nodeSoundSphere.localPosition = soundSpherePos;
        }

        protected override void OnClick(string name)
        {
            switch (name)
            {
                case "Button_Close":
                    Close();
                    break;
            }
        }

        public override void Dispose()
        {
            // 注销音乐按钮点击
            _toggleMusic.onValueChanged.RemoveAllListeners();
            // 注销音效按钮点击
            _toggleSound.onValueChanged.RemoveAllListeners();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using DG.Tweening;
using GameInit.Framework;
using GameLogic.HotfixFramework;
using UnityEngine;

namespace GameLogic.Hotfix
{
    public partial class DialogTankIntroduce
    {
        // 战斗单位配置
        private List<CombatUnitsDef> _combatUnitsDefs;
        // item
        private List<ItemTankIntroduce> _itemTankIntroduces;
        
        protected override async void OnOpen(params object[] args)
        {
            // 战斗单位配置
            _combatUnitsDefs = CombatUnitsData.Instance.GetAllCombatUnitDef();
            // 创建item
            await CreateItemTankIntroduce();
            // 显示信息
            ShowInfo(0);
        }
        
        /// <summary>
        /// 显示信息
        /// </summary>
        public void ShowInfo(int index)
        {
            // 最大配置
            var maxDef = CombatUnitsData.Instance.MaxData;
            // 当前配置
            var curDef = _combatUnitsDefs[index];
            // 名字
            _textTankName.text = curDef.Name;
            // icon
            _imageTank.sprite = SpriteAtlasMgr.Instance.LoadSprite(curDef.Icon);
            
            // 生命
            TweenUtil.ToValue(0, curDef.Hp / (float)maxDef.Hp, 0.5f, x => _sliderHp.value = x);
            // 战斗力
            TweenUtil.ToValue(0, curDef.Attack / (float)maxDef.Attack, 0.5f, x => _sliderAttack.value = x);
            // 速度
            TweenUtil.ToValue(0, curDef.Speed / (float)maxDef.Speed, 0.5f, x => _sliderSpeed.value = x);
            // 视野
            TweenUtil.ToValue(0, curDef.ViewRange / (float)maxDef.ViewRange, 0.5f, x => _sliderView.value = x);
            
            // 刷新item
            for (int i = 0; i < _itemTankIntroduces.Count; i++)
            {
                _itemTankIntroduces[i].ShowSelect(i == index);
            }
            
        }

        /// <summary>
        /// 创建战斗单位item
        /// </summary>
        private async UniTask CreateItemTankIntroduce()
        {
            // 初始化
            _itemTankIntroduces = new List<ItemTankIntroduce>();

            for (int i = 0; i < _combatUnitsDefs.Count; i++)
            {
                var item = new ItemTankIntroduce();
                await item.Create(this, _nodeBottom, "ItemTankIntroduce_" + 1);
                item.Show(_combatUnitsDefs[i],i);
                _itemTankIntroduces.Add(item);
            }
        }


        protected override void OnClose()
        {
            
        }
        
        protected override void OnClick(string name)
        {
            switch (name)
            {
                case "Button_Close":
                    Close();
                    break;
            }
        }

        protected override void OnDialogIn()
        {
            _nodeLeft.anchoredPosition = new Vector2(-150,0);
            _nodeLeft.DOAnchorPosX(150,0.5f);
            
            _nodeBottom.anchoredPosition = new Vector2(300.001f,-270);
            _nodeBottom.DOAnchorPosY(270, 0.5f).OnComplete(() =>DialogInComplete());;;

            //base.OnDialogIn();
        }

        protected override void OnDialogOut(Action callBack)
        {
            _nodeLeft.DOAnchorPosX(-150,0.3f);
            _nodeBottom.DOAnchorPosY(-270, 0.3f).OnComplete(()=> callBack?.Invoke());;
            
            //base.OnDialogOut(callBack);
        }

        public override void Dispose()
        {
            for(int i = 0; i < _itemTankIntroduces.Count; i++)
                _itemTankIntroduces[i].Dispose();
            _itemTankIntroduces.Clear();
            
            base.Dispose();
        }
    }
}
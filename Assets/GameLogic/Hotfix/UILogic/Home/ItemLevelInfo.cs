﻿using UnityEngine;

namespace GameLogic.Hotfix
{
    public partial class ItemLevelInfo
    {
        protected override void OnClick(string name) 
        {
            switch (name)
            {
                case "Button_Start":
                    //进入战斗
                    GameStateManager.Instance.ChangeState(GameStateType.Fight, () =>
                    {
                        // 关闭界面
                        (_parentView as DialogLevelInfo).Close();
                    });
                    break;
            }
        }
    }
}
﻿using GameInit.Framework;
using GameLogic.HotfixFramework;

namespace GameLogic.Hotfix
{
    public partial class ItemTankIntroduce
    {
        // 配置
        private CombatUnitsDef _def;
        // 序号
        private int _index;
        protected override void OnShow(params object[] arg)
        {
            // 配置
            _def = (CombatUnitsDef)arg[0];
            // 序号
            _index = (int) arg[1];
            // icon
            _imageIcon.sprite = SpriteAtlasMgr.Instance.LoadSprite(_def.Icon);
        }
        
        /// <summary>
        /// 显示选择
        /// </summary>
        /// <param name="isShow"></param>
        public void ShowSelect(bool isShow)
        {
            _nodeSelect.gameObject.SetActive(isShow);
        }

        protected override void OnClick(string name)
        {
            switch (name)
            {
                case "Button_Click":
                    (_parentView as DialogTankIntroduce)?.ShowInfo(_index);
                    break;
            }
        }
    }
}
﻿using DG.Tweening;
using GameLogic.HotfixFramework;
using UnityEngine;

namespace GameLogic.Hotfix
{
    public partial class WindowHomeUI
    {
        protected override void ParseComponentExit()
        {
            
        }

        protected override void RegisterEvent()
        {
            
        }

        protected override void UnRegisterEvent()
        {
            
        }

        protected override void OnOpen(params object[] args)
        {
            _textTaskPoint.text = "7758758";
        }

        protected override void OnClose()
        {
            
        }

        protected override void OpenAnimation()
        {
            // 左边界面宽
            var leftPanelWidth = _nodeLeftPanel.rect.size.x;
            _nodeLeftPanel.anchoredPosition = new Vector2(-leftPanelWidth,0);
            _nodeLeftPanel.DOAnchorPosX(leftPanelWidth / 2, 0.5f);
            // 右边界面宽
            var rightPanelWidth = _nodeRightPanel.rect.size.x;
            _nodeRightPanel.anchoredPosition = new Vector2(rightPanelWidth,0);
            _nodeRightPanel.DOAnchorPosX(-rightPanelWidth / 2, 0.5f);
            // 下边界面高
            var bottomPanelHeight = _nodeBottom.rect.size.y;
            var bottomPanelAnchorPos = _nodeBottom.anchoredPosition;
            _nodeBottom.anchoredPosition = new Vector2(bottomPanelAnchorPos.x,-bottomPanelHeight);
            _nodeBottom.DOAnchorPosY(bottomPanelHeight / 2, 0.5f);
        }

        protected override void OnClick(string name)
        {
            switch (name)
            {
                case "Button_Setting":
                    DialogManager.Instance.Open(new DialogSet(), UILayer.Top);
                    break;
                case "Button_Introduce":
                    DialogManager.Instance.Open(new DialogTips(), UILayer.Top, "项目介绍", UIConstant.GameIntroduce);
                    break;
                case "Button_GameIntroduction":
                    DialogManager.Instance.Open(new DialogTips(), UILayer.Top, "游戏介绍", UIConstant.GameIntroduction);
                    break;
                case "Button_Tank":
                    DialogManager.Instance.Open(new DialogTankIntroduce(), UILayer.Middle);
                    return;
                case "Button_Building":
                    DialogManager.Instance.Open(new DialogBuildingIntroduce(), UILayer.Middle);
                    return;
                case "Button_Machine":
                    DialogManager.Instance.Open(new DialogLevelInfo(), UILayer.Middle);
                    return;
                case "Button_Head":
                    DialogManager.Instance.Open(new DialogPlayerInfo(), UILayer.Middle);
                    return;
            }
        }

        public override void Dispose()
        {
            base.Dispose();
        }
    }
}
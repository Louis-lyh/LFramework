﻿using GameInit.Framework;

namespace GameLogic.Hotfix
{
    public partial class ListViewItemList
    {
        protected override void ParseComponentExit()
        {
            Logger.LogMagenta("ListViewItemList ParseComponentExit");
        }

        protected override void RegisterEvent()
        {
            Logger.LogMagenta("ListViewItemList RegisterEvent");
        }

        protected override void UnRegisterEvent()
        {
            Logger.LogMagenta("ListViewItemList UnRegisterEvent");
        }

        protected override void OnShow(params object[] arg)
        {
            Logger.LogMagenta("ListViewItemList OnShow");
            _textName.text = "ListViewItemList " + Index;
        }

        protected override void OnHide()
        {
            Logger.LogMagenta("ListViewItemList OnHide");
        }

        public override void Dispose()
        {
            Logger.LogMagenta("ListViewItemList Dispose");
            base.Dispose();
        }
    }
}
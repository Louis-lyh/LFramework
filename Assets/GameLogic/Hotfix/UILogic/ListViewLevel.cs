﻿using GameLogic.HotfixFramework;
using UnityEngine;
using Logger = GameInit.Framework.Logger;

namespace GameLogic.Hotfix
{
    public partial class ListViewLevel
    {
        protected override void ParseComponentExit()
        {
            Logger.LogGreen("ListViewLevel ParseComponentExit");
        }

        protected override void RegisterEvent()
        {
            Logger.LogGreen("ListViewLevel RegisterEvent");
        }

        protected override void UnRegisterEvent()
        {
            Logger.LogGreen("ListViewLevel UnRegisterEvent");
        }

        protected override void OnShow(params object[] arg)
        {
            // name
            _textName.text = "ListView";
            // 设置列表间隔
            SetSpace(Vector2.up * 5,0,0,0,0);
            // 初始化列表
            InitListView(20,_listViewItemList,ListViewDir.vertical);
        }

        protected override void OnHide()
        {
            Logger.LogGreen("ListViewLevel OnHide");
        }

        protected override void OnRefreshListItem(ListViewItem listItem)
        {
            // 显示item
            listItem.Show();
        }

        public override void Dispose()
        {
            base.Dispose();
        }
    }
}
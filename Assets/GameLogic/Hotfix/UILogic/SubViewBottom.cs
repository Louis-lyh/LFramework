﻿using GameInit.Framework;

namespace GameLogic.Hotfix
{
    public partial class SubViewBottom
    {
        protected override void ParseComponentExit()
        {
            Logger.LogMagenta("SubViewBottom ParseComponentExit");
        }

        protected override void RegisterEvent()
        {
            Logger.LogMagenta("SubViewBottom RegisterEvent");
        }

        protected override void UnRegisterEvent()
        {
            Logger.LogMagenta("SubViewBottom UnRegisterEvent");
        }

        protected override void OnShow(params object[] arg)
        {
            Logger.LogMagenta("SubViewBottom OnShow");
            // title
            _textName.text = " subView";
        }

        protected override void OnHide()
        {
            Logger.LogMagenta("SubViewBottom OnHide");
        }

        public override void Dispose()
        {
            Logger.LogMagenta("SubViewBottom Dispose");
            base.Dispose();
        }
    }
}
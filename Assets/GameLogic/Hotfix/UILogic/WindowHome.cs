﻿using GameHotfix.Game;
using GameLogic.HotfixFramework;
using UnityEngine;
using Logger = GameInit.Framework.Logger;

namespace GameLogic.Hotfix
{
    public partial class WindowHome
    {
        protected override void RegisterEvent()
        {
            Logger.LogBlue("WindowHome RegisterEvent");
        }

        protected override void UnRegisterEvent()
        {
            Logger.LogBlue("WindowHome UnRegisterEvent");
        }

        protected override void ParseComponentExit()
        {
            Logger.LogBlue("WindowHome ParseComponentExit");
        }

        protected override void OnOpen(params object[] args)
        {
            // title
            _textTitle.text = ""+Excel01Config.Get(1).Name;
            // 显示循环列表
            _listViewLevel.Show();
            // 显示子界面
            _subViewBottom.Show();
        }

        protected override void OnShow(params object[] arg)
        {
            Logger.LogBlue("WindowHome OnShow");
        }

        protected override void OnClose()
        {
            Logger.LogBlue("WindowHome OnClose");
        }

        protected override void OnHide()
        {
            Logger.LogBlue("WindowHome OnClose");
        }

        protected  override async void OnClick(string name)
        {
            switch (name)
            {
                case "Button_Close":
                    Close();
                    break;
                case "Button_Start":
                    await DialogManager.Instance.Open(new DialogShowReward(), UILayer.Middle);
                    break;
            }
        }

        public override void Dispose()
        {
            // 显示循环列表
            _listViewLevel.Dispose();
            // 显示子界面
            _subViewBottom.Dispose();
            Logger.LogBlue("WindowHome Dispose");
            base.Dispose();
        }
    }
}
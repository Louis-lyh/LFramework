﻿

using UnityEngine;

namespace GameLogic.Hotfix
{
    public static class GameHelpUtil
    {
        /// <summary>
        /// 随机打乱顺序 
        /// </summary>
        public static T[] ShuffleCoords<T>(T[] _dataArray)
        {
            for (int i = 0; i < _dataArray.Length; i++)
            {
                int randomNum = Random.Range(i, _dataArray.Length);
                T Tmp = _dataArray[randomNum];
                _dataArray[randomNum] = _dataArray[i];
                _dataArray[i] = Tmp;
            }
            return _dataArray;
        }
    }
}
﻿namespace GameLogic.HotfixFramework
{
    /// <summary>
    /// 行为数据
    /// </summary>
    public class BTActionData
    {
    }
    /// <summary>
    /// 行为
    /// </summary>
    public abstract class BTAction : BTTreeNode
    {
        /// <summary>
        /// 前置条件
        /// </summary>
        protected BTPrecondition _precondition;
        
        public BTAction(int maxChildCount) : base(maxChildCount)
        {
        }
        /// <summary>
        /// 销毁
        /// </summary>
        public override void Dispose()
        {
            _precondition = null;
            base.Dispose();
        }
        
        /// <summary>
        /// 评估,该行为是否执行
        /// </summary>
        /// <returns></returns>
        public bool Evaluate(BTWorkingData wData)
        {
            return (_precondition == null || _precondition.IsTrue(wData)) && OnEvaluate(wData);
        }
        /// <summary>
        /// 评估-执行条件是否满足
        /// </summary>
        /// <param name="wData"></param>
        /// <returns></returns>
        protected virtual bool OnEvaluate(BTWorkingData wData)
        {
            return true;
        }
        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="wDate"></param>
        /// <returns></returns>
        public virtual int Update(BTWorkingData wDate)
        {
            return BTRunningStatus.Finished;
        }
        /// <summary>
        /// 结束过度
        /// </summary>
        /// <param name="wData"></param>
        public virtual void Transition(BTWorkingData wData)
        {
        }
        /// <summary>
        /// 设置前置条件
        /// </summary>
        /// <param name="precondition"></param>
        /// <returns></returns>
        public BTAction SetPrecondition(BTPrecondition precondition)
        {
            _precondition = precondition;
            return this;
        }
        /// <summary>
        /// 获得运行数据
        /// </summary>
        /// <param name="wData"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        protected T GetActionData<T>(BTWorkingData wData) where T : BTActionData, new()
        {
            // 获得对象hash code
            int uniqueKey = GetHashCode();
            T thisData;
            // 没有数据-添加
            if (!wData.ActionDatas.ContainsKey(uniqueKey))
            {
                thisData = new T();
                wData.ActionDatas.Add(uniqueKey, thisData);
            }
            thisData = (T)wData.ActionDatas[uniqueKey];
            
            return thisData;
        }
    }
}
﻿namespace GameLogic.HotfixFramework
{
    /// <summary>
    /// 叶子节点（每片叶子记录一种策略，执行具体的行为，如移动、攻击、播放动画等行为逻辑。）
    /// </summary>
    public class BTActionLeaf : BTAction
    {
        /// <summary>
        /// 准备
        /// </summary>
        private const int ActionReady = 0;
        /// <summary>
        /// 运行
        /// </summary>
        private const int ActionRunning = 1;
        /// <summary>
        /// 完成
        /// </summary>
        private const int ActionFinished = 2;
        
        /// <summary>
        /// 运行数据
        /// </summary>
        class BTActionLeafData : BTActionData
        {
            // 状态
            internal int  Status;
            // 是否需要退出
            internal bool NeedExit;

            private object _userData;
            public T GetUserData<T>() where T : class, new()
            {
                if (_userData == null) {
                    _userData = new T();
                }
                return (T)_userData;
            }
            public BTActionLeafData()
            {
                Status = ActionReady;
                NeedExit = false;
                _userData = null;
            }
        }
        
        public BTActionLeaf()
            : base(0)
        {
        }
        
        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="wData"></param>
        /// <returns></returns>
        public sealed override int Update(BTWorkingData wData)
        {
            // 运行状态
            var runningState = BTRunningStatus.Finished;
            // 运行数据
            var thisActionData = GetActionData<BTActionLeafData>(wData);
            
            // 准备状态
            if (thisActionData.Status == ActionReady) 
            {
                // 进入行为节点
                OnEnter(wData);
                // 需要退出
                thisActionData.NeedExit = true;
                // 运行状态
                thisActionData.Status = ActionRunning;
            }
            // 运行状态
            if (thisActionData.Status == ActionRunning) 
            {
                // 运行
                runningState = OnExecute(wData); 
                // 判断是否完成
                if (BTRunningStatus.IsFinished(runningState))    
                {
                    thisActionData.Status = ActionFinished; // 完成
                }
            }
            // 完成状态
            if (thisActionData.Status == ActionFinished)
            {
                // 判断是否需要退出处理
                if (thisActionData.NeedExit) 
                {
                    OnExit(wData, runningState); // 退出
                }
                thisActionData.Status = ActionReady;
                thisActionData.NeedExit = false;
            }
            
            return runningState;
        }
        
        /// <summary>
        /// 进入节点
        /// </summary>
        /// <param name="wData"></param>
        protected virtual void OnEnter(BTWorkingData wData)
        {
        }
        
        /// <summary>
        /// 运行节点
        /// </summary>
        /// <param name="wData"></param>
        /// <returns></returns>
        protected virtual int OnExecute(BTWorkingData wData)
        {
            return BTRunningStatus.Finished;
        }
        
        /// <summary>
        /// 退出
        /// </summary>
        /// <param name="wData"></param>
        /// <param name="runningStatus"></param>
        protected virtual void OnExit(BTWorkingData wData, int runningStatus)
        {

        }
    }
}
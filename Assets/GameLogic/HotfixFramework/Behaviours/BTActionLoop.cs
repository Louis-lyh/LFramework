﻿namespace GameLogic.HotfixFramework
{
    /// <summary>
    /// 控制节点 - 循环节点
    /// </summary>
    public class BTActionLoop : BTAction
    {
        protected class TBTActionLoopContext : BTActionData
        {
            /// <summary>
            /// 当前循环次数
            /// </summary>
            internal int CurrentCount;

            public TBTActionLoopContext()
            {
                CurrentCount = 0;
            }
        }
        
        /// <summary>
        /// 无穷大
        /// </summary>
        public const int Infinity = -1;

        /// <summary>
        /// 循环次数
        /// </summary>
        private int _loopCount;
        
        public BTActionLoop()
            : base(1)
        {
            _loopCount = Infinity;
        }
        /// <summary>
        /// 设置循环次数
        /// </summary>
        /// <param name="count">次数</param>
        /// <returns></returns>
        public BTActionLoop SetLoopCount(int count)
        {
            _loopCount = count;
            return this;
        }
        /// <summary>
        /// 评估-执行条件是否满足
        /// </summary>
        /// <param name="wData"></param>
        /// <returns></returns>
        protected override bool OnEvaluate(BTWorkingData wData)
        {
            // 获取运行数据
            var thisContext = GetActionData<TBTActionLoopContext>(wData);
            // 检查循环还可以循环
            var checkLoopCount = (_loopCount == Infinity || thisContext.CurrentCount < _loopCount);
            if (!checkLoopCount || !IsIndexValid(0)) 
            {
                return false;
            }
            // 评估子节点是否满足执行条件
            var node = GetChild<BTAction>(0);
            return node.Evaluate(wData);
        }
        
        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="wData"></param>
        /// <returns></returns>
        public override int Update(BTWorkingData wData)
        {
            // 运行数据
            var thisActionData = GetActionData<TBTActionLoopContext>(wData);
            var runningStatus = BTRunningStatus.Finished;
            // 没有子节点直接完成
            if (!IsIndexValid(0))
                return runningStatus;
            
            // 更新子节点
            var node = GetChild<BTAction>(0);
            runningStatus = node.Update(wData);
            
            // 完成
            if (!BTRunningStatus.IsFinished(runningStatus))
                return runningStatus;
            // 运行次数加一
            thisActionData.CurrentCount++;
            
            // 次数没有用完继续运行
            if (thisActionData.CurrentCount < _loopCount || _loopCount == Infinity)
                runningStatus = BTRunningStatus.Executing;

            return runningStatus;
        }
        /// <summary>
        /// 结束-过度
        /// </summary>
        /// <param name="wData"></param>
        public override void Transition(BTWorkingData wData)
        {
            // 运行数据
            var thisActionData = GetActionData<TBTActionLoopContext>(wData);
            // 调用子节点过度
            if (IsIndexValid(0)) 
            {
                BTAction node = GetChild<BTAction>(0);
                node.Transition(wData);
            }
            
            thisActionData.CurrentCount = 0;
        }
    }
}
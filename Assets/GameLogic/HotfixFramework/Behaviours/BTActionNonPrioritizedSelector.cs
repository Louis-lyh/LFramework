﻿namespace GameLogic.HotfixFramework
{
    /// <summary>
    /// 控制节点-非优先选择
    /// </summary>
    public class BTActionNonPrioritizedSelector : BTActionPrioritizedSelector
    {
        public BTActionNonPrioritizedSelector()
            : base()
        {
        }
        
        protected override bool OnEvaluate(BTWorkingData wData)
        {
            var thisContext = GetActionData<BTPrioritizedSelectorData>(wData);
            // 没有运行的节点 按顺序选择一个
            if (!IsIndexValid(thisContext.CurrentSelectedIndex))
                return base.OnEvaluate(wData);
            
            // 由在运行的节点 继续运行当前节点
            var node = GetChild<BTAction>(thisContext.CurrentSelectedIndex);
            return node.Evaluate(wData) || base.OnEvaluate(wData);
        }
    }
}
﻿using System.Collections.Generic;

namespace GameLogic.HotfixFramework
{
    /// <summary>
    /// 控制节点-并行运行
    /// </summary>
    public class BTActionParallel : BTAction
    {
        /// <summary>
        /// 子节点关系
        /// </summary>
        public enum ChildrenRelationship
        {
            And,
            Or,
        }
        /// <summary>
        /// 运行数据
        /// </summary>
        protected class BTActionParallelData : BTActionData
        {
            /// <summary>
            /// 子节点评估状态
            /// </summary>
            internal List<bool> EvaluationStatus;
            /// <summary>
            /// 子节点运行状态
            /// </summary>
            internal List<int> RunningStatus;

            public BTActionParallelData()
            {
                EvaluationStatus = new List<bool>();
                RunningStatus = new List<int>();
            }
        }
        
        /// <summary>
        /// 评估关系
        /// </summary>
        private ChildrenRelationship _evaluationRelationship;
        /// <summary>
        /// 运行关系
        /// </summary>
        private ChildrenRelationship _runningStatusRelationship;
        
        public BTActionParallel()
            : base(-1)
        {
            _evaluationRelationship = ChildrenRelationship.And;
            _runningStatusRelationship = ChildrenRelationship.Or;
        }
        /// <summary>
        /// 设置子节点评估关系
        /// </summary>
        /// <param name="v"></param>
        /// <returns></returns>
        public BTActionParallel SetEvaluationRelationship(ChildrenRelationship v)
        {
            _evaluationRelationship = v;
            return this;
        }
        /// <summary>
        /// 设置子节点运行关系
        /// </summary>
        /// <param name="v"></param>
        /// <returns></returns>
        public BTActionParallel SetRunningStatusRelationship(ChildrenRelationship v)
        {
            _runningStatusRelationship = v;
            return this;
        }
        
        /// <summary>
        /// 评估
        /// </summary>
        /// <param name="wData"></param>
        /// <returns></returns>
        protected override bool OnEvaluate(BTWorkingData wData)
        {
            // 运行数据
            BTActionParallelData thisActionData = GetActionData<BTActionParallelData>(wData);
            // 初始化
            InitListTo(thisActionData.EvaluationStatus, false);
            
            // 评估结果
            var finalResult = false;
            // 遍历子节点
            for (int i = 0; i < ChildrenCount; ++i)
            {
                var node = GetChild<BTAction>(i);
                var ret = node.Evaluate(wData);
                
                // 子节点评估状态关系为 与 ，有一个失败直接跳过.
                if (_evaluationRelationship == ChildrenRelationship.And && ret == false) 
                {
                    finalResult = false;
                    break;
                }
                // 子节点评估关系为 或 ，有一个成功结果为true.
                else if(_evaluationRelationship == ChildrenRelationship.Or && ret == true)
                    finalResult = true;
                
                // 记录节点评估状态
                thisActionData.EvaluationStatus[i] = ret;
            }
            
            return finalResult;
        }
        
        /// <summary>
        /// 初始化
        /// </summary>
        /// <param name="list"></param>
        /// <param name="value"></param>
        /// <typeparam name="T"></typeparam>
        private void InitListTo<T>(List<T> list, T value)
        {
            if (list.Count != ChildrenCount)
            {
                list.Clear();
                for (var i = 0; i < ChildrenCount; ++i)
                    list.Add(value);
            } 
            else
            {
                for (var i = 0; i < ChildrenCount; ++i)
                    list[i] = value;
            }
        }
        
        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="wData"></param>
        /// <returns></returns>
        public override int Update(BTWorkingData wData)
        {
            // 运行数据
            var thisActionData = GetActionData<BTActionParallelData>(wData);
            
            // 数据不对称-初始化
            if (thisActionData.RunningStatus.Count != ChildrenCount)
                InitListTo<int>(thisActionData.RunningStatus, BTRunningStatus.Executing);

            var hasFinished  = false;
            var hasExecuting = false;
            // 遍历子节点
            for (var i = 0; i < ChildrenCount; ++i)
            {
                // 评估状态false 跳过
                if (thisActionData.EvaluationStatus[i] == false)
                    continue;
                
                // 完成跳过
                if (BTRunningStatus.IsFinished(thisActionData.RunningStatus[i])) 
                {
                    hasFinished = true;
                    continue;
                }
                
                // 更新子节点
                var node = GetChild<BTAction>(i);
                var runningStatus = node.Update(wData);
                
                // 完成
                if (BTRunningStatus.IsFinished(runningStatus))
                    hasFinished  = true;
                // 运行中
                else
                    hasExecuting = true;
                
                // 记录运行状态
                thisActionData.RunningStatus[i] = runningStatus;
            }
            
            // 子节点运行关系 或 有一个完成代表完成，子节点运行关系 与 都完成代表完成
            if (_runningStatusRelationship == ChildrenRelationship.Or && hasFinished 
                || _runningStatusRelationship == ChildrenRelationship.And && hasExecuting == false) 
            {
                //clear 运行状态
                InitListTo<int>(thisActionData.RunningStatus, BTRunningStatus.Executing);
                return BTRunningStatus.Finished;
            }
            
            return BTRunningStatus.Executing;
        }
        
        /// <summary>
        /// 变化
        /// </summary>
        /// <param name="wData"></param>
        public override void Transition(BTWorkingData wData)
        {
            // 运行数据
            var thisActionData = GetActionData<BTActionParallelData>(wData);
            for (var i = 0; i < ChildrenCount; ++i) 
            {
                var node = GetChild<BTAction>(i);
                node.Transition(wData);
            }
            
            //clear 运行状态
            InitListTo<int>(thisActionData.RunningStatus, BTRunningStatus.Executing);
        }
        
    }
}
﻿namespace GameLogic.HotfixFramework
{
    /// <summary>
    /// 控制节点 - 节点优先选择器
    /// </summary>
    public class BTActionPrioritizedSelector : BTAction
    {
        /// <summary>
        /// 运行数据
        /// </summary>
        protected class BTPrioritizedSelectorData : BTActionData
        {
            /// <summary>
            /// 当前选择节点
            /// </summary>
            internal int CurrentSelectedIndex;
            /// <summary>
            /// 上一个选择节点
            /// </summary>
            internal int LastSelectedIndex;

            public BTPrioritizedSelectorData()
            {
                CurrentSelectedIndex = -1;
                LastSelectedIndex = -1;
            }
        }
        
        public BTActionPrioritizedSelector() : base(-1)
        {
        }
        
        /// <summary>
        /// 评估-执行条件是否满足
        /// </summary>
        /// <param name="wData"></param>
        /// <returns></returns>
        protected override bool OnEvaluate(BTWorkingData wData)
        {
            // 运行数据
            var thisData = GetActionData<BTPrioritizedSelectorData>(wData);
            // 重置当前选择节点
            thisData.CurrentSelectedIndex = -1;
            // 获取孩子节点数量
            for(var i = 0; i < ChildrenCount; i++) 
            {
                var node = GetChild<BTAction>(i);
                // 判断是否满足条件
                if (node.Evaluate(wData))
                {
                    // 选择当前节点
                    thisData.CurrentSelectedIndex = i;
                    return true;
                }
            }
            
            return false;
        }
        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="wData"></param>
        /// <returns></returns>
        public override int Update(BTWorkingData wData)
        {
            // 运行数据
            var thisData = GetActionData<BTPrioritizedSelectorData>(wData);
            // 初始化运行状态
            var runningState = BTRunningStatus.Finished;
            
            // 当前行为节点不是上一次行为节点
            if (thisData.CurrentSelectedIndex != thisData.LastSelectedIndex) 
            {
                // 判断上一次节点是否有效
                if (IsIndexValid(thisData.LastSelectedIndex))
                {
                    var node = GetChild<BTAction>(thisData.LastSelectedIndex);
                    // 上一节点进入结束-过度阶段
                    node.Transition(wData);
                }
                // 更新
                thisData.LastSelectedIndex = thisData.CurrentSelectedIndex;
            }
            
            // 运行当前节点
            if (IsIndexValid(thisData.CurrentSelectedIndex)) 
            {
                // 当前节点
                var node = GetChild<BTAction>(thisData.CurrentSelectedIndex);
                // 运行
                runningState = node.Update(wData);
                // 当前节点运行完成-重置
                if (BTRunningStatus.IsFinished(runningState))
                {
                    thisData.CurrentSelectedIndex = -1;
                    thisData.LastSelectedIndex = -1;
                }
            }
            
            return runningState;
        }
        /// <summary>
        /// 结束-过度
        /// </summary>
        /// <param name="wData"></param>
        public override void Transition(BTWorkingData wData)
        {
            // 运行数据
            var thisData = GetActionData<BTPrioritizedSelectorData>(wData);
            // 拿到当前节点
            var node = GetChild<BTAction>(thisData.LastSelectedIndex);
            // 结束
            node?.Transition(wData);   
            // 重置
            thisData.LastSelectedIndex = -1;
        }
    }
}
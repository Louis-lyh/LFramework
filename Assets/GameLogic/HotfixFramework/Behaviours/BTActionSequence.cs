﻿namespace GameLogic.HotfixFramework
{
    /// <summary>
    /// 控制节点-顺序执行
    /// </summary>
    public class BTActionSequence : BTAction
    {
        protected class BTActionSequenceData : BTActionData
        {
            /// <summary>
            /// 当前选择节点
            /// </summary>
            internal int CurrentSelectedIndex;
            
            public BTActionSequenceData()
            {
                CurrentSelectedIndex = -1;
            }
        }

        public BTActionSequence() : base(-1)
        {
        }
        
        /// <summary>
        /// 判断-评估 节点可以执行
        /// </summary>
        /// <param name="wData"></param>
        /// <returns></returns>
        protected override bool OnEvaluate(BTWorkingData wData)
        {
            // 获取当前状态
            var thisActionData = GetActionData<BTActionSequenceData>(wData);
            // 重置节点序号
            var checkedNodeIndex = -1;
            // 判断节点是否合法
            if (IsIndexValid(thisActionData.CurrentSelectedIndex))
                checkedNodeIndex = thisActionData.CurrentSelectedIndex; // 选择当前
            else
                checkedNodeIndex = 0; // 选择第一个

            // 判断节点是否合法
            if (IsIndexValid(checkedNodeIndex)) 
            {
                // 拿到节点
                var node = GetChild<BTAction>(checkedNodeIndex);
                if (node.Evaluate(wData))
                {
                    thisActionData.CurrentSelectedIndex = checkedNodeIndex;
                    return true;
                }
            }
            return false;
        }
        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="wData"></param>
        /// <returns></returns>
        public override int Update(BTWorkingData wData)
        {
            // 状态
            var thisActionData = GetActionData<BTActionSequenceData>(wData);
            // 重置运行状态
            var runningStatus = BTRunningStatus.Finished;
            // 获取节点
            var node = GetChild<BTAction>(thisActionData.CurrentSelectedIndex);
            // 运行节点
            runningStatus = node.Update(wData);
            // 判断运行节点是否错误
            if (BTRunningStatus.IsError(runningStatus)) 
            {
                thisActionData.CurrentSelectedIndex = -1; // 错误重置序号退出
                return runningStatus;
            }
            // 判断是否完成
            if (BTRunningStatus.IsFinished(runningStatus))
            {
                // 完成节点序号加一
                thisActionData.CurrentSelectedIndex++;
                // 判断序号是否有效
                if (IsIndexValid(thisActionData.CurrentSelectedIndex))
                {
                    runningStatus = BTRunningStatus.Executing; // 执行
                } 
                else 
                {
                    // 重置序号
                    thisActionData.CurrentSelectedIndex = -1;
                }
            }
            return runningStatus;
        }
        /// <summary>
        /// 结束-过度
        /// </summary>
        /// <param name="wData"></param>
        public override void Transition(BTWorkingData wData)
        {
            var thisActionData = GetActionData<BTActionSequenceData>(wData);
            var node = GetChild<BTAction>(thisActionData.CurrentSelectedIndex);
            node?.Transition(wData);
            thisActionData.CurrentSelectedIndex = -1;
        }
    }
}
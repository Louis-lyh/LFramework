﻿namespace GameLogic.HotfixFramework
{
    public static class BTRunningStatus
    {
        /// <summary>
        /// 运行
        /// </summary>
        public const int Executing   = 0;
        /// <summary>
        /// 完成
        /// </summary>
        public const int Finished    = 1;
        /// <summary>
        /// 过度
        /// </summary>
        public const int Transition  = 2;
        
        /// <summary>
        /// 是否完成
        /// </summary>
        /// <param name="runningStatus"></param>
        /// <returns></returns>
        private static bool IsOk(int runningStatus)
        {
            return runningStatus == Finished;
        }
        /// <summary>
        /// 状态是否错误
        /// </summary>
        /// <param name="runningStatus"></param>
        /// <returns></returns>
        public static bool IsError(int runningStatus)
        {
            return runningStatus < 0;
        }
        /// <summary>
        /// 判断是否完成
        /// </summary>
        /// <param name="runningStatus"></param>
        /// <returns></returns>
        public static bool IsFinished(int runningStatus)
        {
            return IsOk(runningStatus) || IsError(runningStatus);
        }
        /// <summary>
        /// 是否运行中
        /// </summary>
        /// <param name="runningStatus"></param>
        /// <returns></returns>
        public static bool IsExecuting(int runningStatus)
        {
            return !IsFinished(runningStatus);
        }
    }
}
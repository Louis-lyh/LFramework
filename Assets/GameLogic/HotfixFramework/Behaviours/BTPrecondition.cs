﻿namespace GameLogic.HotfixFramework
{
    /// <summary>
    /// 条件节点-前置节点
    /// </summary>
    public abstract class BTPrecondition:BTTreeNode
    {
        public BTPrecondition(int maxChildCount) : base(maxChildCount)
        {
        }

        public abstract bool IsTrue(BTWorkingData wData);

    }
    /// <summary>
    /// 前置条件叶节点
    /// </summary>
    public abstract class BTPreconditionLeaf : BTPrecondition
    {
        public BTPreconditionLeaf()
            : base(0)
        {
        }
    }
    
    /// <summary>
    /// 一元前置条件
    /// </summary>
    public abstract class BTPreconditionUnary : BTPrecondition
    {
        public BTPreconditionUnary(BTPrecondition lhs)
            : base(1)
        {
            AddChild(lhs);
        }
    }
    /// <summary>
    /// 二元前置条件
    /// </summary>
    public abstract class BTPreconditionBinary : BTPrecondition
    {
        public BTPreconditionBinary(BTPrecondition lhs, BTPrecondition rhs)
            : base(2)
        {
            AddChild(lhs).AddChild(rhs);
        }
    }
    
    /// <summary>
    /// 前置条件-true
    /// </summary>
    public class BTPreconditionTrue : BTPreconditionLeaf
    {
        public override bool IsTrue(BTWorkingData wData)
        {
            return true;
        }
    }
    /// <summary>
    /// 前置条件-false
    /// </summary>
    public class BTPreconditionFalse : BTPreconditionLeaf
    {
        public override bool IsTrue(BTWorkingData wData)
        {
            return false;
        }
    }
    
    /// <summary>
    /// 前置条件取反
    /// </summary>
    public class BTPreconditionNot : BTPreconditionUnary
    {
        public BTPreconditionNot(BTPrecondition lhs)
            : base(lhs)
        {}
        public override bool IsTrue(BTWorkingData wData)
        {
            return !GetChild<BTPrecondition>(0).IsTrue(wData);
        }
    }
    /// <summary>
    /// 二元前置条件-与
    /// </summary>
    public class BTPreconditionAnd : BTPreconditionBinary
    {
        public BTPreconditionAnd(BTPrecondition lhs, BTPrecondition rhs)
            : base(lhs, rhs)
        { }
        public override bool IsTrue(BTWorkingData wData)
        {
            return GetChild<BTPrecondition>(0).IsTrue(wData) &&
                   GetChild<BTPrecondition>(1).IsTrue(wData);
        }
    }
    /// <summary>
    /// 二元前置条件-或
    /// </summary>
    public class BTPreconditionOr : BTPreconditionBinary
    {
        public BTPreconditionOr(BTPrecondition lhs, BTPrecondition rhs)
            : base(lhs, rhs)
        { }
        public override bool IsTrue(BTWorkingData wData)
        {
            return GetChild<BTPrecondition>(0).IsTrue(wData) ||
                   GetChild<BTPrecondition>(1).IsTrue(wData);
        }
    }
    /// <summary>
    /// 二元前置条件-异或
    /// </summary>
    public class BTPreconditionXor : BTPreconditionBinary
    {
        public BTPreconditionXor(BTPrecondition lhs, BTPrecondition rhs)
            : base(lhs, rhs)
        { }
        public override bool IsTrue(BTWorkingData wData)
        {
            return GetChild<BTPrecondition>(0).IsTrue(wData) ^
                   GetChild<BTPrecondition>(1).IsTrue(wData);
        }
    }
}


using System.Collections.Generic;
using GameInit.Framework;

namespace GameLogic.HotfixFramework
{
    /// <summary>
    /// 行为树节点
    /// </summary>
    public class BTTreeNode
    {
        /// <summary>
        /// 默认了子节点数量
        /// </summary>
        private const int DefaultChildCount = -1;
        /// <summary>
        /// 子节点
        /// </summary>
        private List<BTTreeNode> _children;
        
        public int ChildrenCount => _children.Count;
        /// <summary>
        /// 最大子节点数量
        /// </summary>
        private readonly int _maxChildCount;
        
        public BTTreeNode(int maxChildCount = -1)
        {
            // 初始化
            _children = new List<BTTreeNode>();
            // 设置容量
            if (maxChildCount >= 0)
                _children.Capacity = maxChildCount;
            _maxChildCount = maxChildCount;
        }
        
        public BTTreeNode() : this(DefaultChildCount) { }


        /// <summary>
        /// 添加子节点
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        public BTTreeNode AddChild(BTTreeNode node)
        {
            if (_maxChildCount >= 0 && _children.Count >= _maxChildCount)
            {
                Logger.LogWarning("BTTreeNode.AddChild() => 超过子节点上限");
                return this;
            }
            _children.Add(node);
            return this;
        }
        /// <summary>
        /// 索引是否有效
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public bool IsIndexValid(int index)
        {
            return index >= 0 && index < _children.Count;
        }
        /// <summary>
        /// 获取子节点
        /// </summary>
        /// <param name="index">索引</param>
        /// <typeparam name="T">类型</typeparam>
        /// <returns></returns>
        public T GetChild<T>(int index) where T : BTTreeNode 
        {
            if (index < 0 || index >= _children.Count) {
                return null;
            }
            return (T)_children[index];
        }
        
        /// <summary>
        /// 销毁
        /// </summary>
        public virtual void Dispose()
        {
            _children = null;
        }
    }
}



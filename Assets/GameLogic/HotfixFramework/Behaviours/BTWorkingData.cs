﻿using System.Collections.Generic;

namespace GameLogic.HotfixFramework
{
    /// <summary>
    /// 行为树工作数据
    /// </summary>
    public class BTWorkingData
    {
        // 行为数据
        private Dictionary<int, BTActionData> _actionDatas;
        public Dictionary<int, BTActionData> ActionDatas => _actionDatas;

        public BTWorkingData()
        {
            _actionDatas = new Dictionary<int, BTActionData>();
        }

        public T As<T>() where T : BTWorkingData
        {
            return (T) this;
        }

        public virtual void Dispose()
        {
            _actionDatas?.Clear();
            _actionDatas = null;
        }
    }
}
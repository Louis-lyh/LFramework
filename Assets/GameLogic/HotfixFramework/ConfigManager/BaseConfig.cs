﻿namespace GameLogic.HotfixFramework
{
    public abstract class BaseConfig
    {
        public abstract void Deserialize(byte[] bytes);
    }
}
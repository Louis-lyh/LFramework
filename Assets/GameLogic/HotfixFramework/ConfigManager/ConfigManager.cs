﻿using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using GameInit.Framework;
using UnityEngine;
using Logger = GameInit.Framework.Logger;

namespace GameLogic.HotfixFramework
{
    public class ConfigManager : Singleton<ConfigManager>
    {
        private readonly Dictionary<string, BaseConfig> _allConfigs = new Dictionary<string, BaseConfig>();

        public void AddConfig(string cfgName, BaseConfig config)
        {
            if (_allConfigs.ContainsKey(cfgName))
            {
                Logger.LogMagenta("[ConfigMgr.AddConfig() => 重复添加配置表，cfgName:" + cfgName + "]");
                return;
            }

            _allConfigs[cfgName] = config;
        }

        public async UniTask LoadConfig()
        {
            foreach (var kv in _allConfigs)
            {
                var configText = await ResourceManager.LoadAssetAsync<TextAsset>(ResPathUtils.GetConfigPath(kv.Key));
                kv.Value.Deserialize(configText?.bytes);
            }
        }
    }
}
﻿using System;
using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace GameLogic.HotfixFramework
{
    public static class UIExtensions
    {
        /// <summary>
        /// 按钮点击事件
        /// </summary>
        /// <param name="self"></param>
        /// <param name="action"></param>
        /// <param name="openAni"></param>
        public static void Click(this Button self, Action action,bool openAni = true)
        {
            self.onClick.AddListener(() =>
            {
                action?.Invoke();
                // 音效
                SoundManager.Instance.PlaySound(1);
                // 动画
                self.transform.DOScale(0.9f, 0.1f).OnComplete(() =>
                {
                    self.transform.DOScale(1f, 0.2f);
                });
            });
        }

        /// <summary>
        /// 将十六进制颜色转成 Color
        /// </summary>
        /// <param name="hexColor">带#号的16进制颜色</param>
        /// <returns></returns>
        private static Color ColorFromHex(string hexColor)
        {
            ColorUtility.TryParseHtmlString(hexColor, out var color);
            return color;
        }
    }
}
﻿namespace GameLogic.HotfixFramework
{
    /// <summary>
    /// 销毁接口
    /// </summary>
    public interface IDispose
    {
        void Dispose();
    }
}
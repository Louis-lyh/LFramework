﻿namespace GameLogic.HotfixFramework
{
    /// <summary>
    /// 单例基类
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class Singleton<T> where T : new()
    {
        private static T _instnce;

        public static T Instance
        {
            get
            {
                if(_instnce == null)
                    _instnce = new T();
                return _instnce;
            }
        }

    }
}
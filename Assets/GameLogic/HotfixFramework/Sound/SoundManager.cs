using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Logger = GameInit.Framework.Logger;

namespace GameLogic.HotfixFramework
{
    /// <summary>
    /// 音乐播放管理器
    /// </summary>
    public class SoundManager : Singleton<SoundManager>
    {
        /// <summary>
        /// 音效队列
        /// </summary>
        private Queue<SoundPlayer> _playerPool;
        /// <summary>
        /// 音效对象池节点
        /// </summary>
        private Transform _soundPoolRoot;
        /// <summary>
        /// 音效运行节点
        /// </summary>
        private Transform _soundRunRoot;
        /// <summary>
        /// 正在运行的音效
        /// </summary>
        private Dictionary<long, SoundPlayer> _dictRunningPlayer;
        /// <summary>
        /// 音效同时播放上限
        /// </summary>
        private Dictionary<string, int> _dictQueueLimit;
        /// <summary>
        /// 音效播放器递增id
        /// </summary>
        private long _soundPlayerId = 0;
        /// <summary>
        /// 背景音乐播放器id
        /// </summary>
        private long _bgmSoundPlayerId = 0;
        /// <summary>
        /// 背景音乐配置id
        /// </summary>
        private int _bgmConfigId = 0;
    
        public bool IsOpenSound = true; // 开启音效
        public bool IsOpenMusic = true; // 开启音乐
        
        /// <summary>
        /// 初始化
        /// </summary>
        public void Init()
        {
            var soundRoot = GameObject.Find("SoundRoot");
            // 音效对象池节点
            _soundPoolRoot = soundRoot.transform.Find("SoundPoolRoot");
            // 音效运行节点
            _soundRunRoot = soundRoot.transform.Find("SoundRunRoot");
    
            _playerPool = new Queue<SoundPlayer>();
            _dictRunningPlayer = new Dictionary<long, SoundPlayer>();
            _dictQueueLimit = new Dictionary<string, int>();
        }
        
        /// <summary>
        /// 获取现有声音播放器
        /// </summary>
        /// <param name="soundPlayerId">id</param>
        /// <returns></returns>
        public SoundPlayer GetExistentSoundPlayer(long soundPlayerId)
        {
            _dictRunningPlayer.TryGetValue(soundPlayerId, out var soundPlayer);
            return soundPlayer;
        }
        
        /// <summary>
        /// 播放音效
        /// </summary>
        /// <param name="id">音效id</param>
        /// <param name="isLoop">是否循环</param>
        /// <returns></returns>
        public long PlaySound(int id, bool isLoop = false)
        {
            // 关闭音效退出
            if (IsOpenSound == false)
                return -1;
            
            // 读取音效配置
            SoundVolDef def = SoundVolConfig.Get(id);
            if (def == null)
            {
                Logger.LogMagenta("[SoundManager.PlaySound() => 无效的音频id:" + id);
                return -1;
            }
        
            string name = def.SoundName;
            // 音效播放数量达到上限退出
            if (def.QueueLimit > 0 && _dictQueueLimit.ContainsKey(name) && _dictQueueLimit[name] >= def.QueueLimit)
                return -1;

            return _PlaySound(name, def.Volume, def.DelayTime, def.QueueLimit, isLoop, def);
        }
        /// <summary>
        /// 播放音效
        /// </summary>
        /// <param name="name">音效名</param>
        /// <param name="vol">音量</param>
        /// <param name="delay">延迟</param>
        /// <param name="queueLimit">队列上限</param>
        /// <param name="isLoop">是否循环</param>
        /// <param name="def">配置</param>
        /// <returns></returns>
        private long _PlaySound(string name, float vol, float delay, int queueLimit, bool isLoop = false, SoundVolDef def = null)
        {
            // 获得音效播发器
            var player = GetSoundPlayer();
            
            if (_dictRunningPlayer.ContainsKey(player.SoundPlayerId))
            {
                Logger.LogMagenta("[SoundManager.PlaySound() => 重复添加音频播放器,id:" + player.SoundPlayerId + "]");
                return player.SoundPlayerId;
            }
            
            // 播放音乐
            player.PlaySound(name, isLoop, vol, delay, def);
            // 加入运行列表
            _dictRunningPlayer.Add(player.SoundPlayerId, player);
    
            // 记录该音效运行数量
            if (queueLimit > 0)
            {
                if (_dictQueueLimit.ContainsKey(name)) _dictQueueLimit[name] += 1;
                else _dictQueueLimit.Add(name, 1);
            }
    
            return player.SoundPlayerId;
        }
        
       
        /// <summary>
        /// 获得音效播放器
        /// </summary>
        /// <returns></returns>
        private SoundPlayer GetSoundPlayer()
        {
            SoundPlayer player;
            // 从池子获取
            if (_playerPool.Count > 0)
            {
                player = _playerPool.Dequeue();
            }
            // 新建音效播放器
            else
            {
                var soundObject = new GameObject();
                var result = soundObject.AddComponent<AudioSource>();
                player = new SoundPlayer(result);
            }
            
            // id递增
            _soundPlayerId++;
            // 初始化
            player.SoundPlayerId = _soundPlayerId;
            player.Transform.SetParent(_soundRunRoot, false);
            return player;
        }
       
        
        private int _index;
        private KeyValuePair<long, SoundPlayer> _keyValuePair;
        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="deltaTime"></param>
        public void Update(float deltaTime)
        {
            if (_dictRunningPlayer == null)
                return;
            // 更新音乐播放器逻辑
            for (_index = 0; _index < _dictRunningPlayer.Count; _index++)
            {
                _keyValuePair = _dictRunningPlayer.ElementAt(_index);
                _keyValuePair.Value.Update(deltaTime);
            }
        }
        
        /// <summary>
        /// 暂停所有音乐
        /// </summary>
        public void StopAllSound()
        {
            if (_dictRunningPlayer == null)
                return;
            
            foreach (var kv in _dictRunningPlayer)
            {
                kv.Value.StopSound();
                _playerPool.Enqueue(kv.Value);
                kv.Value.Transform.SetParent(_soundPoolRoot, false);
            }
    
            _dictRunningPlayer.Clear();
            _dictQueueLimit.Clear();
            
            _bgmSoundPlayerId = 0;
            _bgmConfigId = 0;
        }
        
        /// <summary>
        /// 暂停音乐
        /// </summary>
        /// <param name="playerId">1</param>
        public void StopSound(long playerId)
        {
            if (!_dictRunningPlayer.TryGetValue(playerId, out var player))
            {
                Logger.LogError("SoundManager.StopSound() => 移出SoundPlayer失败，id:" + playerId);
                return;
            }
            // 从运行列表中移除
            _dictRunningPlayer.Remove(playerId);
            
            // 运行数量减一
            string name = player.SoundName;
            if (_dictQueueLimit.ContainsKey(name))
            {
                int num = _dictQueueLimit[name];
                num -= 1;
                if (num <= 0) _dictQueueLimit.Remove(name);
                else _dictQueueLimit[name] = num;
            }
            
            // 停止音乐
            player.StopSound();
            // 加入对象池
            _playerPool.Enqueue(player);
            player.Transform.SetParent(_soundPoolRoot, false);
        }
    
   
        /// <summary>
        /// 播放背景音乐
        /// </summary>
        /// <param name="id"></param>
        /// <param name="isLoop"></param>
        public void PlayBackGroundSound(int id, bool isLoop = true)
        {
            if(id > 0)
                _bgmConfigId = id;
    
            if (IsOpenMusic == false)
                return;
    
            SoundVolDef def = SoundVolConfig.Get(id);
            if (def == null)
            {
                Logger.LogMagenta("[SoundManager.PlaySound() => 无效的音频id:" + id);
                return;
            }
            
            // 存在背景音乐，暂停旧的
            if (_bgmSoundPlayerId > 0) 
                StopBgm();
            
            // 播放背景音乐
            _bgmSoundPlayerId = _PlaySound(def.SoundName, def.Volume, def.DelayTime, def.QueueLimit, isLoop);
            // 记录配置id
            _bgmConfigId = id;
        }
        
        /// <summary>
        /// 回复背景音乐
        /// </summary>
        public void RecoveryBgm()
        {
            if (_bgmConfigId > 0) PlayBackGroundSound(_bgmConfigId);
        }
        
        /// <summary>
        /// 停止播放背景音乐
        /// </summary>
        public void StopBgm()
        {
            StopSound(_bgmSoundPlayerId);
            _bgmSoundPlayerId = 0;
        }
        /// <summary>
        /// 清除背景音乐
        /// </summary>
        public void ClearBgm()
        {
            StopSound(_bgmSoundPlayerId);
            _bgmSoundPlayerId = 0;
            _bgmConfigId = 0;
        }
        
        /// <summary>
        /// 销毁
        /// </summary>
        public void Destroy()
        {
            if (_dictRunningPlayer != null)
            {
                foreach (var kv in _dictRunningPlayer)
                {
                    kv.Value.Dispose();
                }
    
                _dictRunningPlayer.Clear();
            }
    
            if (_playerPool == null)
                return;
            while (_playerPool.Count > 0)
            {
                var player = _playerPool.Dequeue();
                player.Dispose();
            }
        }
    }
}
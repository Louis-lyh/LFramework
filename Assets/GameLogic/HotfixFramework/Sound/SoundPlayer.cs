using GameInit.Framework;
using UnityEngine;
using Logger = GameInit.Framework.Logger;

namespace GameLogic.HotfixFramework
{
    /// <summary>
    /// 音乐播放器
    /// </summary>
    public class SoundPlayer : UpdateBase
    {
        /// <summary>
        /// 音乐播发器组件
        /// </summary>
        private AudioSource _source;
        /// <summary>
        /// 是否循环
        /// </summary>
        private bool _isLoop;
        /// <summary>
        /// 节点
        /// </summary>
        public Transform Transform { get; }
        /// <summary>
        /// 音乐id
        /// </summary>
        public long SoundPlayerId;
        /// <summary>
        /// 音乐名
        /// </summary>
        public string SoundName { get; private set; }
        /// <summary>
        /// 延迟定时器
        /// </summary>
        private Timer _delayTimerKey;
        
        public SoundPlayer(AudioSource source)
        {
            _source = source;
            Transform = _source.transform;
        }
        
        /// <summary>
        /// 播放音乐
        /// </summary>
        /// <param name="name">文件名</param>
        /// <param name="blLoop">是否循环</param>
        /// <param name="vol">音量</param>
        /// <param name="delayTime">延迟时间</param>
        /// <param name="def">配置</param>
        public async void PlaySound(string name, bool blLoop, float vol = 1f, float delayTime = 0, SoundVolDef def = null)
        {
            SoundName = name;
            _isLoop = blLoop;
            _source.gameObject.name = "s_" + name;
            // 加载音效文件
            var clip = await ResourceManager.LoadAssetAsync<AudioClip>(ResPathUtils.GetSoundPath(name));
            // 游戏对象在场景中是否处于活动状态
            if (!_source.gameObject.activeInHierarchy)
                return;
            
            if (clip == null)
                Logger.LogError("SoundPlayer.PlaySound() => 加载音频文件失败，name:" + name);
            // 初始化音效组件
            _source.clip = clip;
            _source.loop = blLoop;
            _source.volume = vol;

            // 延迟播放
            if (delayTime > 0)
            {
                StartDelay(delayTime);
                return;
            }
            // 初始化
            OnInitialize();
            // 播放音效
            _source.Play();
        }
        /// <summary>
        /// 启动延迟
        /// </summary>
        /// <param name="delay"></param>
        private void StartDelay(float delay)
        {
            // 移除旧的延迟
            RemoveDelayTimer();
            // 新建延迟
            _delayTimerKey = TimerManager.Instance.AddTimer(delay, false, 0,(data) =>
            {
                // 移除
                RemoveDelayTimer();
                // 初始化
                OnInitialize();
                // 播放音效
                _source.Play();
            });
        }
        /// <summary>
        /// 移除延迟
        /// </summary>
        private void RemoveDelayTimer()
        {
            if (_delayTimerKey == null)
                return;
            TimerManager.Instance.DelTimer(_delayTimerKey);
            _delayTimerKey = null;
        }
        
        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="deltaTime"></param>
        protected override void OnUpdate(float deltaTime)
        {
            if (!_isInitialed || _isLoop)
                return;
            // 循环播放
            if (!_source.isPlaying)
                SoundManager.Instance.StopSound(SoundPlayerId);
        }
        
        /// <summary>
        /// 暂停音效
        /// </summary>
        public void StopSound()
        {
            // 移除定时器
            RemoveDelayTimer();
            // 停止播放
            _source.Stop();
            _source.clip = null;
            _source.loop = false;
            _isLoop = false;
            // 停止更新
            _isInitialed = false;
        }
        /// <summary>
        /// 设置音量
        /// </summary>
        /// <param name="value"></param>
        public void SetVolume(float value)
        {
            if (_source != null)
                _source.volume = value;
        }
        /// <summary>
        /// 暂停
        /// </summary>
        public void Pause()
        {
            if (_source == null) return;
            _source.Pause();
        }
        /// <summary>
        /// 恢复
        /// </summary>
        public void UnPause()
        {
            if (_source == null) return;
            _source.UnPause();
        }
        
        /// <summary>
        /// 销毁
        /// </summary>
        protected override void OnDispose()
        {
            _source = null;
            RemoveDelayTimer();
            base.OnDispose();
        }
    }
}
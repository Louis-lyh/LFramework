﻿using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using UnityEngine;

namespace GameLogic.HotfixFramework
{
    /// <summary>
    /// UI 弹窗管理器
    /// </summary>
    public class DialogManager: Singleton<DialogManager>
    {
        /// <summary>
        /// 弹窗堆栈
        /// </summary>
        private  readonly Stack<UIDialogView> StackDialogs = new Stack<UIDialogView>();
        /// <summary>
        /// 显示的弹窗
        /// </summary>
        private  UIDialogView _stackShowView;
        
        /// <summary>
        /// 弹窗队列
        /// </summary>
        private readonly Queue<UIDialogView> QueueDialogs = new Queue<UIDialogView>();
        /// <summary>
        /// 弹窗数据
        /// </summary>
        private readonly Dictionary<UIDialogView,object[]> _dialogDatas = new Dictionary<UIDialogView, object[]>();
        /// <summary>
        /// 弹窗节点
        /// </summary>
        private readonly Dictionary<UIDialogView,UILayer> _dialogRootType = new Dictionary<UIDialogView, UILayer>();
        
        /// <summary>
        /// 显示的弹窗
        /// </summary>
        private  UIDialogView _quauaShowView;
        
        /// <summary>
        /// 队列外弹窗
        /// </summary>
        private  readonly List<UIDialogView> OtherDialogs = new List<UIDialogView>();
        /// <summary>
        /// 弹窗父节点
        /// </summary>
        public  RectTransform DialogRoot;
        /// <summary>
        /// 引导弹窗节点
        /// </summary>
        public  RectTransform GuildRoot;
       

        /// <summary>
        /// 打开弹窗 - 加入队列挨个打开
        /// </summary>
        /// <param name="view">弹窗界面</param>
        /// <param name="uiLayer">ui层级</param>
        /// <param name="args">数据</param>
        public async UniTask OpenQueue(UIDialogView view,UILayer uiLayer,params object[] args)
        {
            // 有显示的界面 - 加入队列
            if (_quauaShowView != null)
            {
                QueueDialogs.Enqueue(view);
                // 记录数据
                _dialogDatas.Add(view,args);
                // 记录节点
                _dialogRootType.Add(view,uiLayer);
            }

            else
            {
                // 当前队列弹窗
                _quauaShowView = view;
                // 打开
                await view.Open(uiLayer,args);
            }
        }

        /// <summary>
        /// 打开弹窗 - 直接打开弹窗，关闭当前显示界面,并加入堆栈
        /// </summary>
        /// <param name="view">弹窗界面</param>
        /// <param name="uiLayer">ui层级</param>
        /// <param name="args">数据</param>
        public async UniTask OpenStack(UIDialogView view,UILayer uiLayer,params object[] args)
        {
            // 有显示的界面 - 加入队列然后关闭
            if (_stackShowView != null)
            {
                StackDialogs.Push(_stackShowView);
                _stackShowView.Hide();
            }
            
            // 记录当前弹窗
            _stackShowView = view;
            
            // 打开
            await view.Open(uiLayer,args);
        }
        
        /// <summary>
        /// 打开弹窗
        /// </summary>
        /// <param name="view">弹窗界面</param>
        /// <param name="uiLayer">ui层级</param>
        /// <param name="args">数据</param>
        public async UniTask Open(UIDialogView view,UILayer uiLayer, params object[] args)
        {
            // 加入队列
            OtherDialogs.Add(view);
            
            // 打开
            await view.Open(uiLayer,args);
        }

        /// <summary>
        /// 关闭窗口
        /// </summary>
        /// <param name="view"></param>
        public async void CloseDialogView(UIDialogView view)
        {
            // 关闭
            view.Close();
            
            // 堆栈弹窗
            if (view == _stackShowView)
            {
                _stackShowView = null;
                // 打开被隐藏的窗口
                if(StackDialogs.Count > 0)
                {
                    var popView = StackDialogs.Pop();
                    popView.Show();
                    _stackShowView = popView;
                }
            }
            else if (view == _quauaShowView)
            {
                _quauaShowView = null;
                // 打开队列的弹窗
                if (QueueDialogs.Count > 0)
                {
                    _quauaShowView = QueueDialogs.Dequeue();
                    // 数据
                    var data = _dialogDatas[_quauaShowView];
                    // 父节点
                    var rootType = _dialogRootType[_quauaShowView];
                    _dialogDatas.Remove(_quauaShowView);
                    _dialogRootType.Remove(_quauaShowView);
                    // 打开
                    await _quauaShowView.Open(rootType,data);
                }
            }
            // 普通弹窗 移除列表
            else if (OtherDialogs.Contains(view))
                OtherDialogs.Remove(view);
        }
    }
}
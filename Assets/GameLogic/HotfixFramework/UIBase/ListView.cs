﻿using System;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace GameLogic.HotfixFramework
{
    public abstract class ListView : UIBaseView
    {
        // item对象列表
        protected List<ListViewItem> _itemList { get; private set; }
        // item数量
        protected int _itemCount { get; private set; }
        // 循环需要的item个数
        protected int _needItemCount{ get; private set; }
        
        // 显示区域尺寸
        protected Vector2 _viewSize{ get; private set; }
        // item尺寸
        protected Vector2 _itemSize{ get; private set; }
        // 间隔
        protected Vector2 _itemSpace { get; private set; }
        // 顶部间隔
        protected float _topSpace { get; private set; }
        // 底部间隔
        protected float _bottomSpace { get; private set; }
        // 右边间隔
        protected float _rightSpace { get; private set; }
        // 左边间隔
        protected float _leftSpace { get; private set; }
        // 显示方向
        protected ListViewDir _listViewDir;

        /// <summary>
        /// 滑动组件
        /// </summary>
        /// <returns></returns>
        public abstract ScrollRect ScrollRectNode { get; }
        private RectTransform ItemContentNode => ScrollRectNode.content;

        /// <summary>
        /// 刷新Item
        /// </summary>
        /// <param name="listItem"></param>
        protected virtual void OnRefreshListItem(ListViewItem listItem) { }
        
        /// <summary>
        /// 初始化循环列表
        /// </summary>
        /// <param name="itemCount">所有item的数量</param>
        /// <param name="listItem">列表item对象</param>
        /// <param name="listViewDir">列表方向</param>
        /// <param name="startIndex">显示第一行的item索引</param>
        protected void InitListView(int itemCount,ListViewItem listItem,ListViewDir listViewDir,int startIndex = 0)
        {
            // item 数量
            _itemCount = itemCount;
            // 显示区域尺寸
            _viewSize = ScrollRectNode.GetComponent<RectTransform>().rect.size;
            // item尺寸
            _itemSize = listItem.Size;
            // 需要数量
            _needItemCount = NeedItemCount(listViewDir);
            // 设置好 item content的尺寸
            SetContentSize(listViewDir);
            // 起点
            ItemContentNode.anchorMax = ItemContentNode.anchorMin = new Vector2(0, 1);
            ItemContentNode.localPosition = new Vector3(0,0,0);
            // 方向
            _listViewDir = listViewDir;

            // 设置滑动方向
            ScrollRectNode.horizontal = listViewDir == ListViewDir.horizontal;
            ScrollRectNode.vertical = listViewDir == ListViewDir.vertical;
            
            if(_itemList == null)
                _itemList = new List<ListViewItem>();
            
            // 创建循环的item
            for (int i = _itemList.Count; i < _needItemCount; i++)
            {
                var itemTalent = listItem.Instantiate(this,ScrollRectNode.content.gameObject);
                _itemList.Add(itemTalent);
            }
            listItem.DisplayObject.SetActive(false);
            
            // 注册滑动回调
            ScrollRectNode.onValueChanged.RemoveAllListeners();
            ScrollRectNode.onValueChanged.AddListener(OnListViewChanged);

            // 移动到初始位置
            if (startIndex != 0)
                MoveToIndex(startIndex,0);
            // 刷新列表
            else
                RefreshListView();
        }
        
        /// <summary>
        /// 设置间隔
        /// </summary>
        /// <param name="itemSpace">item之间的间隔</param>
        /// <param name="topSpace">与顶部的间隔</param>
        /// <param name="bottomSpace">与底部的间隔</param>
        /// <param name="rightSpace">与右边的间隔</param>
        /// <param name="leftSpace">与左边的间隔</param>
        protected void SetSpace(Vector2 itemSpace,float topSpace,float bottomSpace,float rightSpace,float leftSpace)
        {
            _itemSpace = itemSpace;
            _topSpace = topSpace;
            _bottomSpace = bottomSpace;
            _rightSpace = rightSpace;
            _leftSpace = leftSpace;
        }
        /// <summary>
        /// 滑动界面回调
        /// </summary>
        /// <param name="progress"></param>
        int _preValue = -1;
        private void OnListViewChanged(Vector2 progress)
        {
            // 刷新item
            RefreshListView();
        }
        
        /// <summary>
        /// 刷新列表
        /// </summary>
        protected void RefreshListView()
        {
            // 顶部item位置
            var topPosIndex = CurTopItemIndex();
            // 排序
            int order = 0;
            for (int posIndex = topPosIndex; posIndex < topPosIndex + _needItemCount; posIndex++)
            {
                // 排序
                order++;
                // 取余获取item的索引
                var itemIndex = posIndex % _needItemCount;
                if (posIndex < _itemCount)
                {
                    // 索引
                    _itemList[itemIndex].Index = posIndex;
                    // 更新位置
                    var pos = GetItemPos(posIndex);
                    // 设置位置
                    var rect = _itemList[itemIndex].RectTransform;
                    rect.anchoredPosition = pos;
                    // 刷新Item
                    OnRefreshListItem(_itemList[itemIndex]);
                    // 设置层级
                    SetSibling(rect, order);
                }
                else
                {
                    _itemList[itemIndex].Hide();
                }
            }
        }
        /// <summary>
        /// 设置层级
        /// </summary>
        protected virtual void SetSibling(RectTransform item,int order) { }

        /// <summary>
        /// 移动到指定位置
        /// </summary>
        /// <param name="index">指定item 索引</param>
        /// <param name="duration">移动时间</param>
        /// <param name="callBack">完成回调</param>
        protected void MoveToIndex(int index,float duration = 0.2f,Action callBack = null)
        {
            // 关闭之前动画
            ItemContentNode.DOKill();
            ScrollRectNode.StopMovement();
            switch (_listViewDir)
            {
                case ListViewDir.vertical:
                    // item到上边界距离Y
                    var itemDisY = ItemContentNode.rect.size.y / 2 - GetItemPos(index).y;
                    // 显示区域上边界到item父节点上边界距离
                    var contentDisY = itemDisY - _viewSize.y / 2;
                    // item父节点Y轴值
                    var contentY = Mathf.Clamp(contentDisY, 0, ItemContentNode.rect.size.y - _viewSize.y);
                    if (duration > 0)
                    {
                        // 移动到指定位置
                        ItemContentNode.DOLocalMoveY(contentY, duration).OnComplete(() =>
                        {
                            RefreshListView();
                            callBack?.Invoke();
                        });
                    }
                    else
                    {
                        var pos = ItemContentNode.localPosition;
                        ItemContentNode.localPosition = new Vector3(pos.x,contentY,pos.z);
                        RefreshListView();
                        callBack?.Invoke();
                    }
                    break;
                case ListViewDir.horizontal:
                    var itemDisX = ItemContentNode.rect.size.x / 2 + GetItemPos(index).x;
                    // 显示区域上边界到item父节点上边界距离
                    var contentDisX = itemDisX - _viewSize.x / 2;
                    // item父节点Y轴值
                    var contentX = Mathf.Clamp(-contentDisX, -(ItemContentNode.rect.size.x - _viewSize.x), 0);
                    if (duration > 0)
                    {
                        // 移动到指定位置
                        ItemContentNode.DOLocalMoveX(contentX, duration).OnComplete(() =>
                        {
                            RefreshListView();
                            callBack?.Invoke();
                        });
                    }
                    else
                    {
                        var pos = ItemContentNode.localPosition;
                        ItemContentNode.localPosition = new Vector3(contentX,pos.y,pos.z);
                        RefreshListView();
                        callBack?.Invoke();
                    }
                    break;
            }
        }

        /// <summary>
        /// 获取顶部item索引
        /// </summary>
        /// <returns></returns>
        protected int CurTopItemIndex()
        {
            switch (_listViewDir)
            {
                case ListViewDir.vertical:
                    // item父节点 的Y轴坐标 （左上角对齐）
                    var y = ItemContentNode.localPosition.y - _topSpace;
                    // 去除于上边界对应的index
                    var index = y / (_itemSize.y + _itemSpace.y);
                    // 限制index范围
                    index = Mathf.Clamp(index, 0, _itemCount - 1);
                    // 向下取整
                    index = Mathf.FloorToInt(index);
                    return (int)index;
                case ListViewDir.horizontal:
                    var x = ItemContentNode.localPosition.x - _leftSpace;
                    var indexX = -x / (_itemSize.x + _itemSpace.x);
                    // 限制index范围
                    indexX = Mathf.Clamp(indexX, 0, _itemCount - 1);
                    // 向下取整
                    indexX = Mathf.FloorToInt(indexX);
                    return (int)indexX;
            }
            return 0;
        }
        /// <summary>
        /// 获取item 坐标
        /// </summary>
        /// <param name="index">索引</param>
        /// <returns></returns>
        private Vector3 GetItemPos(int index)
        {
            switch (_listViewDir)
            {
                case ListViewDir.horizontal:
                    var hDis = _itemSize.x / 2 + index * _itemSize.x + index *_itemSpace.x + _leftSpace;
                    var x = ItemContentNode.rect.size.x / 2 - hDis;
                    return new Vector3(-x,0,0);
                case ListViewDir.vertical:
                    var vDis = _itemSize.y / 2 + index * _itemSize.y + index *_itemSpace.y + _topSpace;
                    var y = ItemContentNode.rect.size.y / 2 - vDis;
                    return new Vector3(0,y,0);
            }
            return Vector3.zero;
        }
        
        /// <summary>
        /// 需要数量
        /// </summary>
        /// <param name="listViewDir">方向</param>
        private int NeedItemCount(ListViewDir listViewDir)
        {
            var needItemCount = 1;
            switch (listViewDir)
            {
                case ListViewDir.horizontal:
                    needItemCount += Mathf.CeilToInt(_viewSize.x / _itemSize.x);
                    break;
                case ListViewDir.vertical:
                    needItemCount += Mathf.CeilToInt(_viewSize.y / _itemSize.y);
                    break;
            }

            return needItemCount;
        }
        
        /// <summary>
        /// 设置好content的尺寸
        /// </summary>
        /// <param name="listViewDir">方向</param>
        private void SetContentSize(ListViewDir listViewDir)
        {
            var countSize = ItemContentNode.rect.size;
            var newSize = Vector2.zero;
            switch (listViewDir)
            {
                case ListViewDir.horizontal:
                    newSize.x = _itemCount  * _itemSize.x + (_itemCount - 1) * _itemSpace.x + _rightSpace + _leftSpace;
                    newSize.y = countSize.y;
                    break;
                case ListViewDir.vertical:
                    // 父节点高 = 所有item高 + 所有item之间的间隔 + 底部间隔 + 顶部间隔
                    var contentHeight = _itemCount * _itemSize.y + (_itemCount - 1) * _itemSpace.y + _bottomSpace + _topSpace;
                    // 限制最小为显示区域高
                    newSize.y = Mathf.Max(contentHeight, _viewSize.y);
                    newSize.x = countSize.x;
                    break;
            }

            ScrollRectNode.content.sizeDelta = newSize;
        }

        public override void Dispose()
        {
            if (_itemList != null)
            {
                for(int i = 0; i <  _itemList.Count; i++)
                    _itemList[i].Dispose();
                _itemList = null;
            }
            // 清楚回调
            ScrollRectNode.onValueChanged.RemoveAllListeners();
            
            base.Dispose();
        }
    }
    
    /// <summary>
    /// 方向
    /// </summary>
    public enum ListViewDir
    {
        horizontal,
        vertical,
    }
}
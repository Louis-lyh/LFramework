﻿using UnityEngine;

namespace GameLogic.HotfixFramework
{
    public abstract class ListViewItem : UIBaseView
    {
        // 序号
        public int Index { get; set; }

        // 尺寸
        public Vector2 Size
        {
            get { return RectTransform.sizeDelta; }

            set { RectTransform.sizeDelta = value; }
        }

        // 位置
        public Vector2 Position
        {
            get { return RectTransform.anchoredPosition; }

            set { RectTransform.anchoredPosition = value; }
        }

        public override void Dispose()
        {
            // 回收
            UIItemPool.CollectItem(RectTransform);
            base.Dispose();
        }


        public abstract ListViewItem Instantiate(UIBaseView parentView, GameObject parent);
    }

}
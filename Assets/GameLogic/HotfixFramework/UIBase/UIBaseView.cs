﻿using System;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using GameInit.Framework;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace GameLogic.HotfixFramework
{
    /// <summary>
    /// UI 基类
    /// </summary>
    public abstract class UIBaseView : IDispose
    {
        public RectTransform RectTransform { get; private set; }
        public GameObject DisplayObject { get; private set; }

        /// <summary>
        /// 父对象
        /// </summary>
        protected UIBaseView _parentView; 
        
        /// <summary>
        /// 预制体路径
        /// </summary>
        protected virtual string PrefabPath { get; }
        /// <summary>
        /// 按钮列表
        /// </summary>
        private List<Button> _allButtons;
        /// <summary>
        /// 输入列表
        /// </summary>
        private List<InputField> _allInputFields;
        /// <summary>
        /// 是否显示
        /// </summary>
        protected bool IsShow;

        /// <summary>
        /// 是否需要回收
        /// </summary>
        private bool _isNeedRecycle;

        protected UIBaseView()
        {
            _allButtons = new List<Button>();
            _allInputFields = new List<InputField>(); 
        }
        
        /// <summary>
        /// 初始化
        /// </summary>
        /// <param name="parent"></param>
        protected async UniTask Init(RectTransform parent)
        {
            // 加载对象
            var obj = await ResourceManager.ConstructObjectAsync(ResPathUtils.GetUIPrefab(PrefabPath));
            // 设置游戏对象
            SetDisplayObject(obj,true);
            // 设置层级
            SetParent(parent);
        }
        
        /// <summary>
        /// 设置游戏对象
        /// </summary>
        /// <param name="obj">游戏对象</param>
        /// <param name="isNeedRecucle">是否需要回收</param>
        public void SetDisplayObject(GameObject obj,bool isNeedRecucle = false)
        {
            // 设置ui对象
            DisplayObject = obj;
            if (DisplayObject == null)
            {
                Debug.Log("UIBaseView.SetDisplayObject() : 设置显示对象为null, View:"+GetType().Name);
                return;
            }
            // ui变化组件
            RectTransform = DisplayObject.transform as RectTransform;
            // 需要
            _isNeedRecycle = isNeedRecucle;
            // 加载ui组件
            ParseComponent();
            // 用于ui界面初始化
            ParseComponentExit();
        }

        /// <summary>
        /// 设置父对象
        /// </summary>
        /// <param name="parent"></param>
        public void SetParent(RectTransform parent)
        {
            RectTransform.SetParent(parent, false);
            RectTransform.localPosition = Vector3.zero;
        }
        
        /// <summary>
        /// 显示界面
        /// </summary>
        /// <param name="arg">刷新界面需要的数据</param>
        public void Show(params object[] arg)
        {
            // ------ 显示UI ------
            DisplayObject.SetActive(true);
            // 注册事件
            if(!IsShow)
                RegisterEvent();
            
            // 标记-显示
            IsShow = true;
            // 刷新界面
            OnShow(arg);
        }
        
        /// <summary>
        /// 隐藏
        /// </summary>
        public void Hide()
        {
            // 隐藏界面
            if(DisplayObject != null && DisplayObject.activeSelf)
                DisplayObject.SetActive(false);
            
            if(!IsShow)
                return;
            
            // 标记 - 隐藏
            IsShow = false;
            // 注销事件
            UnRegisterEvent();
            // 隐藏界面
            OnHide();
        }

        /// <summary>
        /// 刷新界面
        /// </summary>
        /// <param name="arg">刷新界面需要的数据</param>
        protected virtual void OnShow(params object[] arg){}
        
        /// <summary>
        /// 隐藏界面
        /// </summary>
        protected virtual void OnHide(){}

        /// <summary>
        /// 注册事件
        /// </summary>
        protected virtual void RegisterEvent(){}
        
        /// <summary>
        /// 注销事件
        /// </summary>
        protected virtual void UnRegisterEvent(){}

        /// <summary>
        /// 设置对象后 - 加载UI组件、UI节点
        /// </summary>
        protected abstract void ParseComponent();
        /// <summary>
        /// 加载Ui组件、UI节点结束
        /// </summary>
        protected virtual void ParseComponentExit(){}
        
        /// <summary>
        /// 注册按钮
        /// </summary>
        /// <param name="btn"></param>
        protected void ListenButton(Button btn)
        {
            if (btn == null)
                return;
            // 注册回调
            btn.Click(() =>
            {
                OnClick(btn.name);
            });
            // 加入列表
            _allButtons.Add(btn);
        }
        
        /// <summary>
        /// 按钮点击事件
        /// </summary>
        /// <param name="name"></param>
        protected virtual void OnClick(string name) { }

        /// <summary>
        /// InputField事件
        /// </summary>
        /// <param name="input">输入组件</param>
        /// <param name="onEndEdit">输入结束回调</param>
        /// <param name="onValueChanged">内容改变回调</param>
        /// <param name="defaultStr">默认显示内容</param>
        protected void ListenInput(InputField input)
        {
            // 输入结束回调
            input.onEndEdit.AddListener((value) =>
            {
                OnInputEndEdit(input.name, value);
            });
            // 内容改变回调
            input.onValueChanged.AddListener((value) =>
            {
                OnInputValueChanged(input.name, value);
            });
            // 加入列表
            _allInputFields.Add(input);
        }
        
       /// <summary>
       /// 输入结束
       /// </summary>
       /// <param name="name">对象名</param>
       /// <param name="value">内容</param>
        protected virtual void OnInputEndEdit(string name,string value)
        {
        }
        
        /// <summary>
        /// 内容改变
        /// </summary>
        /// <param name="name">对象名</param>
        /// <param name="value">内容</param>
        protected virtual void OnInputValueChanged(string name,string value)
        {
        }

        /// <summary>
        /// 找到子节点组件
        /// </summary>
        /// <param name="path"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        protected T Find<T>(string path) where T : Component
        {
            var obj = Find(path);
            // 获取组件
            if (obj != null)
                return obj.GetComponent<T>();

            return null;
        }
        
        /// <summary>
        /// 找到对象
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        protected GameObject Find(string path)
        {
            return RectTransform.Find(path)?.gameObject;
        }
        /// <summary>
        /// 销毁
        /// </summary>
        public virtual void Dispose()
        {
            // 注销事件
            UnRegisterEvent();
            
            // 注销按钮监听
            if (_allButtons != null)
            {
                for (int i = 0; i < _allButtons.Count; i++)
                    _allButtons[i].onClick.RemoveAllListeners();
                _allButtons.Clear();
            }
            
            // 注销输入框监听
            if (_allInputFields != null)
            {
                for (int i = 0; i < _allInputFields.Count; i++)
                {
                    _allInputFields[i].onEndEdit.RemoveAllListeners();
                    _allInputFields[i].onValueChanged.RemoveAllListeners();
                }
                _allInputFields.Clear();
            }

            // 回收对象
            if (DisplayObject != null && _isNeedRecycle)
                ResourceManager.CollectObj(DisplayObject);
            
            DisplayObject = null;
            RectTransform = null;
        }
    }
}
﻿using System;
using Cysharp.Threading.Tasks;
using DG.Tweening;
using UnityEngine;

namespace GameLogic.HotfixFramework
{
    /// <summary>
    /// 弹窗基类
    /// </summary>
    public abstract class UIDialogView : UIBaseView
    {
        /// <summary>
        /// 弹窗的层级
        /// </summary>
        public UILayer DialogLayer { get; protected set; }
        
        #region 基础方法
        /// <summary>
        /// 加载ui对象
        /// </summary>
        /// <returns></returns>
        public async UniTask Open(UILayer dialogLayer,params object[] args)
        {
            // ------ 存在对象 ------
            // 显示
            if (DisplayObject != null)
            {
                Show(args);
                return;
            }
            // 父节点
            var parent = UIManager.Instance.GetLayerRoot(dialogLayer);
            // 初始化
            await Init(parent);

            // ------ 显示UI ------
            DisplayObject.SetActive(true);
            // 注册事件
            RegisterEvent();
            // 标记-显示
            IsShow = true;
            // 打开
            OnOpen(args);
            // 打开动画
            DialogIn();
        }
        /// <summary>
        /// 关闭
        /// </summary>
        public void Close()
        {
            DialogOut(() =>
            {
                // 隐藏界面
                if(DisplayObject != null && DisplayObject.activeSelf)
                    DisplayObject.SetActive(false);
                // 标记 - 隐藏
                IsShow = false;
                // 关闭
                OnClose();
                // 销毁
                Dispose();
            });
        }
        
        /// <summary>
        /// 打开
        /// </summary>
        /// <param name="args">数据</param>
        protected virtual void OnOpen(params object[] args){ }
        
        /// <summary>
        /// 关闭
        /// </summary>
        protected virtual void OnClose() { }
        #endregion

        #region 淡入淡出动画
        // 显示动画
        protected bool ShowInAni = true;
        protected bool ShowOutAni = true;
        
        /// <summary>
        /// 打开动画
        /// </summary>
        private void DialogIn()
        {
            if (!ShowInAni)
            {
                DialogInComplete();
                return;
            }
            // 打开动画
            OnDialogIn();
        }
        /// <summary>
        /// 打开动画
        /// </summary>
        protected virtual void OnDialogIn()
        {
            // 获取画布组件
            var group = GetCanvasGroup();
            // 重置透明度
            group.alpha = 1;
            // 动画
            DOTween.Sequence().SetUpdate(true)
                .Append(group.DOFade(1, 0.25f))
                .Join(RectTransform.DOLocalMove(new Vector3(0, -10, 0), 0.25f))
                .Append(RectTransform.DOLocalMove(Vector3.zero, 0.1f))
                .OnComplete(() =>DialogInComplete());
        }

        /// <summary>
        /// 关闭动画
        /// </summary>
        private void DialogOut(Action callBack)
        {
            if (!ShowOutAni)
            {
                callBack?.Invoke();
                return;
            }

            // 关闭动画
            OnDialogOut(callBack);
        }
        
        /// <summary>
        /// 关闭动画
        /// </summary>
        protected virtual void OnDialogOut(Action callBack)
        {
            var group = GetCanvasGroup();
            group.alpha = 1;
            DOTween.Sequence().SetUpdate(true).Append(RectTransform.DOLocalMove(new Vector3(0, -10, 0), 0.1f))
                .Append(group.DOFade(0, 0.25f))
                .Join(RectTransform.DOLocalMove(new Vector3(0, 200, 0), 0.25f))
                .AppendCallback(() =>
                {
                    RectTransform.localPosition = Vector3.zero;
                    callBack?.Invoke();
                });
        }

        /// <summary>
        /// 淡入动画完成
        /// </summary>
        protected virtual void DialogInComplete(){}
        
        /// <summary>
        /// 获取画布组件
        /// </summary>
        /// <returns></returns>
        protected CanvasGroup GetCanvasGroup()
        {
            var group = RectTransform.GetComponent<CanvasGroup>();
            if (!group)
            {
                group = RectTransform.gameObject.AddComponent<CanvasGroup>();
            }

            return group;
        }

        #endregion

    }
}
﻿using Cysharp.Threading.Tasks;
using UnityEngine;

namespace GameLogic.HotfixFramework
{
    public abstract class UIItemView : UIBaseView
    {
        /// <summary>
        /// 创建
        /// </summary>
        /// <param name="parentView">父界面</param>
        /// <param name="parent">父节点</param>
        /// <param name="name">mz</param>
        /// <returns></returns>
        public async UniTask Create(UIBaseView parentView, RectTransform parent, string name)
        {
            // 父窗口
            _parentView = parentView;
            // 初始化
            await Init(parent);
            // 名字
            DisplayObject.name = name;
        }

    }
}
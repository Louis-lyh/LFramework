﻿using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

namespace GameLogic.HotfixFramework
{
    /// <summary>
    /// UI管理器
    /// </summary>
    public class UIManager : Singleton<UIManager>
    {
        #region 字段
        /// <summary>
        /// ui层级节点-下
        /// </summary>
        private RectTransform _bottomRoot;
        /// <summary>
        /// ui层级节点-中
        /// </summary>
        private RectTransform _middleRoot;
        /// <summary>
        /// ui层级节点-上
        /// </summary>
        private RectTransform _topRoot;
        /// <summary>
        /// ui层级节点-引导
        /// </summary>
        private RectTransform _guideRoot;
        /// <summary>
        /// ui适配缩放组件
        /// </summary>
        private CanvasScaler _canvasScaler;
        /// <summary>
        /// ui相机
        /// </summary>
        public Camera UICamera { get; private set; }
        /// <summary>
        /// ui画布
        /// </summary>
        public Canvas UICanves { get; private set; }
        
        /// <summary>
        /// 屏幕中心到最左边距离
        /// </summary>
        public float Left
        {
            get { return GetNewPos(-Screen.width / 2); }
        }
        /// <summary>
        /// 屏幕中心到最右边距离
        /// </summary>
        public float Right
        {
            get { return GetNewPos(Screen.width / 2); }
        }
        /// <summary>
        /// 屏幕中心到最下边距离
        /// </summary>
        public float Bottom
        {
            get { return GetNewPos(-Screen.height / 2); }
        }
        /// <summary>
        /// 屏幕中心到最上边距离
        /// </summary>
        public float Top
        {
            get { return GetNewPos(Screen.height / 2); }
        }

        #endregion

        /// <summary>
        /// 初始化 ui节点等相关数据
        /// </summary>
        public void Init()
        {
            // ------ 初始化UI节点 ------
            var uiRoot = GameObject.Find("UIRoot");
            // ui相机
            UICamera = uiRoot.transform.Find("UICamera").GetComponent<Camera>();
            _canvasScaler = uiRoot.transform.Find("UICanvas").GetComponent<CanvasScaler>();
            // ui节点
            _bottomRoot = uiRoot.transform.Find("UICanvas/BottomRoot") as RectTransform;
            _middleRoot = uiRoot.transform.Find("UICanvas/MiddleRoot") as RectTransform;
            _topRoot = uiRoot.transform.Find("UICanvas/TopRoot") as RectTransform;
            _guideRoot = uiRoot.transform.Find("UICanvas/GuideRoot") as RectTransform;
            // 初始化UI节点
            DialogManager.Instance.DialogRoot = _topRoot;
            DialogManager.Instance.GuildRoot = _guideRoot;
            
            // ui画布
            UICanves = uiRoot.transform.Find("UICanvas").GetComponent<Canvas>();
            
            // ------ ui界面适配缩放 ------
            if (Screen.height * 9 > Screen.width * 16)
                _canvasScaler.matchWidthOrHeight = 0; // fixed with width
            else
                _canvasScaler.matchWidthOrHeight = 1; // fixed with height
        }
        /// <summary>
        /// 设置窗口ui层级
        /// </summary>
        /// <param name="layer"></param>
        public RectTransform GetLayerRoot(UILayer layer)
        {
            var layerTransform = _bottomRoot;
            switch (layer)
            {
                case UILayer.Bottom:
                    layerTransform = _bottomRoot;
                    break;
                case UILayer.Middle:
                    layerTransform = _middleRoot;
                    break;
                case UILayer.Top:
                    layerTransform = _topRoot;
                    break;
                case UILayer.Guide:
                    layerTransform = _guideRoot;
                    break;
            }

            return layerTransform;
        }
        public Vector3 GetNewPos(Vector3 value)
        {
            if (_canvasScaler.matchWidthOrHeight == 0)
                //匹配宽度时仅按照宽度计算
                return value * _canvasScaler.referenceResolution.x / Screen.width;
            else
                //匹配高度时仅按照高度计算
                return value * _canvasScaler.referenceResolution.y / Screen.height;
        }
        public float GetNewPos(float value)
        {
            if (_canvasScaler.matchWidthOrHeight == 0)
                //匹配宽度时仅按照宽度计算
                return value * _canvasScaler.referenceResolution.x / Screen.width;
            else
                //匹配高度时仅按照高度计算
                return value * _canvasScaler.referenceResolution.y / Screen.height;

        }
        public Vector3 GetOldPos(Vector3 value)
        {
            if (_canvasScaler.matchWidthOrHeight == 0)
                //匹配宽度时仅按照宽度计算
                return value / (_canvasScaler.referenceResolution.x / Screen.width);
            else
                //匹配高度时仅按照高度计算
                return value / (_canvasScaler.referenceResolution.y / Screen.height);
        }

    }
}
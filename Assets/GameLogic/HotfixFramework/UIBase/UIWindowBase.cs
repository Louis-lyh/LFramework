﻿using Cysharp.Threading.Tasks;
using GameInit.Framework;
using UnityEngine;

namespace GameLogic.HotfixFramework
{
    /// <summary>
    /// 窗口基类
    /// </summary>
    public abstract class UIWindowBase<T> : UIBaseView where T : UIBaseView, new() 
    {
        private static T msInstance;
        public static T Instance { get { return msInstance ?? (msInstance = new T()); } }
        public static bool Exist { get { return msInstance != null && msInstance.DisplayObject != null; } }
        public bool IsVisible { get { return DisplayObject.activeSelf; } }
        
        /// <summary>
        /// 窗口层级
        /// </summary>
        public UILayer Layer { get; protected set; }

        #region 基础方法
        /// <summary>
        /// 加载ui对象
        /// </summary>
        /// <returns></returns>
        public async UniTask Open(UILayer dialogLayer,params object[] args)
        {
            // ------ 存在对象 ------
            // 显示
            if (DisplayObject != null)
            {
                Show(args);
                return;
            }
            // 父节点
            var parent = UIManager.Instance.GetLayerRoot(dialogLayer);
            // 初始化
            await Init(parent);

            // ------ 显示UI ------
            DisplayObject.SetActive(true);
            // 注册事件
            RegisterEvent();
            // 标记-显示
            IsShow = true;
            // 打开
            OnOpen(args);
            // 打开动画
            OpenAnimation();
        }
        /// <summary>
        /// 关闭
        /// </summary>
        public void Close()
        {
            // 隐藏界面
            if(DisplayObject != null && DisplayObject.activeSelf)
                DisplayObject.SetActive(false);
            // 标记 - 隐藏
            IsShow = false;
            // 关闭
            OnClose();
            // 销毁
            Dispose();
        }
        
        /// <summary>
        /// 打开
        /// </summary>
        /// <param name="args">数据</param>
        protected virtual void OnOpen(params object[] args){ }
        
        /// <summary>
        /// 关闭
        /// </summary>
        protected virtual void OnClose() { }
        
        /// <summary>
        /// 打开动画
        /// </summary>
        protected virtual void OpenAnimation() { }

        #endregion

    }
    
    /// <summary>
    /// ui层级
    /// </summary>
    public enum UILayer
    {
        Bottom,
        Middle,
        Top,
        Guide,
    }
}
﻿using System;
using DG.Tweening;
using UnityEngine.UI;

namespace GameInit.Framework
{
    public class TweenUtil
    {
        /// <summary>
        /// 数值滚动动画
        /// </summary>
        /// <param name="startValue">开始值</param>
        /// <param name="endValue">结束值</param>
        /// <param name="duration">持续时间</param>
        /// <param name="callBack">回调</param>
        /// <returns></returns>
        public static Tween ToValue(float startValue,float endValue,float duration,Action<float> callBack)
        {
            return DOTween.To(
                ()=> startValue,
                x=>callBack?.Invoke(x),
                endValue,duration);
        }
    }
}